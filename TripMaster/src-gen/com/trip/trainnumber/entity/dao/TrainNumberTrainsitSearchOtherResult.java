package com.trip.trainnumber.entity.dao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table TRAIN_NUMBER_TRAINSIT_SEARCH_OTHER_RESULT.
 */
public class TrainNumberTrainsitSearchOtherResult implements java.io.Serializable {

    private Long id;
    private String zhongzhuan_city;
    private String transferStartCount;
    private String transferEndCount;
    private String minMileage;
    private String maxMileage;
    private String minTime;
    private String maxTime;
    private String minPrice;
    private String maxPrice;

    public TrainNumberTrainsitSearchOtherResult() {
    }

    public TrainNumberTrainsitSearchOtherResult(Long id) {
        this.id = id;
    }

    public TrainNumberTrainsitSearchOtherResult(Long id, String zhongzhuan_city, String transferStartCount, String transferEndCount, String minMileage, String maxMileage, String minTime, String maxTime, String minPrice, String maxPrice) {
        this.id = id;
        this.zhongzhuan_city = zhongzhuan_city;
        this.transferStartCount = transferStartCount;
        this.transferEndCount = transferEndCount;
        this.minMileage = minMileage;
        this.maxMileage = maxMileage;
        this.minTime = minTime;
        this.maxTime = maxTime;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getZhongzhuan_city() {
        return zhongzhuan_city;
    }

    public void setZhongzhuan_city(String zhongzhuan_city) {
        this.zhongzhuan_city = zhongzhuan_city;
    }

    public String getTransferStartCount() {
        return transferStartCount;
    }

    public void setTransferStartCount(String transferStartCount) {
        this.transferStartCount = transferStartCount;
    }

    public String getTransferEndCount() {
        return transferEndCount;
    }

    public void setTransferEndCount(String transferEndCount) {
        this.transferEndCount = transferEndCount;
    }

    public String getMinMileage() {
        return minMileage;
    }

    public void setMinMileage(String minMileage) {
        this.minMileage = minMileage;
    }

    public String getMaxMileage() {
        return maxMileage;
    }

    public void setMaxMileage(String maxMileage) {
        this.maxMileage = maxMileage;
    }

    public String getMinTime() {
        return minTime;
    }

    public void setMinTime(String minTime) {
        this.minTime = minTime;
    }

    public String getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(String maxTime) {
        this.maxTime = maxTime;
    }

    public String getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }

    public String getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(String maxPrice) {
        this.maxPrice = maxPrice;
    }

}
