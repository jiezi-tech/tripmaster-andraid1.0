package afinal.http.base;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.tripmaster.util.MLogger;

import afinal.FinalHttp;
import afinal.http.AjaxCallBack;
import afinal.http.AjaxParams;
import android.content.Context;

/**
 * 网络请求统一接口
 * **/
public class BaseFinalHttp{
	private Context mContext;
	private FinalHttp finalHttp = null;
	private final static MLogger logger = new MLogger(BaseFinalHttp.class);
	
	public BaseFinalHttp(Context mContext) {
		this.mContext = mContext;
		finalHttp = new FinalHttp();
	}
	
	public void get( String url, AjaxCallBack<? extends Object> callBack) {
		finalHttp.get( url, null, callBack);
    }
	public void get( String url, AjaxParams params, AjaxCallBack<? extends Object> callBack) {
		//需要做一下参数处理 每次请求都带上sessionId
		if(params == null){
			params = new AjaxParams();
		}
		finalHttp.get(url, params, callBack);
	}
	public static void setDefaultSSLSocketFactory() {
		SSLContext sc;
		try {
			sc = SSLContext.getInstance("SSL");
			TrustManager[] tmArr = { new X509TrustManager() {
				public void checkClientTrusted(
						X509Certificate[] paramArrayOfX509Certificate,
						String paramString) throws CertificateException {
				}

				public void checkServerTrusted(
						X509Certificate[] paramArrayOfX509Certificate,
						String paramString) throws CertificateException {
				}

				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
			} };
			sc.init(null, tmArr, new SecureRandom());
			HttpsURLConnection
					.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
