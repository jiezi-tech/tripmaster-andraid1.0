package afinal.http.base;

import org.json.JSONException;
import org.json.JSONObject;

import com.tripmaster.util.MLogger;

import afinal.http.AjaxCallBack;
import android.content.Context;

public class BaseAjaxCallBack extends AjaxCallBack<String> {
	private Context mContext;
	private final static MLogger logger = new MLogger(BaseAjaxCallBack.class);
	
	public BaseAjaxCallBack(Context c) {
		mContext = c;
	}

	@Override
	public void onSuccess(String t) {
		try {
			JSONObject result = new JSONObject(t);
			logger.debug(t);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onFailure(Throwable t, int errorNo, String strMsg) {
		// TODO Auto-generated method stub
		super.onFailure(t, errorNo, strMsg);
		logger.error(strMsg);
		if(strMsg != null){
			try {
				onResultError(new JSONObject(strMsg));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onLoading(long count, long current) {
		// TODO Auto-generated method stub
		super.onLoading(count, current);
	}

	/**
	 * 当网络返回正确
	 */
	public void onResultCorrect(JSONObject result) {
		
	}

	/**
	 * 当网络返回错误
	 */
	public void onResultError(JSONObject result) {

	}
}
