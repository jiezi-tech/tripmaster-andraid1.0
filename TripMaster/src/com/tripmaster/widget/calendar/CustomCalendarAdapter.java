package com.tripmaster.widget.calendar;

import hirondelle.date4j.DateTime;

import java.util.HashMap;

import com.tripmaster.R;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class CustomCalendarAdapter extends CaldroidGridAdapter {

	public CustomCalendarAdapter(Context context, int month, int year,
			HashMap<String, Object> caldroidData,
			HashMap<String, Object> extraData) {
		super(context, month, year, caldroidData, extraData);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.custom_calendar_cell, null);
		}
		TextView cellTv = (TextView) convertView.findViewById(R.id.text);

		DateTime dateTime = this.datetimeList.get(position);
		Resources resources = context.getResources();

		// Customize for disabled dates and date outside min/max dates
		if ((minDateTime != null && dateTime.lt(minDateTime))
				|| (maxDateTime != null && dateTime.gt(maxDateTime))
				|| (disableDates != null && disableDates.indexOf(dateTime) != -1)) {
			cellTv.setTextColor(resources
					.getColor(R.color.calendarCellTextColor));

		}
		if (dateTime.equals(getToday())) {
			cellTv.setBackgroundColor(resources
					.getColor(R.color.resultListbottomBarTextPressedColor));
			cellTv.setTextColor(resources
					.getColor(R.color.white));
			cellTv.setText("今天");
		} else
			cellTv.setText(dateTime.getDay() + "");
		
		if(selectedDates != null && selectedDates.indexOf(dateTime) != -1){
			cellTv.setBackgroundColor(resources
					.getColor(R.color.resultListbottomBarTextPressedColor));
			cellTv.setTextColor(resources
					.getColor(R.color.white));
		}

		return convertView;
	}

}
