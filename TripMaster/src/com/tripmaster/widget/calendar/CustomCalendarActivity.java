package com.tripmaster.widget.calendar;

import java.text.ParseException;
import java.util.Date;

import com.tripmaster.R;
import com.tripmaster.base.BaseFragmentActivity;
import com.tripmaster.traintickets.CustomCalendarFragment;
import com.tripmaster.util.Constant;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;

public class CustomCalendarActivity extends BaseFragmentActivity{
	private FragmentManager manager;
	private CustomCalendarFragment fragment;
	private String currentDate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_custom_calendar);
		initView();
		initData();
	}
	
	/**
	 * 初始化页面
	 * **/
	private void initView() {
		initTitle("出发日期", true);
	}

	/**
	 * 初始化数据
	 * **/
	private void initData() {
		Intent intent = getIntent();
		if(intent != null){
			currentDate = intent.getStringExtra("currentDate");
		}
		fragment = new CustomCalendarFragment();
		fragment.setMinDate(new Date());//设置最小日期
		try {
			fragment.setSelectedDate(Constant.sdf.parse(currentDate));//设置当前选中的日期
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fragment.setMaxDateFromString(Constant.afterNDay(60), "yyyy-MM-dd");//设置最大日期
		fragment.setCaldroidListener(new CaldroidListener() {
			
			@Override
			public void onSelectDate(Date date, View view) {
				Log.i("android","当前选中日期:"+ Constant.sdf.format(date));
				Intent data = new Intent();
				data.putExtra("selectDate", Constant.sdf.format(date));
				setResult(RESULT_OK, data);
				finish();
			}
		});
		
		manager = getSupportFragmentManager();
		FragmentTransaction ft = manager.beginTransaction();
		ft.replace(R.id.contentPanel, fragment);
		ft.commit();

	}

}
