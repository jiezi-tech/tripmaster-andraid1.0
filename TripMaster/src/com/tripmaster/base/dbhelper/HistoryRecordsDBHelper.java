package com.tripmaster.base.dbhelper;

import java.util.List;

import com.trip.trainnumber.entity.dao.DaoSession;
import com.trip.trainnumber.entity.dao.HistoryRecords;
import com.trip.trainnumber.entity.dao.HistoryRecordsDao.Properties;
import com.tripmaster.GreenDaoApplication;

import de.greenrobot.dao.Property;
import de.greenrobot.dao.query.QueryBuilder;

import android.content.Context;

public class HistoryRecordsDBHelper {
    private static Context mContext;

    private static HistoryRecordsDBHelper instance;

    private static DaoSession daoSession;

    private HistoryRecordsDBHelper() {
    }

    public static HistoryRecordsDBHelper getInstance(Context context) {
        if (instance == null) {
            instance = new HistoryRecordsDBHelper();
            if (mContext == null) {
                mContext = context;
            }

            // 数据库对象
            daoSession = GreenDaoApplication.getDaoSession();
        }
        return instance;
    }
    
    public long getHistoryRecordsTableCount() {
    	return daoSession.getHistoryRecordsDao().count();

	}
    
    public void addHistoryRecordToTable(HistoryRecords entity) {
		// TODO Auto-generated method stub
    	daoSession.getHistoryRecordsDao().insert(entity);

	}
    
    /***插入集合对象**/
    public void addHistoryRecordListToTable(List<HistoryRecords> entities) {
		// TODO Auto-generated method stub
    	daoSession.getHistoryRecordsDao().insertInTx(entities);

	}
    
    /***返回所有集合对象
     * @return 
     * ***/
    public List<HistoryRecords> getHistoryRecords() {
		// TODO Auto-generated method stub
    	return daoSession.getHistoryRecordsDao().loadAll();
	}
    

    /***返回已经排好序的所有集合对象
     * @return 
     * ***/
    public List<HistoryRecords> getHistoryRecordsList(Property property) {
    	QueryBuilder<HistoryRecords> queryBuilder = daoSession.getHistoryRecordsDao().queryBuilder();
    	return queryBuilder.orderDesc(property).limit(10).list();
	}
    
    public void deleteAll() {
    	daoSession.getHistoryRecordsDao().deleteAll();
	}

}
