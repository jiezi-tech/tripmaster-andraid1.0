package com.tripmaster.base.dbhelper;

import java.util.List;

import com.trip.trainnumber.entity.dao.DaoSession;
import com.trip.trainnumber.entity.dao.TrainNumber;
import com.trip.trainnumber.entity.dao.TrainNumberDao.Properties;
import com.tripmaster.GreenDaoApplication;
import com.tripmaster.util.Constant;

import de.greenrobot.dao.query.QueryBuilder;
import android.content.Context;

public class TrainNumberDBHelper {
	private static TrainNumberDBHelper instance;

	private static DaoSession daoSession;

	private TrainNumberDBHelper() {
	}

	public static TrainNumberDBHelper getInstance() {
		if (instance == null) {
			instance = new TrainNumberDBHelper();

			// 数据库对象
			daoSession = GreenDaoApplication.getDaoSession();
		}
		return instance;
	}

	public void addTrainNumberToTable(TrainNumber entity) {
		// TODO Auto-generated method stub
		entity.setUpdateTime(Constant.formatCurrentString());//设置更新时间
		daoSession.getTrainNumberDao().insert(entity);
	}


	/**
	 * 往数据库中插入集合对象
	 * 
	 * @return
	 */
	public void addTrainNumberListToTable(List<TrainNumber> entities) {
		daoSession.getTrainNumberDao().insertInTx(entities);

	}

	/**
	 * 查询表所有数据
	 * 
	 * @return
	 */
	public List<TrainNumber> getTrainNumberList() {
		QueryBuilder<TrainNumber> queryBuilder = daoSession.getTrainNumberDao()
				.queryBuilder();

		return queryBuilder.list();
	}

	/**
	 * 查询表所有数据
	 * 
	 * @return
	 */
	public List<TrainNumber> getTrainNumber() {
		// TODO Auto-generated method stub
		return daoSession.getTrainNumberDao().loadAll();

	}

	/**
	 * 是否是始发以及终点站
	 * 
	 * @return 存在返回true  否则返回false
	 */
	public boolean isSaveTrain_no(String train_no, String station_train_code) {
		QueryBuilder<TrainNumber> db = daoSession.getTrainNumberDao().queryBuilder();
		db.where(Properties.Train_no.eq(train_no));
		if(db.list().size() > 0){//查找车次编号是否已经在数据库中存储过
			String code = db.list().get(0).getStation_train_code();
			if(!code.contains(station_train_code)){//判断车次是否已经在拼接过
				code += "/"+station_train_code;
				db.list().get(0).setStation_train_code(code);//将拼接好的车次进行重新存储
				db.list().get(0).setUpdateTime(Constant.formatCurrentString());//设置更新
			}
			return true;//存储过
		}
		return false;//未存储

	}

}
