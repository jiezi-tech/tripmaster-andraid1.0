package com.tripmaster.base.dbhelper;

import java.util.List;

import android.database.sqlite.SQLiteDatabase;

import com.trip.trainnumber.entity.dao.DaoSession;
import com.trip.trainnumber.entity.dao.TrainNumberTrainsitSearchOtherResult;
import com.tripmaster.GreenDaoApplication;

import de.greenrobot.dao.query.QueryBuilder;

public class TrainNumberTrainsitSearchOtherResultDBHelper {
	private static TrainNumberTrainsitSearchOtherResultDBHelper instance;
	private static DaoSession daoSession;
	
	private TrainNumberTrainsitSearchOtherResultDBHelper() {
		// TODO Auto-generated constructor stub
	}
	
	public static TrainNumberTrainsitSearchOtherResultDBHelper getInstance() {
		if (instance == null) {
			instance = new TrainNumberTrainsitSearchOtherResultDBHelper();
			// 数据库对象
			daoSession = GreenDaoApplication.getDaoSession();
		}
		return instance;
	}
	
	public void deleteAll() {
		daoSession.getTrainNumberTrainsitSearchOtherResultDao().deleteAll();
	}
	
	public long getCount() {
		return daoSession.getTrainNumberTrainsitSearchOtherResultDao().count();
	}
	
	public void trainNumberTrainsitSearchOtherResultSave() {
		String sql = "insert into TRAIN_NUMBER_TRAINSIT_SEARCH_OTHER_RESULT(zhongzhuan_city,transferStartCount,transferEndCount," +
				"minMileage,maxMileage,minTime,maxTime,minPrice,maxPrice) " +
				"SELECT zhongzhuan_city," +
				"COUNT(distinct transfer_start_train_code) AS transferStartCount," +
				"count(distinct transfer_end_train_code ) AS transferEndCount," +
				"min(total_mileage) AS minMileage," +
				"max(total_mileage) AS maxMileage," +
				"min(total_time) AS minTime," +
				"max(total_time) AS maxTime," +
				"min(first_price+second_price) AS minPrice," +
				"max(first_price+second_price) AS maxPrice " +
				"from TRAIN_NUMBER_TRANSIT_SEARCH_RESULT " +
				"group by zhongzhuan_city";
		SQLiteDatabase db = daoSession.getTrainNumberTrainsitSearchOtherResultDao().getDatabase();
		db.execSQL(sql);
		
	}
	
	public List<TrainNumberTrainsitSearchOtherResult> loadAll() {
		return daoSession.getTrainNumberTrainsitSearchOtherResultDao().loadAll();
	}
	
	 public List<TrainNumberTrainsitSearchOtherResult> query(int offset,int limit) {
		 QueryBuilder<TrainNumberTrainsitSearchOtherResult> qb = daoSession.getTrainNumberTrainsitSearchOtherResultDao().queryBuilder();
		 qb.limit(limit);
		 qb.offset(offset);
		 return qb.list();
	}
	

}
