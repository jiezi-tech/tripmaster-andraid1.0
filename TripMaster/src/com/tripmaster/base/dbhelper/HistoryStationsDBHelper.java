package com.tripmaster.base.dbhelper;

import java.util.List;

import com.trip.trainnumber.entity.dao.DaoSession;
import com.trip.trainnumber.entity.dao.HistoryStations;
import com.trip.trainnumber.entity.dao.HistoryStationsDao.Properties;
import com.tripmaster.GreenDaoApplication;

import de.greenrobot.dao.Property;
import de.greenrobot.dao.query.QueryBuilder;
import android.content.Context;
import android.util.Log;

public class HistoryStationsDBHelper {
    private static Context mContext;

    private static HistoryStationsDBHelper instance;

    private static DaoSession daoSession;

    private HistoryStationsDBHelper() {
    }

    public static HistoryStationsDBHelper getInstance(Context context) {
        if (instance == null) {
            instance = new HistoryStationsDBHelper();
            if (mContext == null) {
                mContext = context;
            }

            // 数据库对象
            daoSession = GreenDaoApplication.getDaoSession();
        }
        return instance;
    }
    
    public long getHistoryStationsTableCount() {
    	return daoSession.getHistoryStationsDao().count();

	}
    
    public void addHistoryStationToTable(HistoryStations entity) {
		// TODO Auto-generated method stub
    	deleteHistoryStations(entity.getTation_name());
    	daoSession.getHistoryStationsDao().insert(entity);

	}
    
    /***插入集合对象**/
    public void addHistoryStationListToTable(List<HistoryStations> entities) {
		// TODO Auto-generated method stub
    	daoSession.getHistoryStationsDao().insertInTx(entities);

	}
    
    /***返回所有集合对象
     * @return 
     * ***/
    public List<HistoryStations> getHistoryStations() {
		// TODO Auto-generated method stub
    	return daoSession.getHistoryStationsDao().loadAll();
	}
    
    public void deleteHistoryStations(String tationName) {
    	Log.d("#########", "######HistoryStationsDBHelper-deleteHistoryStations-1");
    	QueryBuilder<HistoryStations> db = daoSession.getHistoryStationsDao().queryBuilder();
    	Log.d("#########", "######HistoryStationsDBHelper-deleteHistoryStations-2");
    	db.where(Properties.Tation_name.eq(tationName));
    	if(db.list().size() > 0){
    		Log.d("#########", "######HistoryStationsDBHelper-deleteHistoryStations-3"+db.list().get(0).getTation_name());
    		daoSession.getHistoryStationsDao().delete(db.list().get(0));
    	}
    	Log.d("#########", "######HistoryStationsDBHelper-deleteHistoryStations-4");

	}
    

    /***返回已经排好序的所有集合对象
     * @return 
     * ***/
    public List<HistoryStations> getHistoryStationsList(Property property) {
    	QueryBuilder<HistoryStations> queryBuilder = daoSession.getHistoryStationsDao().queryBuilder();
    	queryBuilder.limit(10);
    	return queryBuilder.orderAsc(property).list();
	}
    
    public void deleteAll() {
    	daoSession.getHistoryStationsDao().deleteAll();

	}

}
