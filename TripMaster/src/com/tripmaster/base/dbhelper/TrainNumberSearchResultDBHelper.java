package com.tripmaster.base.dbhelper;

import java.util.List;

import com.trip.trainnumber.entity.dao.DaoSession;
import com.trip.trainnumber.entity.dao.TrainNumberSearchResult;
import com.trip.trainnumber.entity.dao.TrainNumberSearchResultDao.Properties;
import com.tripmaster.GreenDaoApplication;

import de.greenrobot.dao.Property;
import de.greenrobot.dao.query.QueryBuilder;

public class TrainNumberSearchResultDBHelper {
	private static DaoSession daoSession;

	private TrainNumberSearchResultDBHelper() {
	}

	private static TrainNumberSearchResultDBHelper instance;

	public static TrainNumberSearchResultDBHelper getInstance() {
		if (instance == null) {
			instance = new TrainNumberSearchResultDBHelper();
			// 数据库对象
			daoSession = GreenDaoApplication.getDaoSession();
		}
		return instance;
	}

	/**
	 * 删除所有
	 * **/
	public void deleteAll() {
		daoSession.getTrainNumberSearchResultDao().deleteAll();
	}
	
	/**
	 * 保存搜索返回的车次列表
	 * **/
	public void saveTrainNumberSearchResult(List<TrainNumberSearchResult> results) {
		daoSession.getTrainNumberSearchResultDao().insertInTx(results);
	}

	/**
	 * 返回所有
	 * **/
	public List<TrainNumberSearchResult> getAllSearchResulst() {
		return daoSession.getTrainNumberSearchResultDao().loadAll();
	}

	/**
	 * 按车次返回车次对象
	 * **/
	public TrainNumberSearchResult getTrainNumberSearchResult(
			TrainNumberSearchResult searchResult) {
		QueryBuilder<TrainNumberSearchResult> qb = daoSession
				.getTrainNumberSearchResultDao().queryBuilder();
		qb.where(Properties.Train_no.eq(searchResult.getTrain_no()));
		qb.unique();
		if (qb.list().size() > 0) {
			return qb.list().get(0);
		}
		return null;
	}
	
	/**
	 * 更新公里数
	 * **/
	public void updateAllMileage(TrainNumberSearchResult searchResult) {
		TrainNumberSearchResult ts = getTrainNumberSearchResult(searchResult);
		if (ts != null) {
			ts.setAllMileage(searchResult.getAllMileage());
		}
	}

	/**
	 * 更新价格
	 * **/
	public void updatePrice(TrainNumberSearchResult searchResult) {
		TrainNumberSearchResult ts = getTrainNumberSearchResult(searchResult);
		if (ts != null) {
			ts.setGg_price(searchResult.getGg_price());
			ts.setGr_price(searchResult.getGr_price());
			ts.setQt_price(searchResult.getQt_price());
			ts.setRw_price(searchResult.getRw_price());
			ts.setRz_price(searchResult.getRz_price());
			ts.setTz_price(searchResult.getTz_price());
			ts.setWz_price(searchResult.getWz_price());
			ts.setYb_price(searchResult.getYb_price());
			ts.setYw_price(searchResult.getYw_price());
			ts.setYz_price(searchResult.getYz_price());
			ts.setZe_price(searchResult.getZe_price());
			ts.setZy_price(searchResult.getZy_price());
			ts.setSwz_price(searchResult.getSwz_price());

			daoSession.getTrainNumberSearchResultDao().update(ts);// 更新价格信息
		}
	}
	
	/**
	 * 按某个属性升序
	 * **/
	public List<TrainNumberSearchResult> orderAsValue(Property property) {
		QueryBuilder<TrainNumberSearchResult> qb = daoSession
				.getTrainNumberSearchResultDao().queryBuilder();
		qb.orderAsc(property);
		return qb.list();
	}

	/**
	 * 按某个属性降序
	 * **/
	public List<TrainNumberSearchResult> orderDescValue(Property property) {
		QueryBuilder<TrainNumberSearchResult> qb = daoSession
				.getTrainNumberSearchResultDao().queryBuilder();
		qb.orderDesc(property);
		return qb.list();
	}
	
	/**
	 * 获取某种车次的类型
	 * **/
	public List<TrainNumberSearchResult> getTrainNumberSearchResult(String trainClassName) {
		QueryBuilder<TrainNumberSearchResult> qb = daoSession
				.getTrainNumberSearchResultDao().queryBuilder();
		qb.where(Properties.Station_train_code.like(trainClassName));
		return qb.list();
	}
	
}
