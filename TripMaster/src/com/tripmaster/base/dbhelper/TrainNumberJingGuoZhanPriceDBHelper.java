package com.tripmaster.base.dbhelper;

import java.util.List;

import com.trip.trainnumber.entity.dao.DaoSession;
import com.trip.trainnumber.entity.dao.TrainNumberJingGuoZhanPrice;
import com.trip.trainnumber.entity.dao.TrainNumberJingGuoZhanPriceDao;
import com.trip.trainnumber.entity.dao.TrainNumberJingGuoZhanPriceDao.Properties;
import com.trip.trainnumber.entity.dao.TrainStation;
import com.tripmaster.GreenDaoApplication;

import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;
import de.greenrobot.dao.query.WhereCondition.StringCondition;

import android.content.Context;
import android.graphics.Paint.Join;
import android.util.Log;

public class TrainNumberJingGuoZhanPriceDBHelper {

    private static TrainNumberJingGuoZhanPriceDBHelper instance;

    private static DaoSession daoSession;

    private TrainNumberJingGuoZhanPriceDBHelper() {
    }

    public static TrainNumberJingGuoZhanPriceDBHelper getInstance() {
        if (instance == null) {
            instance = new TrainNumberJingGuoZhanPriceDBHelper();
            // 数据库对象
            daoSession = GreenDaoApplication.getDaoSession();

        }
        return instance;
    }
    
    public void addTrainNumberJingGuoZhanPriceToTable(TrainNumberJingGuoZhanPrice entity) {
		// TODO Auto-generated method stub
    	daoSession.getTrainNumberJingGuoZhanPriceDao().insert(entity);
	}

	
	/**插入集合价格**/
	public void insertInTx(List<TrainNumberJingGuoZhanPrice> entities) {
		daoSession.getTrainNumberJingGuoZhanPriceDao().insertInTx(entities);
	}
	
	public void deleteAll() {
		daoSession.getTrainNumberJingGuoZhanPriceDao().deleteAll();
	}
	
	/**
	 * 中转转后查询
	 * @param fromStation 出发站
	 * @param toStation 到达站
	 * @return 查询出来符合条件的结果集
	 * **/
	public List<TrainNumberJingGuoZhanPrice> queryTransitFilterAfter(TrainStation fromStation, TrainStation toStation) {
		TrainNumberJingGuoZhanPriceDao dao = daoSession.getTrainNumberJingGuoZhanPriceDao();
		
		String sql=", TRAIN_NUMBER_JING_GUO_ZHAN_PRICE sd " +
				"WHERE T.start_city_name = sd.end_city_name " +
				"AND sd.start_city_name = ? AND T.end_city_name = ? group by T.station_train_code,T.from_station_no,T.to_station_no";
		
		Query<TrainNumberJingGuoZhanPrice> query = dao.queryRawCreate(sql,
				new String[]{fromStation.getCity_name(),toStation.getCity_name()});
		return query.list();
	}

	/**
	 * 中转转前查询
	 * @param fromStation 出发站
	 * @param toStation 到达站
	 * @return 查询出来符合条件的结果集
	 * **/
	public List<TrainNumberJingGuoZhanPrice> queryTransitFilterBefore(TrainStation fromStation, TrainStation toStation) {
		TrainNumberJingGuoZhanPriceDao dao = daoSession.getTrainNumberJingGuoZhanPriceDao();
		
		String sql=", TRAIN_NUMBER_JING_GUO_ZHAN_PRICE sd " +
				"WHERE sd.start_city_name = T.end_city_name " +
				"AND T.start_city_name = ? AND sd.end_city_name = ? group by T.station_train_code,T.from_station_no,T.to_station_no";
		
		Query<TrainNumberJingGuoZhanPrice> query = dao.queryRawCreate(sql,
				new String[]{fromStation.getCity_name(),toStation.getCity_name()});
		return query.list();
	}
	
	/***
	 * 根据车次、起始站、终点站 返回当前车次对象
	 * @param station_train_code 车次
	 * @param start_city_name 起始城市
	 * @param end_city_name 终到城市
	 * @return 当前车次对象
	 * **/
	public TrainNumberJingGuoZhanPrice queryTrainNumber(String station_train_code,String start_city_name,String end_city_name) {
		QueryBuilder<TrainNumberJingGuoZhanPrice> qb = daoSession.getTrainNumberJingGuoZhanPriceDao().queryBuilder();
		qb.where(Properties.Station_train_code.eq(station_train_code), Properties.Start_city_name.eq(start_city_name),
				Properties.End_city_name.eq(end_city_name));
		if(qb.list().size() > 0){
			return qb.list().get(0);
		}
		return null;
	}
	
	/**
	 * 获取某趟车次的所有经过站集合
	 * @param station_train_code 车次
	 * @return 经过站集合
	 * **/
	public List<TrainNumberJingGuoZhanPrice> queryTrainNumberJingGuoZhanList(String station_train_code) {
		QueryBuilder<TrainNumberJingGuoZhanPrice> qb = daoSession.getTrainNumberJingGuoZhanPriceDao().queryBuilder();
		qb.where(Properties.Station_train_code.eq(station_train_code),Properties.From_station_no.eq("01"));
		return qb.list();
	}
	
	
    
}
