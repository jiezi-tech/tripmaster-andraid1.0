package com.tripmaster.base.dbhelper;

import java.util.List;

import com.trip.trainnumber.entity.dao.DaoSession;
import com.trip.trainnumber.entity.dao.TrainStation;
import com.trip.trainnumber.entity.dao.TrainStationDao.Properties;
import com.tripmaster.GreenDaoApplication;

import de.greenrobot.dao.query.QueryBuilder;

import android.content.Context;

public class TrainStationDBHelper {
    private static TrainStationDBHelper instance;

    private static DaoSession daoSession;

    private TrainStationDBHelper() {
    }

    public static TrainStationDBHelper getInstance() {
        if (instance == null) {
            instance = new TrainStationDBHelper();

            // 数据库对象
            daoSession = GreenDaoApplication.getDaoSession();
        }
        return instance;
    }
    
    public long getTrainStationTableCount() {
    	return daoSession.getTrainStationDao().count();

	}
    
    public void addTrainStationToTable(TrainStation entity) {
		// TODO Auto-generated method stub
    	daoSession.getTrainStationDao().insert(entity);

	}
    
    /***插入集合对象**/
    public void addTrainStationListToTable(List<TrainStation> entities) {
		// TODO Auto-generated method stub
    	daoSession.getTrainStationDao().insertInTx(entities);

	}
    
    /***返回所有集合对象
     * @return 
     * ***/
    public List<TrainStation> getTrainStation() {
		// TODO Auto-generated method stub
    	return daoSession.getTrainStationDao().loadAll();
	}

    
    /***返回已经排好序的所有集合对象
     * @return 
     * ***/
    public List<TrainStation> getTrainStationList() {
    	QueryBuilder<TrainStation> queryBuilder = daoSession.getTrainStationDao().queryBuilder();
    	return queryBuilder.orderAsc(Properties.Pinyin).list();

	}
    
    public void getTrainStationName(String name) {

	}
    
    /**
     * 根据站名获取站对象
     * @param stationName 站名
     * 
     * **/
    public TrainStation getTrainStationByStationName(String stationName) {
    	QueryBuilder<TrainStation> qb = daoSession.getTrainStationDao().queryBuilder();
    	qb.where(Properties.Station_name.eq(stationName));
    	if(qb.list().size() > 0){
    		return qb.list().get(0);
    	}
    	return null;
	}

}
