package com.tripmaster.base.dbhelper;

import java.util.List;

import android.database.sqlite.SQLiteDatabase;

import com.trip.trainnumber.entity.dao.DaoSession;
import com.trip.trainnumber.entity.dao.TrainNumberSearchResult;
import com.trip.trainnumber.entity.dao.TrainNumberTransitSearchResult;
import com.trip.trainnumber.entity.dao.TrainNumberTransitSearchResultDao.Properties;
import com.tripmaster.GreenDaoApplication;

import de.greenrobot.dao.Property;
import de.greenrobot.dao.query.QueryBuilder;
import de.greenrobot.dao.query.WhereCondition.StringCondition;

public class TrainNumberTransitSearchResultDBHelper {
	public static String ORDER_ASC = "asc";
	public static String ORDER_DESC = "desc";
	private static DaoSession daoSession;
	
	private static TrainNumberTransitSearchResultDBHelper instance;
	
	private TrainNumberTransitSearchResultDBHelper() {
		// TODO Auto-generated constructor stub
	}
	
	public static TrainNumberTransitSearchResultDBHelper getInstance() {
		if (instance == null) {
			instance = new TrainNumberTransitSearchResultDBHelper();
			// 数据库对象
			daoSession = GreenDaoApplication.getDaoSession();
		}
		return instance;
	}
	
	/**删除所有**/
	public void deleteAll() {
		daoSession.getTrainNumberTransitSearchResultDao().deleteAll();
	}
	
	
	/** 返回数据库所有数据**/
	public List<TrainNumberTransitSearchResult> queryAll() {
		return daoSession.getTrainNumberTransitSearchResultDao().loadAll();
	}
	
	/***
	 * 根据所给的出发城市以及终到城市 将中转的线路保存到查询结果表中
	 * @param startCity 出发城市
	 * @param endCity 终到城市
	 * **/
	public void trainNumberTransitSave(String startCity,String endCity) {
		
		String sql="insert into TRAIN_NUMBER_TRANSIT_SEARCH_RESULT('zhongzhuan_city','start_time','arrive_time','end_day','total_time'," +
				"'total_train_time','total_mileage','transfer_time','transfer_start_station','transfer_arrive_time'," +
				"'transfer_arrive_day','transfer_start_train_code','first_total_time','first_total_mileage','first_price'," +
				"'first_price_seat_type','transfer_end_station','transfer_start_time','transfer_start_day','transfer_end_train_code'," +
				"'second_total_time','second_total_mileage','second_price','second_price_seat_type') " +
				"SELECT ed.start_city_name AS zhongzhuan_city,sd.start_time,ed.arrive_time," +
				"ROUND( ( sd.start_hour * 60 + sd.start_minute + sd.alltime + ed.alltime +" +
				"( CASE WHEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) < 0" +
				" THEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) + 1440 " +
				"ELSE ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute )END )  ) / 24 / 60 ) AS end_day," +
				"sd.alltime + ed.alltime +( CASE WHEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) < 0" +
				" THEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) + 1440 " +
				"ELSE ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute )END ) AS total_time," +
				"sd.alltime + ed.alltime AS total_train_time, sd.allmileage + ed.allmileage AS total_mileage," +
				"( CASE WHEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) < 0" +
				" THEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) + 1440 " +
				"ELSE ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute )END ) AS transfer_time," +
				" sd.end_station_name AS transfer_start_station, sd.arrive_time AS transfer_arrive_time," +
				"ROUND( ( sd.start_hour * 60 + sd.start_minute + sd.alltime  ) / 24 / 60 ) AS transfer_arrive_day," +
				"sd.station_train_code AS transfer_start_train_code,sd.alltime AS first_total_time," +
				"sd.allmileage AS first_total_mileage,CASE WHEN sd.yz_price <> '' THEN sd.yz_price " +
				"WHEN sd.yw_price <> '' THEN sd.yw_price " +
				"WHEN sd.rw_price <> '' THEN sd.rw_price ELSE sd.ze_price END AS first_price," +
				"CASE WHEN sd.yz_price <> '' THEN '硬座' WHEN sd.yw_price <> '' THEN '硬卧' " +
				"WHEN sd.rw_price <> '' THEN '软卧' ELSE '二等座' END AS first_price_seat_type," +
				"ed.start_station_name AS transfer_end_station,ed.start_time AS transfer_start_time," +
				"ROUND( ( substr( sd.start_time, 1, 2 ) * 60 + substr( sd.start_time, 4 ) + sd.alltime +" +
				"( CASE WHEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) < 0" +
				" THEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) + 1440 " +
				"ELSE ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) END )  ) / 24 / 60 ) AS transfer_start_day," +
				"ed.station_train_code AS transfer_end_train_code,ed.alltime AS second_total_time,ed.allmileage AS second_total_mileage," +
				" CASE WHEN ed.yz_price <> '' THEN ed.yz_price WHEN ed.yw_price <> '' THEN ed.yw_price " +
				"WHEN ed.rw_price <> '' THEN ed.rw_price ELSE ed.ze_price END AS second_price," +
				"CASE WHEN ed.yz_price <> '' THEN '硬座' " +
				"WHEN ed.yw_price <> '' THEN '硬卧' " +
				"WHEN ed.rw_price <> '' THEN '软卧' ELSE '二等座' END AS second_price_seat_type " +
				"FROM TRAIN_NUMBER_JING_GUO_ZHAN_PRICE sd, TRAIN_NUMBER_JING_GUO_ZHAN_PRICE ed " +
				"WHERE ed.start_city_name = sd.end_city_name AND sd.start_city_name = ? " +
				"AND ed.end_city_name = ? ORDER BY sd.alltime + ed.alltime +" +
				"( CASE WHEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) < 0" +
				" THEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) + 1440" +
				" ELSE ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute )END )  ASC ";
	
		SQLiteDatabase db = daoSession.getTrainNumberJingGuoZhanPriceDao().getDatabase();
		db.execSQL(sql,new String[]{startCity,endCity});
		deletePriceIsNull();//执行完中转语句之后排除不合法的数据
	}
	
	/**
	 * 删除价格为空的数据 方便计算
	 * */
	public void deletePriceIsNull() {
		QueryBuilder<TrainNumberTransitSearchResult> qb = daoSession.getTrainNumberTransitSearchResultDao().queryBuilder();
		qb.whereOr(Properties.Second_price.isNull(), Properties.Second_price.eq(""), Properties.First_price.isNull()
				,Properties.First_price.eq(""));
		if(qb.list().size() > 0){
			daoSession.getTrainNumberTransitSearchResultDao().deleteInTx(qb.list());
		}
	}
	
	/**
	 * 分页查询
	 * @param offset 偏移量
	 * @param limit 每页数量
	 * **/
	public List<TrainNumberTransitSearchResult> query(int offset,int limit) {
		QueryBuilder<TrainNumberTransitSearchResult> qb = daoSession.getTrainNumberTransitSearchResultDao().queryBuilder();
		qb.limit(limit);
		qb.offset(offset);
		return qb.list();
	}
	
	/**
	 * 获取总条数
	 * **/
	public long getCount() {
		return daoSession.getTrainNumberTransitSearchResultDao().count();
	}
	
	/**
	 * 按某个属性排序，默认升序
	 * **/
	public List<TrainNumberTransitSearchResult> orderAsValue(Property property,int offset,int limit) {
		return orderAsValue(property, ORDER_ASC,offset,limit);
	}
	
	/**
	 * 按某个属性排序
	 * **/
	public List<TrainNumberTransitSearchResult> orderAsValue(Property property, String orderStyle,int offset,int limit) {
		QueryBuilder<TrainNumberTransitSearchResult> qb = daoSession
				.getTrainNumberTransitSearchResultDao().queryBuilder();
		if(orderStyle.equals(ORDER_ASC)){
			qb.orderAsc(property);
		}else{
			qb.orderDesc(property);
		}
		qb.limit(limit);
		qb.offset(offset);
		return qb.list();
	}
	
	public List<TrainNumberTransitSearchResult> orderPrice(int offset,int limit) {
		QueryBuilder<TrainNumberTransitSearchResult> qb = daoSession
				.getTrainNumberTransitSearchResultDao().queryBuilder();
		qb.orderAsc(Properties.Second_price,Properties.First_price);
		qb.limit(limit);
		qb.offset(offset);
		return qb.list();
	}

	
}
