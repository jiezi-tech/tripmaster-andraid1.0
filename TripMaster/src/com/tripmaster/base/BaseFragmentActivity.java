package com.tripmaster.base;

import com.tripmaster.R;
import com.tripmaster.traintickets.ResultListFilterActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Administrator on 2015/4/27.
 */
public class BaseFragmentActivity extends FragmentActivity{
    private ImageView backIv;
    private TextView titleTv;
    private LinearLayout secordTitleTv;
    private TextView secordTitleTvBig;
    private TextView secordTitleTvSmall;
    public TextView filterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    
    public void initTitle(String str,boolean isHasBack) {
    	if(isHasBack){
    		backIv = (ImageView) findViewById(R.id.backIv);
    		backIv.setVisibility(View.VISIBLE);
    		backIv.setOnClickListener(new OnClickListener() {
    			
    			@Override
    			public void onClick(View v) {
    				finish();				
    			}
    		});
    	}
    	titleTv = (TextView) findViewById(R.id.titleTv);
    	titleTv.setText(str);

	}
    
    public void initTitle(String textBig,String textSmall, boolean isHasBack,boolean isFilter) {
    	titleTv = (TextView) findViewById(R.id.titleTv);
    	titleTv.setVisibility(View.GONE);
    	if(isHasBack){
    		backIv = (ImageView) findViewById(R.id.backIv);
    		backIv.setVisibility(View.VISIBLE);
    		backIv.setOnClickListener(new OnClickListener() {
    			
    			@Override
    			public void onClick(View v) {
    				finish();				
    			}
    		});
    	}
//    	if(isFilter){
//    		filterButton =  (TextView) findViewById(R.id.filter);
//    		filterButton.setVisibility(View.VISIBLE);
//    		filterButton.setOnClickListener(new OnClickListener() {
//    			
//    			@Override
//    			public void onClick(View v) {
//    				Intent intent = new Intent(getApplication(),ResultListFilterActivity.class);
//    				startActivityForResult(intent, 1);
//    			}
//    		});
//    	}
    	
    	secordTitleTv = (LinearLayout) findViewById(R.id.secondTitleTv);
    	secordTitleTv.setVisibility(View.VISIBLE);
    	secordTitleTvBig = (TextView) findViewById(R.id.secondTitleTvBig);
    	secordTitleTvBig.setText(textBig);
    	secordTitleTvSmall = (TextView) findViewById(R.id.secondTitleTvSmall);
    	secordTitleTvSmall.setText(textSmall);
	}
}
