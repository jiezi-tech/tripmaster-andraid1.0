package com.tripmaster.base;

import com.tripmaster.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Administrator on 2015/4/27.
 */
public class BaseActivity extends Activity{
    protected int screenWidth;//屏幕宽
    protected int screenHeight;//屏幕高
    protected ImageView backIv;
    protected TextView titleTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        screenHeight = getResources().getDisplayMetrics().heightPixels;
        screenWidth = getResources().getDisplayMetrics().widthPixels;
    }
    
    public void initTitle(String str) {
    	backIv = (ImageView) findViewById(R.id.backIv);
    	backIv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();				
			}
		});
    	titleTv = (TextView) findViewById(R.id.titleTv);
    	titleTv.setText(str);

	}
}
