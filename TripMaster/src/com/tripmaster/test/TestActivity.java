package com.tripmaster.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONReader;
import com.alibaba.fastjson.JSONWriter;
import com.alibaba.fastjson.TypeReference;
import com.trip.trainnumber.entity.dao.TrainNumberJingGuoZhanPrice;
import com.tripmaster.R;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class TestActivity extends Activity implements OnClickListener{
	private Button exportPriceBtn;
	private GuoZhanPriceHelper ph;
	private final String priceFileDirectoryName ="jingguozhanprice";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_test);
		
		exportPriceBtn = (Button)findViewById(R.id.exportPriceBtn);
		exportPriceBtn.setOnClickListener(this);
		
		ph = GuoZhanPriceHelper.getInstance();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.exportPriceBtn:
			ph.query();
//			new Thread(new Runnable() {
//				
//				@Override
//				public void run() {
//					importPrice();//导入价格表
////					exportPrice();//导出价格表
//				}
//			}).start();
			break;

		default:
			break;
		}
	}
	
	private void importPrice() {
		long startTime = System.currentTimeMillis();
		ph.deleteAll();
		try {
			String[] paths = getAssets().list(priceFileDirectoryName);
			for(String s: paths){
				String fileName =priceFileDirectoryName+"/"+s;
				System.out.println(fileName);
				List<TrainNumberJingGuoZhanPrice> list = readFile(fileName);
				Log.i("lanou",""+ list.size());
				if(list != null && list.size() > 0){
					ph.insertInTx(list);//保存到数据库
				}
			}
			long endTime = System.currentTimeMillis();
			System.out.println("读取结束  耗时："+(endTime - startTime));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/***
	 * 
	 * 将价格表中的所有数据均导出成文件
	 * **/
	private void exportPrice() {
		
		long count = ph.getCount();
		Log.i("lanou", "数据长度---->>"+count);
		if(count == 0)
			return;
		
		long startTime = System.currentTimeMillis();
		int limit = 5000;
		int offset = 0;
		long pagerSize = count / limit;
		if(count % limit != 0)
			pagerSize += 1;
		Log.i("lanou", "总页数---->>"+pagerSize);
		
		File file = getFilesDir();
		File priceFiles = new File(file.getAbsolutePath()+"/"+priceFileDirectoryName);
		
		//删除该目录下的所有文件
		if(priceFiles.exists() && priceFiles.isDirectory()){
			File[] files = priceFiles.listFiles();
			for(File f : files){//先遍历文件删除
				f.delete();
			}
			priceFiles.deleteOnExit();//再删除目录
		}
		
		priceFiles.mkdir();//重新生成目录
		
		for(int i=0;i<pagerSize;i++){
			offset = i * limit;
			List<TrainNumberJingGuoZhanPrice> prices = ph.getTrainNumberJingGuoZhanPrice(offset, limit);
			Log.i("lanou", prices.get(0).getId()+"  "+prices.get(prices.size() -1).getId());
			String path =new String(priceFiles.getAbsolutePath()+"/JingGuoZhanPrice_"+i+".json");
			jsonWriteFile(prices, path);
		}
		
		long endTime = System.currentTimeMillis();
		
		System.out.println("价格表导出完毕 耗时："+(endTime - startTime)+" 秒");
	}
	
	public static void jsonWriteFile(List<TrainNumberJingGuoZhanPrice> prices, String filename) {
		try {
			JSONWriter writer = new JSONWriter(new FileWriter(filename));
	        writer.writeObject(prices);
	        writer.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private List<TrainNumberJingGuoZhanPrice> readFile(String filename) {

		try {
			InputStream is = getAssets().open(filename);

			InputStreamReader isr = new InputStreamReader(is, "utf-8");
			BufferedReader reader = new BufferedReader(isr);
			StringBuffer buffer = new StringBuffer();
			String line = null;
			while((line = reader.readLine())  != null){
				buffer.append(line);
			}
			List<TrainNumberJingGuoZhanPrice> list = JSON.parseArray(
					buffer.toString(),TrainNumberJingGuoZhanPrice.class);

	        reader.close();
	        isr.close();
	        is.close();
	        
			return list;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
