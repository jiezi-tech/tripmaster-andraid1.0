package com.tripmaster.test;

import java.util.List;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.trip.trainnumber.entity.dao.DaoSession;
import com.trip.trainnumber.entity.dao.TrainNumberJingGuoZhanPrice;
import com.tripmaster.GreenDaoApplication;

import de.greenrobot.dao.query.QueryBuilder;

public class GuoZhanPriceHelper {
	private static GuoZhanPriceHelper helper;

	private static DaoSession daoSession;

	private GuoZhanPriceHelper() {
		// TODO Auto-generated constructor stub
	}

	public static GuoZhanPriceHelper getInstance() {
		if (helper == null) {
			helper = new GuoZhanPriceHelper();
			// 数据库对象
			daoSession = GreenDaoApplication.getDaoSession();
		}
		return helper;
	}
	
	/**返回当前数据库数据长度**/
	public long getCount() {
		return daoSession.getTrainNumberJingGuoZhanPriceDao().count();
	}
	
	/**
	 * 返回限定条数的价格集合对象
	 * @author lilyxiao
	 * @param offset 偏移量
	 * @param limit 限制条数
	 * **/
	public List<TrainNumberJingGuoZhanPrice> getTrainNumberJingGuoZhanPrice(int offset, int limit) {
		QueryBuilder<TrainNumberJingGuoZhanPrice> qb = daoSession.getTrainNumberJingGuoZhanPriceDao().queryBuilder();
		qb.offset(offset);
		qb.limit(limit);
		return qb.list();
	}
	
	/**插入集合价格**/
	public void insertInTx(List<TrainNumberJingGuoZhanPrice> entities) {
		daoSession.getTrainNumberJingGuoZhanPriceDao().insertInTx(entities);
	}
	
	public void deleteAll() {
		daoSession.getTrainNumberJingGuoZhanPriceDao().deleteAll();
	}
	
	public void query() {
		
		String sql="insert into TRAIN_NUMBER_TRANSIT_SEARCH_RESULT('zhongzhuan_city','start_time','arrive_time','end_day','total_time'," +
				"'total_train_time','total_mileage','transfer_time','transfer_start_station','transfer_arrive_time'," +
				"'transfer_arrive_day','transfer_start_train_code','first_total_time','first_total_mileage','first_price'," +
				"'first_price_seat_type','transfer_end_station','transfer_start_time','transfer_start_day','transfer_end_train_code'," +
				"'second_total_time','second_total_mileage','second_price','second_price_seat_type') " +
				"SELECT ed.start_city_name AS zhongzhuan_city,sd.start_time,ed.arrive_time," +
				"ROUND( ( sd.start_hour * 60 + sd.start_minute + sd.alltime + ed.alltime +" +
				"( CASE WHEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) < 0" +
				" THEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) + 1440 " +
				"ELSE ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute )END )  ) / 24 / 60 ) AS end_day," +
				"sd.alltime + ed.alltime +( CASE WHEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) < 0" +
				" THEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) + 1440 " +
				"ELSE ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute )END ) AS total_time," +
				"sd.alltime + ed.alltime AS total_train_time, sd.allmileage + ed.allmileage AS total_mileage," +
				"( CASE WHEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) < 0" +
				" THEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) + 1440 " +
				"ELSE ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute )END ) AS transfer_time," +
				" sd.end_station_name AS transfer_start_station, sd.arrive_time AS transfer_arrive_time," +
				"ROUND( ( sd.start_hour * 60 + sd.start_minute + sd.alltime  ) / 24 / 60 ) AS transfer_arrive_day," +
				"sd.station_train_code AS transfer_start_train_code,sd.alltime AS first_total_time," +
				"sd.allmileage AS first_total_mileage,CASE WHEN sd.yz_price IS NOT NULL THEN sd.yz_price " +
				"WHEN sd.yw_price IS NOT NULL THEN sd.yw_price " +
				"WHEN sd.rw_price IS NOT NULL THEN sd.rw_price ELSE sd.ze_price END AS first_price," +
				"CASE WHEN sd.yz_price IS NOT NULL THEN '硬座' WHEN sd.yw_price IS NOT NULL THEN '硬卧' " +
				"WHEN sd.rw_price IS NOT NULL THEN '软卧' ELSE '二等座' END AS first_price_seat_type," +
				"ed.start_station_name AS transfer_end_station,ed.start_time AS transfer_start_time," +
				"ROUND( ( substr( sd.start_time, 1, 2 ) * 60 + substr( sd.start_time, 4 ) + sd.alltime +" +
				"( CASE WHEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) < 0" +
				" THEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) + 1440 " +
				"ELSE ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) END )  ) / 24 / 60 ) AS transfer_start_day," +
				"ed.station_train_code AS transfer_end_train_code,ed.alltime AS second_total_time,ed.allmileage AS second_total_mileage," +
				" CASE WHEN ed.yz_price IS NOT NULL THEN ed.yz_price WHEN ed.yw_price IS NOT NULL THEN ed.yw_price " +
				"WHEN ed.rw_price IS NOT NULL THEN ed.rw_price ELSE ed.ze_price END AS second_price," +
				"CASE WHEN ed.yz_price IS NOT NULL THEN '硬座' " +
				"WHEN ed.yw_price IS NOT NULL THEN '硬卧' " +
				"WHEN ed.rw_price IS NOT NULL THEN '软卧' ELSE '二等座' END AS second_price_seat_type " +
				"FROM TRAIN_NUMBER_JING_GUO_ZHAN_PRICE sd, TRAIN_NUMBER_JING_GUO_ZHAN_PRICE ed " +
				"WHERE ed.start_city_name = sd.end_city_name AND sd.start_city_name = ? " +
				"AND ed.end_city_name = ? ORDER BY sd.alltime + ed.alltime +" +
				"( CASE WHEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) < 0" +
				" THEN ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute ) + 1440" +
				" ELSE ed.start_hour * 60 + ed.start_minute -( sd.start_hour * 60 + sd.start_minute )END )  ASC ";
	
		SQLiteDatabase db = daoSession.getTrainNumberJingGuoZhanPriceDao().getDatabase();
		db.execSQL(sql,new String[]{"大连","西安"});

	}

}
