package com.tripmaster.sortlistview;

import java.util.Comparator;

import com.trip.trainnumber.entity.dao.TrainStation;


/**
 * 
 * @author xiaanming
 *
 */
public class PinyinComparator implements Comparator<TrainStation> {

	public int compare(TrainStation o1, TrainStation o2) {
		if (o1.getPinyin().equals("@")
				|| o2.getPinyin().equals("#")) {
			return -1;
		} else if (o1.getPinyin().equals("#")
				|| o2.getPinyin().equals("@")) {
			return 1;
		} else {
			return o1.getPinyin().compareTo(o2.getPinyin());
		}
	}

}
