package com.tripmaster.sortlistview;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.trip.trainnumber.entity.dao.TrainStation;
import com.tripmaster.R;
import com.tripmaster.base.BaseFragment;
import com.tripmaster.base.dbhelper.TrainStationDBHelper;
import com.tripmaster.sortlistview.SideBar.OnTouchingLetterChangedListener;
import com.tripmaster.traintickets.CitySelectRecentlyFragment.MyOnItem;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**城市选择列表界面**/
public class CitySelectStationListFragment extends BaseFragment {
	private ListView sortListView;
	private SideBar sideBar;
	private TextView dialog;
	private SortAdapter adapter;
	private ClearEditText mClearEditText;
	
	/**
	 * ����ת����ƴ������
	 */
	private CharacterParser characterParser;
	private static List<TrainStation> SourceDateList;
	static{
		SourceDateList = TrainStationDBHelper.getInstance().getTrainStationList();
	}
	
	/**
	 * ���ƴ��������ListView����������
	 */
	private PinyinComparator pinyinComparator;
	
	@Override
	protected void onCreateView(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(savedInstanceState);
		setContentView(R.layout.cityselect_stationlist_sort);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initViews();
	}
	

	private void initViews() {
		//ʵ����תƴ����
		characterParser = CharacterParser.getInstance();
		
		pinyinComparator = new PinyinComparator();
		
		sideBar = (SideBar) getActivity().findViewById(R.id.sidrbar);
		dialog = (TextView) getActivity().findViewById(R.id.dialog);
		sideBar.setTextView(dialog);
		
		//�����Ҳഥ������
		sideBar.setOnTouchingLetterChangedListener(new OnTouchingLetterChangedListener() {
			
			@Override
			public void onTouchingLetterChanged(String s) {
				//����ĸ�״γ��ֵ�λ��
				int position = adapter.getPositionForSection(s.charAt(0));
				if(position != -1){
					sortListView.setSelection(position);
				}
				
			}
		});
		
		sortListView = (ListView) getActivity().findViewById(R.id.country_lvcountry);
		sortListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				//����Ҫ����adapter.getItem(position)����ȡ��ǰposition���Ӧ�Ķ���
//				Toast.makeText(getActivity(), ((SortModel)adapter.getItem(position)).getName(), Toast.LENGTH_SHORT).show();
				itemListener.onItemClick(adapter.getItem(position));
			}
		});
		
		// ���a-z��������Դ���
//		Collections.sort(SourceDateList, pinyinComparator);
		adapter = new SortAdapter(getActivity());
		sortListView.setAdapter(adapter);
		adapter.updateListView(SourceDateList);
		
		mClearEditText = (ClearEditText) getActivity().findViewById(R.id.filter_edit);
//		new AsyncLoadTask().execute();
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mClearEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				filterData(s.toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
	}
	
	private class AsyncLoadTask extends AsyncTask<Void, Void, Void>{
		private ProgressDialog dialog;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new ProgressDialog(getActivity());
			dialog.setMessage("数据加载中...");
			dialog.setTitle("数据加载");
			dialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			/**从数据库中读取所有t到站名**/
			SourceDateList = TrainStationDBHelper.getInstance().getTrainStationList();
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
//			Collections.sort(SourceDateList, pinyinComparator);
			adapter.updateListView(SourceDateList);
			dialog.dismiss();
		}
		
	}
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if(activity instanceof MyOnItem ){
			itemListener = (MyOnItem) activity;			
		}
	}
	
	private MyOnItem itemListener;


	/**
	 * ΪListView������
	 * @param date
	 * @return
	 */
//	private List<SortModel> filledData(String [] date){
//		List<SortModel> mSortList = new ArrayList<SortModel>();
//		
//		for(int i=0; i<date.length; i++){
//			SortModel sortModel = new SortModel();
//			sortModel.setName(date[i]);
//			//����ת����ƴ��
//			String pinyin = characterParser.getSelling(date[i]);
//			String sortString = pinyin.substring(0, 1).toUpperCase();
//			
//			// ������ʽ���ж�����ĸ�Ƿ���Ӣ����ĸ
//			if(sortString.matches("[A-Z]")){
//				sortModel.setSortLetters(sortString.toUpperCase());
//			}else{
//				sortModel.setSortLetters("#");
//			}
//			
//			mSortList.add(sortModel);
//		}
//		return mSortList;
//		
//	}
	
	/**
	 * ���������е�ֵ��������ݲ�����ListView
	 * @param filterStr
	 */
	private void filterData(String filterStr){
		List<TrainStation> filterDateList = new ArrayList<TrainStation>();
		
		if(TextUtils.isEmpty(filterStr)){
			filterDateList = SourceDateList;
		}else{
			filterDateList.clear();
			for(TrainStation sortModel : SourceDateList){
				String name = sortModel.getStation_name();
				if(name.indexOf(filterStr.toString()) != -1 || characterParser.getSelling(name).startsWith(filterStr.toString())){
					filterDateList.add(sortModel);
				}
			}
		}
		
		// ���a-z��������
//		Collections.sort(filterDateList, pinyinComparator);
		adapter.updateListView(filterDateList);
	}
	
}
