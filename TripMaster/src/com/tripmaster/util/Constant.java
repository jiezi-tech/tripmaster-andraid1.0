package com.tripmaster.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.content.Context;

import com.trip.trainnumber.entity.dao.TrainNumber;
import com.trip.trainnumber.entity.dao.TrainNumberJingGuoZhanPrice;
import com.tripmaster.base.dbhelper.TrainNumberJingGuoZhanPriceDBHelper;


public class Constant {
    /**
     * 数据库版本号
     */
    public static final int DATABASE_VERSION = 1;

    /**
     * 数据库名称
     */
    public static final String DATABASE_NAME = "tripmaster.db";
    public static final String DATABASE_TEST_NAME = "greendao.db";
    
	/** 返回的数据格式 **/
	public static final String FORMAT = "json";
	/** 返回的数据格式 **/
	public static final String QUERY_DATE = "2015-08-26";

	public static final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd");
	public static final SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
	
	public static final int ALL_STATION_FINISH = 0;//取所有车站结束
	public static final int ALL_QUNAER_CHECI_FINISH = 1;//取去哪儿的所有车次结束
	public static final int ALL_12306_CHECI_FINISH = 2;//取12306所有车次结束
	public static final int ALL_12306_CHECI_GUOZHAN_FINISH = 3;//取12306车次时刻表结束
	public static final int ALL_12306_CHECI_GUOZHAN_PRICE_FINISH = 4;//取12306车次时刻表价格结束
	public static final int ALL_12306_CHECI_GUOZHAN_MILEAGE_FINISH = 5;//取12306车次时刻表公里数结束

	public static final int RESULTLISTFILTER = 100;
	public static final int TRANSITFILTER = 200;

	/**取后N天的日期***/
	public static String afterNDay(int n) {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, n);
		Date d2 = c.getTime();
		String s = sdf.format(d2);
		return s;
	}
	
	/**
	 * 当前时间延后一天
	 * **/
	public static String afterDay(String time) {
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(sdf.parse(time));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		c.add(Calendar.DATE, 1);
		Date d2 = c.getTime();
		String s = sdf.format(d2);
		return s;
	}
	
	/**
	 * 当前时间延后一天
	 * **/
	public static String beforeDay(String time) {
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(sdf.parse(time));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		c.add(Calendar.DATE, -1);
		Date d2 = c.getTime();
		String s = sdf.format(d2);
		return s;
	}
	
	/**
     * 获取当前日期是星期几<br>
     * 
     * @param dt
     * @return 当前日期是星期几
     */
    public static String getWeekOfDate(String time) {
		try {
			Date dt = sdf.parse(time);
			String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
			Calendar cal = Calendar.getInstance();
			cal.setTime(dt);
			
			int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
			if (w < 0)
				w = 0;
			
			return weekDays[w];
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
    }

	/**
	 * 两个时间的比较
	 * 
	 * @return 如果newDate在oldDate之后 则返回true 否则返回false
	 * **/
	public static boolean compareTwoDate(String oldDate, String newDate) {
		// TODO Auto-generated method stub
		try {
			Date od = sdf2.parse(oldDate);
			Date nd = sdf2.parse(newDate);

			if (od.before(nd))
				return true;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}
	
	/***
	 *判断给出的时间是否在当前日期之后
	 *即判断时间是否有效
	 *@return 如果在当前日期之后 则返回true 否则返回false 
	 * 
	 * **/
	public static boolean compareDateAfterCurrent(String date) {
		try {
			Date od = new Date(System.currentTimeMillis());
			Date nd = sdf.parse(date);

			if (od.before(nd))
				return true;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	/***
	 * 按yyyyMMdd格式化时间
	 * **/
	public static String formatDate(String date) {
		try {
			Date d = sdf2.parse(date);
			return sdf.format(d);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/***
	 * 返回格式化好的当前时间
	 * 
	 * @return String 返回一个格式化好的当前系统时间
	 * **/
	public static String formatCurrentString() {
		return sdf.format(new Date(System.currentTimeMillis()));
	}
	

	
	/***根据车次的起始站序与终点站序算出价格表**/
	public static void calculate(TrainNumber number, Context mContext) {
		int from = Integer.parseInt(number.getFrom_station_no());
		int to = Integer.parseInt(number.getTo_station_no());

		for (int i = from; i <= to; i++) {
			for (int j = i + 1; j <= to; j++) {
				String from_station_no = String.valueOf(i);
				String to_station_no = String.valueOf(j);
				if (i < 10)
					from_station_no = "0" + i;
				if (j < 10)
					to_station_no = "0" + j;
				
				TrainNumberJingGuoZhanPrice guoZhanPrice = new TrainNumberJingGuoZhanPrice();
				guoZhanPrice.setTrain_no(number.getTrain_no());//车次编码
				guoZhanPrice.setStation_train_code(number.getStation_train_code());//车次
				guoZhanPrice.setFrom_station_no(from_station_no);//起始序号
				guoZhanPrice.setTo_station_no(to_station_no);//终点序号
				guoZhanPrice.setSeat_types(number.getSeat_types());//座位类型
				
				/**将该对象保存到数据库中**/
				TrainNumberJingGuoZhanPriceDBHelper.getInstance().addTrainNumberJingGuoZhanPriceToTable(guoZhanPrice);
				
				System.out.println(number.getStation_train_code()+"   "+from_station_no+"   "+to_station_no);
			}
		}
		
	}
	
	public void dataToSecond(String time) {

	}

}
