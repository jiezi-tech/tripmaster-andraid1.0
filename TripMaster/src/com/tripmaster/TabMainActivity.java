package com.tripmaster;

import com.shizhefei.view.indicator.Indicator;
import com.shizhefei.view.indicator.IndicatorViewPager;
import com.shizhefei.view.indicator.IndicatorViewPager.IndicatorFragmentPagerAdapter;
import com.tripmaster.base.BaseFragment;
import com.tripmaster.base.BaseFragmentActivity;
import com.tripmaster.personal_center.PersonalCenterFragment;
import com.tripmaster.traintickets.MainFragment;
import com.tripmaster.travel.TravelFragment;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class TabMainActivity extends BaseFragmentActivity {
	private IndicatorViewPager indicatorViewPager;
	private MyAdapter adapter;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_tabmain);
		initTitle("车票查询", false);
		
		ViewPager viewPager = (ViewPager) findViewById(R.id.tabmain_viewPager);
		Indicator indicator = (Indicator) findViewById(R.id.tabmain_indicator);
		indicatorViewPager = new IndicatorViewPager(indicator, viewPager);
		adapter = new MyAdapter(getSupportFragmentManager());
		indicatorViewPager.setAdapter(adapter);
		// 禁止viewpager的滑动事件
//		viewPager.setCanScroll(false);
		// 设置viewpager保留界面不重新加载的页面数量
		viewPager.setOffscreenPageLimit(3);
		// 默认是1,，自动预加载左右两边的界面。设置viewpager预加载数为0。只加载加载当前界面。
//		viewPager.setPrepareNumber(0);
	}
	
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		// TODO Auto-generated method stub
//		super.onActivityResult(requestCode, resultCode, data);
//		Fragment fragment = adapter.getFragmentForPage(indicatorViewPager.getCurrentItem());
//		if(fragment != null)
//			fragment.onActivityResult(requestCode, resultCode, data);
//		Log.d("######", "######");
//	}

	private class MyAdapter extends IndicatorFragmentPagerAdapter {
		private String[] tabNames = { "车票预售", "旅行服务", "个人中心" };
		private int[] tabIcons = { R.drawable.tab_main_ticket_icon_selector, R.drawable.tab_main_travel_icon_selector, R.drawable.tab_main_person_icon_selector};
		private LayoutInflater inflater;

		public MyAdapter(FragmentManager fragmentManager) {
			super(fragmentManager);
			inflater = LayoutInflater.from(getApplicationContext());
		}

		@Override
		public int getCount() {
			return tabNames.length;
		}

		@Override
		public View getViewForTab(int position, View convertView, ViewGroup container) {
			if (convertView == null) {
				convertView = (TextView) inflater.inflate(R.layout.tab_main, container, false);
			}
			TextView textView = (TextView) convertView;
			textView.setText(tabNames[position]);
			Resources res = getResources();
			Drawable myImage = res.getDrawable(tabIcons[position]);
			myImage.setBounds(20, 0, myImage.getIntrinsicWidth()+20, myImage.getIntrinsicHeight());
			textView.setCompoundDrawables(myImage, null, null, null);
//			textView.setCompoundDrawablesWithIntrinsicBounds(tabIcons[position], 0, 0, 0);
			return textView;
		}

		@Override
		public Fragment getFragmentForPage(int position) {
			BaseFragment bf = null;
			switch (position) {
			case 0:
				bf = new MainFragment();
				
				break;

			case 1:
				bf = new TravelFragment();
				
				break;
			case 2:
				bf = new PersonalCenterFragment();
				break;
			}
			return bf;
		}
	}
}
