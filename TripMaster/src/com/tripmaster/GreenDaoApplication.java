package com.tripmaster;

import com.trip.trainnumber.entity.dao.DaoMaster;
import com.trip.trainnumber.entity.dao.DaoMaster.OpenHelper;
import com.trip.trainnumber.entity.dao.DaoSession;
import com.tripmaster.util.Constant;

import android.app.Application;
import android.content.Intent;

public class GreenDaoApplication extends Application {
    private static GreenDaoApplication appContext;

    private static DaoMaster daoMaster;

    private static DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        if (appContext == null) {
            appContext = this;
        }
        //启动加载数据的服务
        long count = getDaoSession().getTrainNumberJingGuoZhanPriceDao().count();
        if(count == 0){
	        Intent intent= new Intent("com.tripmaster.service.downloadservice");
	        startService(intent);
        }
    }

    /**
     * 取得DaoMaster
     * 
     * @param context
     * @return
     */
    public static DaoMaster getDaoMaster() {
        if (daoMaster == null) {
            OpenHelper helper = new DaoMaster.DevOpenHelper(appContext, Constant.DATABASE_TEST_NAME,
                    null);
            daoMaster = new DaoMaster(helper.getWritableDatabase());
        }
        return daoMaster;
    }

    /**
     * 取得DaoSession
     * 
     * @param context
     * @return
     */
    public static DaoSession getDaoSession() {
        if (daoSession == null) {
            if (daoMaster == null) {
                daoMaster = getDaoMaster();
            }
            daoSession = daoMaster.newSession();
        }
        return daoSession;
    }
}
