package com.tripmaster.http;

import java.util.List;

import com.trip.trainnumber.entity.dao.TrainNumber;
import com.trip.trainnumber.entity.dao.TrainNumberSearchResult;

/** 站站查询返回的车次列表集合结果对象 **/
public class QueryResult {

	private List<TrainNumberSearchResult> datas;// 返回的车次列表

	public List<TrainNumberSearchResult> getDatas() {
		return datas;
	}

	public void setDatas(List<TrainNumberSearchResult> datas) {
		this.datas = datas;
	}

}
