package com.tripmaster.http;

import android.content.Context;

/*** 提供网络服务的基类 **/
public class BaseHttpRequest {
	protected Context mContext = null;
	protected static final int ZHAN_TO_ZHAN_REQUEST = 1;
	protected static final int TRAINNUMBER_PRICE_REQUESRT = 2;
	protected static final int GET_ALIMEGER_REQUEST = 3;
	protected static final int QUERY_BY_TRAINNO_REQUEST = 4;//queryByTrainNo

	public BaseHttpRequest(Context mContext) {
		this.mContext = mContext;
	}

	public interface HttpRequesCallBack {
		void onResult(String result, int requestCode);
	}

}
