package com.tripmaster.http;

import java.util.List;

import org.json.JSONException;

import com.alibaba.fastjson.JSONArray;
import com.feiyucloud.http.AsyncHttpClient;
import com.feiyucloud.http.Request;
import com.feiyucloud.http.TextResponseHandler;
import com.feiyucloud.http.Request.Method;

import android.content.Context;

/***
 * 
 * 车次查询 返回经过站列表集合
 * **/
public class TrainNoRequest extends BaseHttpRequest {
	private static final String trainNoUrl = "https://kyfw.12306.cn/otn/czxx/queryByTrainNo?";
	private HttpRequestResponseListener listener;
	
	public TrainNoRequest(Context mContext,HttpRequestResponseListener listener) {
		super(mContext);
		this.listener = listener;
	}

	public void queryTrainNo(String train_no, String from_station_telecode,
			String to_station_telecode, String depart_date) {

		String url = trainNoUrl + "train_no=" + train_no
				+ "&from_station_telecode=" + from_station_telecode
				+ "&to_station_telecode=" + to_station_telecode
				+ "&depart_date=" + depart_date;

		Request request = new Request(Method.GET);
		request.url(url);
		AsyncHttpClient client = new AsyncHttpClient();
		client.doRequest(request, new TextResponseHandler() {
			
			@Override
			public void onFailure(Throwable e) {
				e.printStackTrace();
			}
			
			@Override
			public void onSuccess(String response) {
				if(response != null){
					try {
						org.json.JSONObject object = new org.json.JSONObject(response);
						if(object.has("status") && object.getBoolean("status") && object.has("data")){
							org.json.JSONObject data = object.getJSONObject("data");
							if(data.has("data")){
								String str = data.getString("data");
								List<TrainNumberJingGuoZhan> guoZhans = JSONArray.parseArray(str, TrainNumberJingGuoZhan.class);
								if(guoZhans != null && guoZhans.size() > 0)
									listener.onSuccess(guoZhans);
							}
							
						}
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

	}
	
	public interface HttpRequestResponseListener{
		void onSuccess(List<TrainNumberJingGuoZhan> guoZhans);//成功回调
		void onFailure();//失败回调
	}

}
