package com.tripmaster.http;

import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

import com.alibaba.fastjson.JSON;
import com.feiyucloud.http.AsyncHttpClient;
import com.feiyucloud.http.Request;
import com.feiyucloud.http.TextResponseHandler;
import com.feiyucloud.http.Request.Method;
import com.trip.trainnumber.entity.dao.TrainNumber;
import com.trip.trainnumber.entity.dao.TrainNumberSearchResult;
import com.tripmaster.base.dbhelper.TrainNumberSearchResultDBHelper;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

/***
 * 站站查询服务 参数：始发站编码、终点站编码、以及查询时间 返回：车次集合对象
 * **/
public class StationToStationRequest extends BaseHttpRequest {
	private List<TrainNumberSearchResult> numbers;// 站站查询出来的车次集合
	private String queryDate;
	private String from_station;
	private String to_Station;
	private int index = 0;
	private QueryZhanToZhanCallBack callBack = null;
	private Handler handler = null;
	private final int FINISH = 1;

	public StationToStationRequest(Context context) {
		super(context);
		handler = new Handler(new Handler.Callback() {

			@Override
			public boolean handleMessage(Message msg) {
				switch (msg.what) {
				case FINISH:
					callBack.onResult(numbers);
					break;
				}
				return false;
			}
		});
	}

	public void getQuery(String queryDate, String from_station,
			String to_Station, QueryZhanToZhanCallBack callBack) {
		this.queryDate = queryDate;
		this.from_station = from_station;
		this.to_Station = to_Station;
		this.callBack = callBack;

		requestZhantoZhan();// 站站查询
	}

	/*** 先进行站站查询 **/
	private void requestZhantoZhan() {

		String url = "https://kyfw.12306.cn/otn/lcxxcx/query?"
				+ "purpose_codes=ADULT&queryDate=" + queryDate
				+ "&from_station=" + from_station + "&to_station=" + to_Station;

		Request request = new Request(Method.GET);
		request.url(url);
		AsyncHttpClient client = new AsyncHttpClient();
		client.doRequest(request, new TextResponseHandler() {

			@Override
			public void onSuccess(String response) {
				System.out.println(response);
				if (response != null) {
					try {

						JSONObject object = new JSONObject(response);
						if (object.has("status") && object.getBoolean("status")) {// 返回数据正常
							String data = object.getString("data");
							QueryResult qr = JSON.parseObject(data, QueryResult.class);
							
							if (qr != null && qr.getDatas() != null
									&& qr.getDatas().size() > 0) {
								numbers = qr.getDatas();
								callBack.onResult(numbers);
								index = 0;
								queryTicketPrice(numbers.get(index));
							} else
								handler.sendEmptyMessage(FINISH);

						} else
							handler.sendEmptyMessage(FINISH);

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						handler.sendEmptyMessage(FINISH);
					}
				} else
					handler.sendEmptyMessage(FINISH);

			}

			@Override
			public void onFailure(Throwable e) {
				handler.sendEmptyMessage(FINISH);
			}

		});

	}

	/**
	 * 票价遍历查询
	 * 
	 * @param number
	 *            要求车次必须有train_no、seat_types、from_station_no、to_station_no
	 * @return 返回带有价格的车次对象
	 * **/
	private void queryTicketPrice(final TrainNumberSearchResult number) {
		new TrainNumberPriceRequest(mContext, number,
				new TrainNumberPriceRequest.TrainNumberPriceResult() {

					@Override
					public void onResult(TrainNumberSearchResult number) {
						TrainNumberSearchResultDBHelper helper = TrainNumberSearchResultDBHelper.getInstance();
						helper.updatePrice(number);
						
						index++;
						if (index < numbers.size()) {
							queryTicketPrice(numbers.get(index));
						} else {
							System.out.println("车次价格查询完毕");
							index = 0;
							getAllMileage(numbers.get(index));
						}
					}
				});

	}

	/***
	 * 车次里程查询中
	 * 
	 * @param number
	 *            要求车次必须有from_station_name、to_station_name、station_train_code
	 * @return 返回带有里程数的车次对象
	 * **/
	private void getAllMileage(final TrainNumberSearchResult number) {
		new AllMileageRequest(mContext, number,
				new AllMileageRequest.AllMileageResult() {

					@Override
					public void onResult(TrainNumberSearchResult number) {
						TrainNumberSearchResultDBHelper helper = TrainNumberSearchResultDBHelper.getInstance();
						helper.updateAllMileage(number);
						
						index++;
						if (index < numbers.size()) {
							getAllMileage(numbers.get(index));
						} else {
							System.out.println("所有任务执行完毕");
							handler.sendEmptyMessage(FINISH);
						}

					}
				});

	}

	/** 站站查询最后返回的集合列表 **/
	public interface QueryZhanToZhanCallBack {
		void onResult(List<TrainNumberSearchResult> list);
	}

}
