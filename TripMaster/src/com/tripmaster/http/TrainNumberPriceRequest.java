package com.tripmaster.http;

import org.json.JSONException;
import org.json.JSONObject;

import com.alibaba.fastjson.JSON;
import com.feiyucloud.http.AsyncHttpClient;
import com.feiyucloud.http.Request;
import com.feiyucloud.http.TextResponseHandler;
import com.feiyucloud.http.Request.Method;
import com.trip.trainnumber.entity.dao.TrainNumber;
import com.trip.trainnumber.entity.dao.TrainNumberSearchResult;
import com.tripmaster.util.Constant;

import android.content.Context;
import android.util.Log;

/***
 * 车次价格查询服务 参数：车次编码、起始序号、结束序号、座位类型、以及查询时间 返回：带有价格带车次对象
 * **/
public class TrainNumberPriceRequest extends BaseHttpRequest{
	private TrainNumberPriceResult priceResult;

	public TrainNumberPriceRequest(Context mContext, TrainNumberSearchResult number,
			TrainNumberPriceResult priceResult) {
		super(mContext);
		this.priceResult = priceResult;

		queryTicketPrice(number);
	}

	/** 票价遍历查询 **/
	private void queryTicketPrice(final TrainNumberSearchResult number) {
		Log.i("lanou",
				number.getTrain_no() + " --- " + number.getSeat_types() + " --- "
						+ number.getFrom_station_no() + "  "
						+ number.getTo_station_no());
		String queryDate = Constant.formatDate(number.getStart_train_date());

		String path = "https://kyfw.12306.cn/otn/leftTicket/queryTicketPrice?"
				+ "train_no=" + number.getTrain_no() + "&from_station_no="
				+ number.getFrom_station_no() + "&to_station_no="
				+ number.getTo_station_no() + "&seat_types="
				+ number.getSeat_types() + "&train_date=" + queryDate;

		System.out.println(path);
		Request request = new Request(Method.GET);
		request.url(path);
		AsyncHttpClient client = new AsyncHttpClient();
		client.doRequest(request, new TextResponseHandler() {

			@Override
			public void onSuccess(String response) {
				priceToJson(number, response);
			}

			@Override
			public void onFailure(Throwable e) {
				// TODO Auto-generated method stub
				priceResult.onResult(null);
				e.printStackTrace();
			}
			
		});

	}

	private void priceToJson(TrainNumberSearchResult number, String result) {
		if (result != null) {
			try {
				JSONObject object = new JSONObject(result);
				if (object.has("status") && object.getBoolean("status")) {
					String data = object.getString("data");
					TicketPrice ticketPrice = JSON.parseObject(data, TicketPrice.class);

					if (ticketPrice.getA6() == null)
						number.setGr_price("--");
					else
						number.setGr_price(ticketPrice.getA6());

					if (ticketPrice.getA4() == null)
						number.setRw_price("--");
					else
						number.setRw_price(ticketPrice.getA4());

					if (ticketPrice.getA2() == null)
						number.setRz_price("--");
					else
						number.setRz_price(ticketPrice.getA2() + "");

					if (ticketPrice.getP() == null)
						number.setTz_price("--");
					else
						number.setTz_price(ticketPrice.getP());

					if (ticketPrice.getWZ() == null)
						number.setWz_price("--");
					else
						number.setWz_price(ticketPrice.getWZ());

					if (ticketPrice.getA3() == null)
						number.setYw_price("--");
					else
						number.setYw_price(ticketPrice.getA3());

					if (ticketPrice.getA1() == null)
						number.setYz_price("--");
					else
						number.setYz_price(ticketPrice.getA1());

					if (ticketPrice.getO() == null)
						number.setZe_price("--");
					else
						number.setZe_price(ticketPrice.getO());

					if (ticketPrice.getM() == null)
						number.setZy_price("--");
					else
						number.setZy_price(ticketPrice.getM());

					if (ticketPrice.getA9() == null)
						number.setSwz_price("--");
					else
						number.setSwz_price(ticketPrice.getA9());
					
					priceResult.onResult(number);//取到结果之后返回
					return;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		queryTicketPrice(number);//取不到重新取

	}

	public interface TrainNumberPriceResult {
		void onResult(TrainNumberSearchResult number);
	}

}
