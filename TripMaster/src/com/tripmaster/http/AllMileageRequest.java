package com.tripmaster.http;

import org.json.JSONException;
import org.json.JSONObject;
import com.trip.trainnumber.entity.dao.TrainNumber;
import com.trip.trainnumber.entity.dao.TrainNumberSearchResult;

import afinal.http.AjaxCallBack;
import afinal.http.AjaxParams;
import afinal.http.base.BaseFinalHttp;
import android.content.Context;
import android.util.Log;

public class AllMileageRequest extends BaseHttpRequest {
	private AllMileageResult mileageResult = null;

	public AllMileageRequest(Context mContext, TrainNumberSearchResult number,
			AllMileageResult mileageResult) {
		super(mContext);
		this.mileageResult = mileageResult;
		getAllMileage(number);

	}

	private static final String path = "http://train.qunar.com/qunar/checiPrice.jsp";

	/*** 获取里程数据 **/
	private void getAllMileage(final TrainNumberSearchResult tn) {
		Log.i("lanou",
				tn.getFrom_station_name() + " --- " + tn.getTo_station_name()
						+ " ---  " + tn.getStation_train_code());

		BaseFinalHttp finalHttp = new BaseFinalHttp(mContext);
		AjaxParams ajaxParams = new AjaxParams();
		ajaxParams.put("from", tn.getFrom_station_name());
		ajaxParams.put("to", tn.getTo_station_name());
		ajaxParams.put("q", tn.getStation_train_code());
		ajaxParams.put("format", "json");
		finalHttp.get(path, ajaxParams, new AjaxCallBack<String>() {
			@Override
			public void onSuccess(String t) {
				// TODO Auto-generated method stub
				super.onSuccess(t);
				if (t != null) {
					try {
						JSONObject object = new JSONObject(t);
						if (object.has("extInfo")) {
							JSONObject extInfo = object
									.getJSONObject("extInfo");
							if (extInfo != null) {
//								if (extInfo.has("allTime"))
//									tn.setAllTime(extInfo.getString("allTime"));
								if (extInfo.has("allMileage"))
									tn.setAllMileage(extInfo
											.getString("allMileage"));
							}
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				mileageResult.onResult(tn);// 取到之后返回

			}

			@Override
			public void onFailure(Throwable t, int errorNo, String strMsg) {
				// TODO Auto-generated method stub
				super.onFailure(t, errorNo, strMsg);
				mileageResult.onResult(tn);// 取到之后返回
			}

		});

	}

	public interface AllMileageResult {
		void onResult(TrainNumberSearchResult number);
	}
}
