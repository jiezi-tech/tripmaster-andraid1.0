package com.tripmaster.http;

import java.util.List;

public class TicketPrice{
	private String A6;// gr_price
	private List<String> OT;// qt_price
	private String A4;// rw_price
	private String A2;// rz_price
	private String P;// tz_price
	private String WZ;// wz_price
	private String A3;// yb_price
	private String A1;// yw_price
	private String O;// yz_price
	private String M;// ze_price
	private String A9;// zy_price
	private String train_no;// swz_price
	private String station_no;// 开始站
	/*
	 *   ["gr_num"]=>高级软卧
	 *   ["qt_num"]=>其他
	 *   ["rw_num"]=> 软卧
	 *   ["rz_num"]=>软座
	 *   ["tz_num"]=>特等座
	 *   ["wz_num"]=>无座
	 *   ["yw_num"]=>硬卧
	 *   ["yz_num"]=>硬座
	 *   ["ze_num"]=>二等座
	 *   ["zy_num"]=> 一等座
	 *   ["swz_num"]=> 商务座
	 */
	
	public String getA6() {
		return A6;
	}

	public void setA6(String a6) {
		A6 = a6;
	}

	public List<String> getOT() {
		return OT;
	}

	public void setOT(List<String> oT) {
		OT = oT;
	}

	public String getA4() {
		return A4;
	}

	public void setA4(String a4) {
		A4 = a4;
	}

	public String getA2() {
		return A2;
	}

	public void setA2(String a2) {
		A2 = a2;
	}

	public String getP() {
		return P;
	}

	public void setP(String p) {
		P = p;
	}

	public String getWZ() {
		return WZ;
	}

	public void setWZ(String wZ) {
		WZ = wZ;
	}

	public String getA3() {
		return A3;
	}

	public void setA3(String a3) {
		A3 = a3;
	}

	public String getA1() {
		return A1;
	}

	public void setA1(String a1) {
		A1 = a1;
	}

	public String getO() {
		return O;
	}

	public void setO(String o) {
		O = o;
	}

	public String getM() {
		return M;
	}

	public void setM(String m) {
		M = m;
	}

	public String getA9() {
		return A9;
	}

	public void setA9(String a9) {
		A9 = a9;
	}

	public String getTrain_no() {
		return train_no;
	}

	public void setTrain_no(String train_no) {
		this.train_no = train_no;
	}

	public String getStation_no() {
		return station_no;
	}

	public void setStation_no(String station_no) {
		this.station_no = station_no;
	}

}
