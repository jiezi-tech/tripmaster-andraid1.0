package com.tripmaster;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/** 闪屏界面 用于数据库的加载 **/
public class SplashActivity extends Activity {
	private Handler handler;
	private static final int DOWNLOAD_FINISH = 1;// 加载完事

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		initView();
	}

	private void initView() {
		handler = new Handler(new Handler.Callback() {

			@Override
			public boolean handleMessage(Message msg) {
				// 跳转到首页 并且关闭当前页面
				Intent intent = new Intent(SplashActivity.this,
						TabMainActivity.class);
				startActivity(intent);
				finish();

				return false;
			}
		});

		handler.sendEmptyMessageDelayed(DOWNLOAD_FINISH, 4000);
	}

}
