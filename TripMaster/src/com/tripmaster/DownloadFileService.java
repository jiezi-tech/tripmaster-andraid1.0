package com.tripmaster;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.trip.trainnumber.entity.dao.TrainNumber;
import com.trip.trainnumber.entity.dao.TrainNumberJingGuoZhanPrice;
import com.trip.trainnumber.entity.dao.TrainStation;
import com.tripmaster.base.dbhelper.TrainNumberDBHelper;
import com.tripmaster.base.dbhelper.TrainNumberJingGuoZhanPriceDBHelper;
import com.tripmaster.base.dbhelper.TrainStationDBHelper;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

public class DownloadFileService extends IntentService{
	private TrainNumberJingGuoZhanPriceDBHelper ph;
	private final String priceFileDirectoryName ="jingguozhanprice";

	public DownloadFileService() {
		super("DownloadFileService");
		ph = TrainNumberJingGuoZhanPriceDBHelper.getInstance();
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		//加载文件数据到数据库中
		getAllStationFromJson();
		getAllTrainNumberFromJson();
		importPrice();
	}


	/** 获取所有车站 **/
	private void getAllTrainNumberFromJson() {
		InputStream is = null;
		BufferedReader reader = null;
		try {
			is = getAssets().open("TRAIN_NUMBER.json");
			reader = new BufferedReader(new InputStreamReader(is));
			String str = new String();
			StringBuffer buffer = new StringBuffer();
			while ((str = reader.readLine()) != null) {
				buffer.append(str);
			}
			List<TrainNumber> list = JSON.parseArray(buffer.toString(), TrainNumber.class);
			TrainNumberDBHelper.getInstance().addTrainNumberListToTable(list);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {// 关流
			try {
				if (is != null)
					is.close();
				if (reader != null)
					reader.close();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("12306车次集合执行完毕");
	}


	/** 获取所有车站 **/
	private void getAllStationFromJson() {
		InputStream is = null;
		BufferedReader reader = null;
		try {
			is = getAssets().open("TRAIN_STATION.json");
			reader = new BufferedReader(new InputStreamReader(is));
			String str = new String();
			StringBuffer buffer = new StringBuffer();
			while ((str = reader.readLine()) != null) {
				buffer.append(str);
			}
			List<TrainStation> list = JSON.parseArray(buffer.toString(), TrainStation.class);

			TrainStationDBHelper.getInstance()
					.addTrainStationListToTable(list);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {// 关流
			try {
				if (is != null)
					is.close();
				if (reader != null)
					reader.close();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("所有车站集合执行完毕");

	}
	

	private void importPrice() {
		long startTime = System.currentTimeMillis();
		try {
			String[] paths = getAssets().list(priceFileDirectoryName);
			for(String s: paths){
				String fileName =priceFileDirectoryName+"/"+s;
				System.out.println(fileName);
				List<TrainNumberJingGuoZhanPrice> list = readFile(fileName);
				Log.i("lanou",""+ list.size());
				
				 for(TrainNumberJingGuoZhanPrice t:list){
					 String start = t.getStart_time();
					 String end = t.getArrive_time();
					 String[] starts = start.split(":");
					 String[] ends = end.split(":");
					 t.setStart_hour(starts[0]);
					 t.setStart_minute(starts[1]);
					 t.setArrive_hour(ends[0]);
					 t.setArrive_minute(ends[1]);
					 
		                if(t.getGg_price() != null && t.getGg_price().contains("--"))
		                    t.setGg_price("");
		                if(t.getGr_price() != null && t.getGr_price().contains("--"))
		                    t.setGr_price("");

		                if(t.getQt_price() != null && t.getQt_price().contains("--"))
		                    t.setQt_price("");

		                if(t.getRw_price() != null && t.getRw_price().contains("--")){
		                    t.setRw_price("");
		                }

		                if(t.getRz_price() != null && t.getRz_price().contains("--"))
		                    t.setRz_price("");

		                if(t.getTz_price() != null && t.getTz_price().contains("--"))
		                    t.setTz_price("");

		                if(t.getWz_price() != null && t.getWz_price().contains("--"))
		                    t.setWz_price("");

		                if(t.getYb_price() != null && t.getYb_price().contains("--"))
		                    t.setYb_price("");

		                if(t.getYw_price() != null && t.getYw_price().contains("--"))
		                    t.setYw_price("");

		                if(t.getYz_price() != null && t.getYz_price().contains("--"))
		                    t.setYz_price("");

		                if(t.getZe_price() != null && t.getZe_price().contains("--"))
		                    t.setZe_price("");

		                if(t.getZy_price() != null && t.getZy_price().contains("--"))
		                    t.setZy_price("");

		                if(t.getSwz_price() != null && t.getSwz_price().contains("--"))
		                    t.setSwz_price("");
		            }
				 
				if(list != null && list.size() > 0){
					ph.insertInTx(list);//保存到数据库
				}
			}
			long endTime = System.currentTimeMillis();
			System.out.println("读取结束  耗时："+(endTime - startTime));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private List<TrainNumberJingGuoZhanPrice> readFile(String filename) {

		try {
			InputStream is = getAssets().open(filename);

			InputStreamReader isr = new InputStreamReader(is, "utf-8");
			BufferedReader reader = new BufferedReader(isr);
			StringBuffer buffer = new StringBuffer();
			String line = null;
			while((line = reader.readLine())  != null){
				buffer.append(line);
			}
			
			List<TrainNumberJingGuoZhanPrice> list = JSON.parseArray(
					buffer.toString(),TrainNumberJingGuoZhanPrice.class);

	        reader.close();
	        isr.close();
	        is.close();
	        
			return list;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
