package com.tripmaster.traintickets;

import com.tripmaster.widget.calendar.CaldroidFragment;
import com.tripmaster.widget.calendar.CaldroidGridAdapter;
import com.tripmaster.widget.calendar.CustomCalendarAdapter;

public class CustomCalendarFragment extends CaldroidFragment{
	
	@Override
	public CaldroidGridAdapter getNewDatesGridAdapter(int month, int year) {
		// TODO Auto-generated method stub
		return new CustomCalendarAdapter(getActivity(), month, year, getCaldroidData(), extraData);
	}

}
