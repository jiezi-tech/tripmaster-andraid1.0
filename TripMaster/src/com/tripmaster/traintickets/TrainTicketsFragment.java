package com.tripmaster.traintickets;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.trip.trainnumber.entity.dao.HistoryRecords;
import com.trip.trainnumber.entity.dao.HistoryRecordsDao.Properties;
import com.trip.trainnumber.entity.dao.HistoryStations;
import com.trip.trainnumber.entity.dao.TrainStation;
import com.tripmaster.R;
import com.tripmaster.base.BaseFragment;
import com.tripmaster.base.dbhelper.HistoryRecordsDBHelper;
import com.tripmaster.base.dbhelper.HistoryStationsDBHelper;
import com.tripmaster.base.dbhelper.TrainStationDBHelper;
import com.tripmaster.util.Constant;
import com.tripmaster.util.MyCustomDialog;
import com.tripmaster.util.MyCustomDialog.ClickListenerInterface;
import com.tripmaster.widget.calendar.CustomCalendarActivity;
/**
 * 首页里面的火车票tab页
 * **/
public class TrainTicketsFragment extends BaseFragment implements OnClickListener{
	private LinearLayout fromCity;

	private TextView fromCityTv;

	private LinearLayout toCity;

	private TextView toCityTv;

	private TextView searchBtn;

	private ImageButton exchangeCity;

	private LinearLayout dateLl;

	private TextView nextDateTv;
	
	private CheckBox checkBox;
	
	private TextView downLoad;
	
	private ListView listView;
	
	private ArrayAdapter<String> arrayAdapter;
	
	private TrainStation fromStation;//出发站
	private TrainStation toStation;//到达站
	
	private List<HistoryRecords> list = new ArrayList<HistoryRecords>();//历史记录list
//	private String[] data;
	
	private static TrainTicketsFragment instance = null;
	
	public static TrainTicketsFragment getInstance(){
		if(instance == null){
			instance = new TrainTicketsFragment();
		}
		return instance;
	}
	
	@Override
	protected void onCreateView(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(savedInstanceState);
		setContentView(R.layout.fragment_trainticket);
		
		fromCity = (LinearLayout)findViewById(R.id.fromCity);
		fromCity.setOnClickListener(this);
		fromCityTv = (TextView) findViewById(R.id.fromCityTv);
		toCity = (LinearLayout) findViewById(R.id.toCity);
		toCity.setOnClickListener(this);
		toCityTv = (TextView) findViewById(R.id.toCityTv);
		searchBtn = (TextView) findViewById(R.id.searchBtn);
		searchBtn.setText("搜索");
		searchBtn.setOnClickListener(this);
		exchangeCity = (ImageButton) findViewById(R.id.exchangeCity);
		exchangeCity.setOnClickListener(this);
		dateLl = (LinearLayout)findViewById(R.id.dateLl);
		dateLl.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				/**跳转到日期选择界面**/
				Intent intent = new Intent();
				intent.setClass(getActivity(), CustomCalendarActivity.class);
				String currentDate = nextDateTv.getText().toString().trim();
				if(currentDate != null)
					intent.putExtra("currentDate", currentDate);
				startActivityForResult(intent, 3);
			}
		});
		nextDateTv = (TextView)findViewById(R.id.nextDate);
		nextDateTv.setText(Constant.afterNDay(30));
		
		
		checkBox = (CheckBox) findViewById(R.id.checkbox);
		checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				Toast.makeText(getActivity(), ""+checkBox.isChecked(), Toast.LENGTH_SHORT).show();
			}
		});
		
		downLoad = (TextView) findViewById(R.id.download);
		downLoad.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final MyCustomDialog dialog = new MyCustomDialog(getActivity(), getString(R.string.download_dialogtitle),
						getString(R.string.download_dialogcontext), getString(R.string.download_dialogconfirm), 
						getString(R.string.download_dialogcancel));
				dialog.show();
				dialog.setClicklistener(new ClickListenerInterface() {
					
					@Override
					public void doConfirm() {
						// TODO Auto-generated method stub
						Toast.makeText(getActivity(), "下载", Toast.LENGTH_SHORT);
						dialog.dismiss();
					}
					
					@Override
					public void doCancel() {
						// TODO Auto-generated method stub
						dialog.dismiss();
						
					}
				});
			}
		});
		listView = (ListView) findViewById(R.id.historyList);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				HistoryRecords record = list.get(arg2);
				fromCityTv.setText(record.getFrom_station_name());
				toCityTv.setText(record.getTo_station_name());
				
				fromStation = TrainStationDBHelper.getInstance().getTrainStationByStationName(record.getFrom_station_name());
				toStation = TrainStationDBHelper.getInstance().getTrainStationByStationName(record.getTo_station_name());
				
			}
		});
		arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.arrayadapter_item);
		listView.setAdapter(arrayAdapter);
	}
	
	@Override
	public void onResume() { 
		// TODO Auto-generated method stub
		super.onResume();
		new AsyncHistoryRecords().execute();
	}
	
	private class AsyncHistoryRecords extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			Log.d("#########", "######TrainTicketsFragment-AsyncHistoryRecords-1");
			list = HistoryRecordsDBHelper.getInstance(getActivity()).getHistoryRecordsList(Properties.From_station_name);
			Log.d("#########", "######TrainTicketsFragment-AsyncHistoryRecords-2");
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			arrayAdapter.clear();
			List<String> listTemp = new ArrayList<String>();
			String record = "";
			for(int i=0;i<list.size();i++){
				if(list.get(i).getFrom_station_name() != null && list.get(i).getTo_station_name() != null){
					record = list.get(i).getFrom_station_name()+"--"+list.get(i).getTo_station_name();
					listTemp.add(record);
				}
			}
			arrayAdapter.addAll(listTemp);
			arrayAdapter.notifyDataSetChanged();;
		}
		
	}


	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case 1:
			if (data != null) {
				fromStation = (TrainStation) data.getSerializableExtra("station");
				fromCityTv.setText(fromStation.getStation_name());					
			}
			break;
		case 2:
			if (data != null) {
				toStation = (TrainStation) data.getSerializableExtra("station");
				toCityTv.setText(toStation.getStation_name());
			}
			break;
		case 3:
			if(data!=null){
				String date = data.getStringExtra("selectDate");
				if(date!=null&&!"".equals(date)){
					nextDateTv.setText(date);
				}
			}
			break;
		default:
			break;
		}
	}

	@SuppressLint("ShowToast")
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.fromCity:
			Intent fromCityintent = new Intent(getActivity(), SearchCityActivity.class);
			fromCityintent.putExtra("requestCode ", 1);
			startActivityForResult(fromCityintent, 1);
//			startActivity(fromCityintent);
			break;
		case R.id.toCity:
			Intent toCityIntent = new Intent(getActivity(),
					SearchCityActivity.class);
			toCityIntent.putExtra("requestCode ", 2);
			startActivityForResult(toCityIntent, 2);
//			startActivity(toCityIntent);
			break;
		case R.id.searchBtn:
			if(fromStation == null || toStation == null)
				Toast.makeText(getActivity(), "请先选择出发站以及终到站", Toast.LENGTH_LONG).show();
			else if (fromStation.getStation_name().equals(toStation.getStation_name())) {
//				AlertDialogUtil.getInstance().show(getActivity(), " ",
//						"出发到达城市不能相同", null, null, "确定",
//						new DialogInterface.OnClickListener() {
//
//							@Override
//							public void onClick(DialogInterface dialog,
//									int which) {
//							}
//						});
				Toast.makeText(getActivity(),"出发到达城市不能相同" , Toast.LENGTH_LONG	).show();
				
			}
			else if(nextDateTv.getText().toString() == null || nextDateTv.getText().toString().equals("")){
				Toast.makeText(getActivity(),"请选择出发日期" , Toast.LENGTH_LONG	).show();
			}
			else {
				
				Log.d("#########", "######TrainTicketsFragment-searchbtn-1");
				
				//查询记录
				HistoryRecords historyRecords = new HistoryRecords();
				historyRecords.setFrom_station_name(fromStation.getStation_name());
				historyRecords.setFrom_station_telecode(fromStation.getTele_code());
				historyRecords.setFrom_station_pinyin(fromStation.getPinyin());
				historyRecords.setTo_station_name(toStation.getStation_name());
				historyRecords.setTo_station_telecode(toStation.getTele_code());
				historyRecords.setTo_station_pinyin(toStation.getPinyin());
				historyRecords.setDate("2015-8-29");
				historyRecords.setPhone_number("13940877777");
				historyRecords.setStation_train_code("2051");
				historyRecords.setType("a");
				historyRecords.setUpdateTime(""+System.currentTimeMillis());
				list.add(historyRecords);
				Log.d("#########", "######TrainTicketsFragment-searchbtn-2");
				HistoryRecordsDBHelper.getInstance(getActivity()).addHistoryRecordToTable(historyRecords);//保存当前对象就可以 不需要再重新保存所有
				
				Log.d("#########", "######TrainTicketsFragment-searchbtn-3");
				//出发站
				HistoryStations historyFromStations = new HistoryStations();
				historyFromStations.setTation_name(fromStation.getStation_name());
				historyFromStations.setStation_telecode(fromStation.getTele_code());
				historyFromStations.setStation_pinyin(fromStation.getPinyin());
				historyFromStations.setUpdateTime(""+System.currentTimeMillis());
				
				//到达站
				HistoryStations historyToStations = new HistoryStations();
				historyToStations.setTation_name(toStation.getStation_name());
				historyToStations.setStation_telecode(toStation.getTele_code());
				historyToStations.setStation_pinyin(toStation.getPinyin());
				historyToStations.setUpdateTime(""+System.currentTimeMillis());
				
				HistoryStationsDBHelper.getInstance(getActivity()).addHistoryStationToTable(historyFromStations);
				HistoryStationsDBHelper.getInstance(getActivity()).addHistoryStationToTable(historyToStations);
				Log.d("#########", "######TrainTicketsFragment-searchbtn-4");
				
				/**跳转到站站查询之后的结果界面**/
				Intent searchIntent = new Intent(getActivity(), ResultListMainActivity.class);
				searchIntent.putExtra("toStation", toStation);
				searchIntent.putExtra("fromStation", fromStation);
				searchIntent.putExtra("date", nextDateTv.getText().toString().trim());
				searchIntent.putExtra("isRoundTrip", false);
				getActivity().startActivity(searchIntent);
				Log.d("#########", "######TrainTicketsFragment-searchbtn-5");
			}
			break;
		case R.id.exchangeCity:
			//交换始发站和终点站
			if(fromStation != null && toStation != null){
				TrainStation temp = fromStation;
				fromStation = toStation;
				toStation = temp;
				
				fromCityTv.setText(fromStation.getStation_name());
				toCityTv.setText(toStation.getStation_name());
			}
			break;
		default:
			break;
		}
	}

}
