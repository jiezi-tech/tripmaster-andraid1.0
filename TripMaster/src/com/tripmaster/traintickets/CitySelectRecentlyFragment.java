package com.tripmaster.traintickets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.trip.trainnumber.entity.dao.HistoryStations;
import com.trip.trainnumber.entity.dao.HistoryStationsDao.Properties;
import com.trip.trainnumber.entity.dao.TrainStation;
import com.tripmaster.R;
import com.tripmaster.base.BaseFragment;
import com.tripmaster.base.dbhelper.HistoryStationsDBHelper;
import com.tripmaster.sortlistview.CharacterParser;
import com.tripmaster.sortlistview.ClearEditText;
import com.tripmaster.sortlistview.PinyinComparator;
import com.tripmaster.sortlistview.SortAdapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

/**
 * 最近常用城市选择
 * **/
public class CitySelectRecentlyFragment extends BaseFragment {
	private SortAdapter adapter;
	private ListView listView;
	private ClearEditText mClearEditText;
	//
	// /**
	// * ����ת����ƴ������
	// */
	private CharacterParser characterParser;
	private List<TrainStation> SourceDateList = new ArrayList<TrainStation>();;

	/**
	 * ���ƴ��������ListView����������
	 */
	private PinyinComparator pinyinComparator;
	private int requestCode;

	@Override
	protected void onCreateView(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(savedInstanceState);
		setContentView(R.layout.cityselect_recently);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		requestCode = getActivity().getIntent().getIntExtra("requestCode", 0);
		initViews();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (activity instanceof MyOnItem) {
			itemlistener = (MyOnItem) activity;
		}
	}

	public interface MyOnItem {
		public void onItemClick(TrainStation station);
	}

	public MyOnItem itemlistener;

	private void initViews() {
		characterParser = CharacterParser.getInstance();
		pinyinComparator = new PinyinComparator();
		listView = (ListView) getActivity().findViewById(R.id.citylist);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				itemlistener.onItemClick(adapter.getItem(position));
			}
		});

		// ���a-z��������Դ���
		// Collections.sort(SourceDateList, pinyinComparator);
		adapter = new SortAdapter(getActivity());
		listView.setAdapter(adapter);

		mClearEditText = (ClearEditText) getActivity().findViewById(
				R.id.filter_edit);

		new AsyncLoadTask().execute();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mClearEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				filterData(s.toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
	}

	private class AsyncLoadTask extends AsyncTask<Void, Void, Void> {
		private ProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
//			dialog = new ProgressDialog(getActivity());
//			dialog.setMessage("数据加载中...");
//			dialog.setTitle("数据加载");
//			dialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// // TODO Auto-generated method stub
			// TrainStation tempFromStation = new TrainStation();
			// TrainStation tempToStation = new TrainStation();
			/** 从数据库中读取所有到站名 **/
			// List<HistoryRecords> historyRecordsList = new
			// ArrayList<HistoryRecords>();//历史记录list
			List<HistoryStations> historyStationsList = HistoryStationsDBHelper
					.getInstance(getActivity()).getHistoryStationsList(
							Properties.UpdateTime);
			for (int u = 0; u < historyStationsList.size(); u++) {
				Log.d("#########", "###历史记录表"
						+ historyStationsList.get(u).getTation_name());
			}
			for (int i = 0; i < historyStationsList.size(); i++) {
				TrainStation tempStation = new TrainStation();
				tempStation.setStation_name(historyStationsList.get(i)
						.getTation_name());
				tempStation.setTele_code(historyStationsList.get(i)
						.getStation_telecode());
				tempStation.setPinyin(historyStationsList.get(i)
						.getStation_pinyin());
				SourceDateList.add(tempStation);
			}
			for (int u = 0; u < historyStationsList.size(); u++) {
				Log.d("#########", "###2历史记录表"
						+ SourceDateList.get(u).getStation_name());
			}

			// for(int u=0;u<historyRecordsList.size();u++){
			// Log.d("#########",
			// "###历史记录表"+historyRecordsList.get(u).getFrom_station_name()+"#"+historyRecordsList.get(u).getTo_station_name());
			// }
			// for (int a = 0; a < historyRecordsList.size(); a++) {
			// TrainStation tempFromStation = new TrainStation();
			// TrainStation tempToStation = new TrainStation();
			// tempFromStation.setStation_name(historyRecordsList.get(a)
			// .getFrom_station_name());
			// tempFromStation.setTele_code(historyRecordsList.get(a)
			// .getFrom_station_telecode());
			// tempFromStation.setPinyin(historyRecordsList.get(a)
			// .getFrom_station_pinyin());
			// tempToStation.setStation_name(historyRecordsList.get(a)
			// .getTo_station_name());
			// tempToStation.setTele_code(historyRecordsList.get(a)
			// .getTo_station_telecode());
			// tempToStation.setPinyin(historyRecordsList.get(a)
			// .getTo_station_pinyin());
			// SourceDateList.add(tempFromStation);
			// SourceDateList.add(tempToStation);
			// tempFromStation = null;
			// tempToStation = null;
			// }

			// if(SourceDateList.size() > 0){
			//
			// for(int u=0;u<SourceDateList.size();u++){
			// Log.d("#########",
			// "###合并后的最近常用车站表"+SourceDateList.get(u).getStation_name());
			// }
			// for (int i = 0; i < SourceDateList.size() - 1; i++) {
			// for (int j = i + 1; j < SourceDateList.size(); j++) {
			// if (SourceDateList.get(i).getStation_name()
			// .equals(SourceDateList.get(j).getStation_name())) {
			// SourceDateList.remove(j);
			// }
			// }
			// }
			//
			// for(int u=0;u<SourceDateList.size();u++){
			// Log.d("#########",
			// "###去重后的最近常用车站表"+SourceDateList.get(u).getStation_name());
			// }
			// }
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Collections.sort(SourceDateList, pinyinComparator);
			adapter.updateListView(SourceDateList);
//			dialog.dismiss();
		}

	}

	// ClearEditTextListener listener = new ClearEditTextListener() {
	//
	// public void ClearEditTextChange(String str) {
	// // TODO Auto-generated method stub
	// filterData(str);
	// }
	// };

	/**
	 * ΪListView������
	 * 
	 * @param date
	 * @return
	 */
	// private List<SortModel> filledData(String [] date){
	// List<SortModel> mSortList = new ArrayList<SortModel>();
	//
	// for(int i=0; i<date.length; i++){
	// SortModel sortModel = new SortModel();
	// sortModel.setName(date[i]);
	// //����ת����ƴ��
	// String pinyin = characterParser.getSelling(date[i]);
	// String sortString = pinyin.substring(0, 1).toUpperCase();
	//
	// // ������ʽ���ж�����ĸ�Ƿ���Ӣ����ĸ
	// if(sortString.matches("[A-Z]")){
	// sortModel.setSortLetters(sortString.toUpperCase());
	// }else{
	// sortModel.setSortLetters("#");
	// }
	//
	// mSortList.add(sortModel);
	// }
	// return mSortList;
	//
	// }

	/**
	 * ���������е�ֵ��������ݲ�����ListView
	 * 
	 * @param filterStr
	 */
	private void filterData(String filterStr) {

		List<TrainStation> filterDateList = new ArrayList<TrainStation>();

		if (TextUtils.isEmpty(filterStr)) {
			filterDateList = SourceDateList;
		} else {
			filterDateList.clear();
			for (TrainStation sortModel : SourceDateList) {
				String name = sortModel.getStation_name();
				if (name.indexOf(filterStr.toString()) != -1
						|| characterParser.getSelling(name).startsWith(
								filterStr.toString())) {
					filterDateList.add(sortModel);
				}
			}
		}

//		Collections.sort(filterDateList, pinyinComparator);
		adapter.updateListView(filterDateList);
	}

}
