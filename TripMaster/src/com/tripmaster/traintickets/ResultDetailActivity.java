package com.tripmaster.traintickets;

import java.util.List;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.trip.trainnumber.entity.dao.TrainNumber;
import com.tripmaster.R;
import com.tripmaster.base.BaseFragmentActivity;
import com.tripmaster.http.TrainNoRequest;
import com.tripmaster.http.TrainNoRequest.HttpRequestResponseListener;
import com.tripmaster.http.TrainNumberJingGuoZhan;
import com.tripmaster.util.Constant;

/***
 * 查询结果详情界面
 * **/
public class ResultDetailActivity extends BaseFragmentActivity 
//implements OnClickListener 
 implements HttpRequestResponseListener
{
	private TextView trainName;
	private TextView ticketPrice;
	private TextView startTime;
	private TextView endTime;
	private TextView startStation;
	private TextView endStation;
	private ImageView startImage;
	private ImageView endImage;
	private TextView useTime;
	private TextView useStation;
	private TextView type;
	private TextView number;
	private TrainNumber  currentNumber;
	
	private RecyclerView recyclerView;//车次详情列表
	private ResultDetailRecyclerViewAdapter adapter;//车次站序列表的适配器
	private ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_searchresult_detail);
		trainName = (TextView) findViewById(R.id.trainName);
		ticketPrice = (TextView) findViewById(R.id.ticketPrice);
		startTime = (TextView)findViewById(R.id.startTime);
		endTime = (TextView) findViewById(R.id.endTime);
		startStation = (TextView) findViewById(R.id.startStation);
		endStation = (TextView) findViewById(R.id.endStation);
		useTime = (TextView) findViewById(R.id.useTime);
		useStation = (TextView) findViewById(R.id.useStation);
		type = (TextView) findViewById(R.id.type);
		number = (TextView) findViewById(R.id.number);
		startImage = (ImageView) findViewById(R.id.imageStart);
		endImage = (ImageView) findViewById(R.id.imageEnd);

		Intent intent = getIntent();
		if (intent != null) {
			String t = intent.getStringExtra("trainNumber");
			if(t != null)
				currentNumber = JSON.parseObject(t, TrainNumber.class);//车次对象
			
			trainName.setText(intent.getStringExtra("trainName"));
			ticketPrice.setText(intent.getStringExtra("ticketPrice"));
			startTime.setText(intent.getStringExtra("startTime"));
			endTime.setText(intent.getStringExtra("endTime"));
			startStation.setText(intent.getStringExtra("startStation"));
			endStation.setText(intent.getStringExtra("endStation"));
			if (intent.getStringExtra("useTime") != null)
				useTime.setText(intent.getStringExtra("useTime"));
			if (intent.getStringExtra("useStation") != null)
				useStation.setText(intent.getStringExtra("useStation"));
			type.setText(intent.getStringExtra("type"));
			number.setText(intent.getStringExtra("number"));
			startImage.setImageResource(intent.getBooleanExtra("startImage", true) ? R.drawable.start : R.drawable.transit_end);
			endImage.setImageResource(intent.getBooleanExtra("endImage", true) ? R.drawable.end : R.drawable.transit_end);	
		}
		
		//初始化列表
		recyclerView = (RecyclerView) findViewById(R.id.detaillist);
		GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
		recyclerView.setLayoutManager(gridLayoutManager);
		adapter = new ResultDetailRecyclerViewAdapter();
		recyclerView.setAdapter(adapter);
		
		initData();
		
		initTitle(currentNumber.getStation_train_code(), true);//设置标题
	}
	
	/**
	 * 初始化数据
	 * 需要拉取车次详情
	 * **/
	private void initData() {
		dialog = new ProgressDialog(this);
		dialog.setTitle("网络请求");
		dialog.setMessage("数据拉取中......");
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
		TrainNoRequest request = new TrainNoRequest(this, this);
		
		request.queryTrainNo(currentNumber.getTrain_no(), currentNumber.getFrom_station_telecode(),
				currentNumber.getTo_station_telecode(), Constant.afterNDay(5));
	}

	@Override
	public void onFailure() {
		if(dialog != null && dialog.isShowing())
			dialog.dismiss();
	}

	/**
	 * 拉取车次成功回调
	 * **/
	@Override
	public void onSuccess(List<TrainNumberJingGuoZhan> guoZhans) {
		adapter.setGuoZhans(guoZhans);
		if(dialog != null && dialog.isShowing())
			dialog.dismiss();
	}

}
