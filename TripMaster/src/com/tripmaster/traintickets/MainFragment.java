package com.tripmaster.traintickets;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shizhefei.view.indicator.FixedIndicatorView;
import com.shizhefei.view.indicator.IndicatorViewPager;
import com.shizhefei.view.indicator.IndicatorViewPager.IndicatorFragmentPagerAdapter;
import com.shizhefei.view.indicator.IndicatorViewPager.IndicatorPagerAdapter;
import com.shizhefei.view.indicator.IndicatorViewPager.IndicatorViewPagerAdapter;
import com.shizhefei.view.indicator.slidebar.ColorBar;
import com.shizhefei.view.indicator.transition.OnTransitionTextListener;
import com.tripmaster.R;
import com.tripmaster.R.color;
import com.tripmaster.R.id;
import com.tripmaster.R.layout;
import com.tripmaster.base.BaseFragment;
import com.tripmaster.personal_center.PersonalCenterFragment;
import com.tripmaster.travel.TravelFragment;

/**
 * 火车票tab页面
 * **/
public class MainFragment extends BaseFragment {
	private IndicatorViewPager indicatorViewPager;
	private MyAdapter mAdapter;

	@Override
	protected void onCreateView(Bundle savedInstanceState) {
		super.onCreateView(savedInstanceState);
		setContentView(R.layout.fragment_tabmain);
		Resources res = getResources();

		ViewPager viewPager = (ViewPager) findViewById(R.id.fragment_tabmain_viewPager);
		FixedIndicatorView indicator = (FixedIndicatorView) findViewById(R.id.fragment_tabmain_indicator);

		indicator.setScrollBar(new ColorBar(getApplicationContext(), res.getColor(R.color.tab_top_text_pressed), 5));

		float unSelectSize = 16;
		float selectSize = unSelectSize * 1.2f;

		int selectColor = res.getColor(R.color.tab_top_text_pressed);
		int unSelectColor = res.getColor(R.color.tab_top_text);
		indicator.setOnTransitionListener(new OnTransitionTextListener(selectSize, unSelectSize, selectColor, unSelectColor));

		viewPager.setOffscreenPageLimit(2);

		indicatorViewPager = new IndicatorViewPager(indicator, viewPager);
		mAdapter = new MyAdapter(getChildFragmentManager());
		indicatorViewPager.setAdapter(mAdapter);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(mAdapter!=null && indicatorViewPager !=null){
			Fragment fragment = mAdapter.getFragmentForPage(indicatorViewPager.getCurrentItem());
			if(fragment != null)
				fragment.onActivityResult(requestCode, resultCode, data);			
		}
	}


	private class MyAdapter extends IndicatorFragmentPagerAdapter {
		private String[] tabNames = {"火车票", "飞机票" };
		private int[] tabIcons = { R.drawable.tab_main_train_icon_selector, R.drawable.tab_main_plane_icon_selector};
		private LayoutInflater inflater;

		public MyAdapter(FragmentManager fragmentManager) {
			super(fragmentManager);
			inflater = LayoutInflater.from(getApplicationContext());
		}

		@Override
		public int getCount() {
			return tabNames.length;
		}

		@Override
		public View getViewForTab(int position, View convertView, ViewGroup container) {
			if (convertView == null) {
				convertView = (TextView) inflater.inflate(R.layout.tab_main, container, false);
			}
			TextView textView = (TextView) convertView;
			textView.setText(tabNames[position]);
			Resources res = getResources();
			Drawable myImage = res.getDrawable(tabIcons[position]);
			myImage.setBounds(40, 0, myImage.getIntrinsicWidth()+40, myImage.getIntrinsicHeight());
			textView.setCompoundDrawables(myImage, null, null, null);
//			textView.setCompoundDrawablesWithIntrinsicBounds(tabIcons[position], 0, 0, 0);
			return textView;
		}

		@Override
		public Fragment getFragmentForPage(int position) {
			BaseFragment bf = null;
			switch (position) {

			case 0:
//				bf = new TrainTicketsFragment();
				bf = TrainTicketsFragment.getInstance();
				
				break;
			case 1:
				bf = new AirfareFragment();
				break;
			}
			return bf;
		}
	}

}
