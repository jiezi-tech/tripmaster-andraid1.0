package com.tripmaster.traintickets.transit;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.trip.trainnumber.entity.dao.TrainNumber;
import com.trip.trainnumber.entity.dao.TrainNumberJingGuoZhanPrice;
import com.trip.trainnumber.entity.dao.TrainNumberSearchResult;
import com.trip.trainnumber.entity.dao.TrainNumberTransitSearchResult;
import com.trip.trainnumber.entity.dao.TrainStation;
import com.tripmaster.R;
import com.tripmaster.base.BaseFragmentActivity;
import com.tripmaster.base.dbhelper.TrainNumberJingGuoZhanPriceDBHelper;
import com.tripmaster.http.TrainNoRequest;
import com.tripmaster.http.TrainNoRequest.HttpRequestResponseListener;
import com.tripmaster.http.TrainNumberJingGuoZhan;
import com.tripmaster.util.Constant;

/***
 * 查询结果详情界面
 * **/
public class TransitResultDetailActivity extends BaseFragmentActivity 
//implements OnClickListener 
{
	private TextView startTrainName;
	private TextView startTicketPrice;
	private TextView startStartTime;
	private TextView startEndTime;
	private TextView startStartStation;
	private TextView startEndStation;
	private TextView startUseTime;
	private TextView startUseStation;
	private TextView startType;
	private TextView startNumber;
	
	private TextView endTrainName;
	private TextView endTicketPrice;
	private TextView endStartTime;
	private TextView endEndTime;
	private TextView endStartStation;
	private TextView endEndStation;
	private TextView endUseTime;
	private TextView endUseStation;
	private TextView endType;
	private TextView endNumber;
	
	private TrainNumber  currentNumber;
	
	private ProgressDialog dialog;
	private ListView startListView;
	private TransitResultDetailAdapter startAdapter;
	private ListView endListView;
	private TransitResultDetailAdapter endAdapter;
	private TrainNumberJingGuoZhanPrice fromPrice,toPrice;
	private TrainNumberJingGuoZhanPriceDBHelper helper;
	
	private List<TrainNumberJingGuoZhanPrice> toGuoZhanPrices,fromGuoZhanPrices;
	
	private Handler handler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_transit_result_detail);

		initStartView();
		initEndView();
		
		helper = TrainNumberJingGuoZhanPriceDBHelper.getInstance();
		initTitle("中转详情", true);//设置标题
		
		dialog = new ProgressDialog(this);
		dialog.setMessage("数据读取中......");
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
		
		handler = new Handler(new Handler.Callback() {
			
			@Override
			public boolean handleMessage(Message msg) {
				// TODO Auto-generated method stub
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				if (msg.what == 100) {
					initData();
				}
				return false;
			}
		});
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				getTrainNumberJingGuoZhanPrice();
			}
		}).start();
		
	}
	

	/** 拼接票价 ***/
	private String getPrice(TrainNumberJingGuoZhanPrice n) {
		StringBuffer buffer = new StringBuffer();
		if (n.getGr_price() != null && !n.getGr_price().isEmpty())
			buffer.append("高级软卧:").append(n.getGr_price());
		if (n.getRz_price() != null && !n.getRz_price().isEmpty())
			buffer.append(" 软座:").append(n.getRz_price());
		if (n.getTz_price() != null && !n.getTz_price().isEmpty())
			buffer.append(" 特等座:").append(n.getTz_price());
		if (n.getSwz_price() != null && !n.getSwz_price().isEmpty())
			buffer.append(" 商务座:").append(n.getSwz_price());
		if (n.getZy_price() != null && !n.getZy_price().isEmpty())
			buffer.append(" 一等座:").append(n.getZy_price());
		if (n.getZe_price() != null && !n.getZe_price().isEmpty())
			buffer.append(" 二等座:").append(n.getZe_price());
		if (n.getRw_price() != null && !n.getRw_price().isEmpty())
			buffer.append(" 软卧:").append(n.getRw_price());
		if (n.getYw_price() != null && !n.getYw_price().isEmpty())
			buffer.append(" 硬卧:").append(n.getYw_price());
		if (n.getYz_price() != null && !n.getYz_price().isEmpty())
			buffer.append(" 硬座:").append(n.getYz_price());
		if (n.getWz_price() != null && !n.getWz_price().isEmpty())
			buffer.append(" 无座:").append(n.getWz_price());

		return buffer.toString();
	}
	
	
	private void initStartView() {
		LinearLayout startView = (LinearLayout) findViewById(R.id.startView);
		startTrainName = (TextView) startView.findViewById(R.id.trainName);
		startTicketPrice = (TextView) startView.findViewById(R.id.ticketPrice);
		startStartTime = (TextView)startView.findViewById(R.id.startTime);
		startEndTime = (TextView) startView.findViewById(R.id.endTime);
		startStartStation = (TextView) startView.findViewById(R.id.startStation);
		startEndStation = (TextView) startView.findViewById(R.id.endStation);
		startUseTime = (TextView) startView.findViewById(R.id.useTime);
		startUseStation = (TextView) startView.findViewById(R.id.useStation);
		startType = (TextView) startView.findViewById(R.id.type);
		startNumber = (TextView) startView.findViewById(R.id.number);
		
		startListView = (ListView) startView.findViewById(R.id.detaillist);
		startAdapter = new TransitResultDetailAdapter();
		startListView.setAdapter(startAdapter);
	}
	
	private void initEndView() {
		LinearLayout endView = (LinearLayout) findViewById(R.id.endView);
		endTrainName = (TextView) endView.findViewById(R.id.trainName);
		endTicketPrice = (TextView) endView.findViewById(R.id.ticketPrice);
		endStartTime = (TextView)endView.findViewById(R.id.startTime);
		endEndTime = (TextView) endView.findViewById(R.id.endTime);
		endStartStation = (TextView) endView.findViewById(R.id.startStation);
		endEndStation = (TextView) endView.findViewById(R.id.endStation);
		endUseTime = (TextView) endView.findViewById(R.id.useTime);
		endUseStation = (TextView) endView.findViewById(R.id.useStation);
		endType = (TextView) endView.findViewById(R.id.type);
		endNumber = (TextView) endView.findViewById(R.id.number);		
		
		endListView = (ListView) endView.findViewById(R.id.detaillist);
		endAdapter = new TransitResultDetailAdapter();
		endListView.setAdapter(endAdapter);

	}
	
	private void getTrainNumberJingGuoZhanPrice() {
		TrainNumberTransitSearchResult result = (TrainNumberTransitSearchResult) getIntent().getSerializableExtra("result");
		String start_train_code = result.getTransfer_start_train_code();//转前车次
		String end_train_code = result.getTransfer_end_train_code();//转后车次
		String zhongzhuanCity = result.getZhongzhuan_city();//中转城市
		TrainNumberJingGuoZhanPriceDBHelper priceDBHelper = TrainNumberJingGuoZhanPriceDBHelper.getInstance();

		TrainStation fromStation = (TrainStation) getIntent().getSerializableExtra("fromStation");
		TrainStation toStation = (TrainStation) getIntent().getSerializableExtra("toStation");
		
		//获得转前、转后车次对象
		fromPrice = priceDBHelper.queryTrainNumber(start_train_code, fromStation.getCity_name(), zhongzhuanCity);
		toPrice = priceDBHelper.queryTrainNumber(end_train_code, zhongzhuanCity, toStation.getCity_name());
		
		Log.i("lanou", fromPrice.getStation_train_code()+"  "+fromPrice.getStart_station_name()+"   "+fromPrice.getEnd_station_name());
		Log.i("lanou", toPrice.getStation_train_code()+"  "+toPrice.getStart_station_name()+"   "+toPrice.getEnd_station_name());

		
		fromGuoZhanPrices = new ArrayList<TrainNumberJingGuoZhanPrice>();
		TrainNumberJingGuoZhanPrice start = new TrainNumberJingGuoZhanPrice();
		start.setTo_station_no("01");
		start.setEnd_station_name(fromPrice.getStart_station_name());
		start.setArrive_time("--");
		start.setStart_time(fromPrice.getStart_time());
		start.setStopover_time("--");
		fromGuoZhanPrices.add(start);
		fromGuoZhanPrices.addAll(helper.queryTrainNumberJingGuoZhanList(fromPrice.getStation_train_code()));
		
		toGuoZhanPrices = new ArrayList<TrainNumberJingGuoZhanPrice>();
		TrainNumberJingGuoZhanPrice end = new TrainNumberJingGuoZhanPrice();
		end.setTo_station_no("01");
		end.setEnd_station_name(toPrice.getStart_station_name());
		end.setArrive_time("--");
		end.setStart_time(toPrice.getStart_time());
		end.setStopover_time("--");
		toGuoZhanPrices.add(end);
		toGuoZhanPrices.addAll(helper.queryTrainNumberJingGuoZhanList(toPrice.getStation_train_code()));
		
		handler.sendEmptyMessage(100);

	}
	
	/**
	 * 初始化数据
	 * 需要拉取车次详情
	 * **/
	private void initData() {
		startAdapter.setGuoZhans(fromGuoZhanPrices);
		endAdapter.setGuoZhans(toGuoZhanPrices);
		
		startTrainName.setText(fromPrice.getStation_train_code());
		startTicketPrice.setText(getPrice(fromPrice));
		startStartTime.setText(fromPrice.getStart_time());
		startEndTime.setText(fromPrice.getArrive_time());
		startStartStation.setText(fromPrice.getStart_station_name());
		startEndStation.setText(fromPrice.getEnd_station_name());
		if (fromPrice.getAllTime()!= null){
			int hour =0,minute=0;
			try{
				int alltime = Integer.parseInt(fromPrice.getAllTime());
				hour = alltime/60;
				minute = alltime%60;
			}catch(NumberFormatException e){
				e.printStackTrace();
			}
			startUseTime.setText((hour>0?hour+"小时":"")+(minute>0?minute+"分钟":""));
		}

		String allMileage = new String();
		try {
			int from_no = Integer.parseInt(fromPrice.getFrom_station_no());
			int to_no = Integer.parseInt(fromPrice.getTo_station_no());
			allMileage = (to_no - from_no) + "站";
		} catch (NumberFormatException e) {

		}
		if (fromPrice.getAllMileage() != null)
			allMileage += " " + fromPrice.getAllMileage()+"公里";
		startUseStation.setText(allMileage);
		

		endTrainName.setText(toPrice.getStation_train_code());
		endTicketPrice.setText(getPrice(toPrice));
		endStartTime.setText(toPrice.getStart_time());
		endEndTime.setText(toPrice.getArrive_time());
		endStartStation.setText(toPrice.getStart_station_name());
		endEndStation.setText(toPrice.getEnd_station_name());
		if (toPrice.getAllTime() != null){
			int hour =0,minute=0;
			try{
				int alltime = Integer.parseInt(toPrice.getAllTime());
				hour = alltime/60;
				minute = alltime%60;
			}catch(NumberFormatException e){
				e.printStackTrace();
			}
			endUseTime.setText((hour>0?hour+"小时":"")+(minute>0?minute+"分钟":""));
		}
		
		allMileage = new String();
		try {
			int from_no = Integer.parseInt(toPrice.getFrom_station_no());
			int to_no = Integer.parseInt(toPrice.getTo_station_no());
			allMileage = (to_no - from_no) + "站";
		} catch (NumberFormatException e) {

		}
		if (toPrice.getAllMileage() != null)
			allMileage += " " + toPrice.getAllMileage()+"公里";
		endUseStation.setText(allMileage);
	
		if(currentNumber != null)
			initTitle(currentNumber.getStation_train_code(), true);//设置标题
		else
			initTitle("中转详情", true);//设置标题
	}


}
