package com.tripmaster.traintickets.transit;

import java.util.List;

import com.trip.trainnumber.entity.dao.TrainNumberTransitSearchResult;
import com.tripmaster.R;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class TransitSearchResultAdapter extends BaseAdapter{
	private List<TrainNumberTransitSearchResult> results;//存放查询结果的集合
	private String fromStation,toStation;
	
	public void setFromStation(String fromStation) {
		this.fromStation = fromStation;
	}
	
	public void setToStation(String toStation) {
		this.toStation = toStation;
	}
	
	
	public void setResults(List<TrainNumberTransitSearchResult> results) {
		this.results = results;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return results != null && results.size() >0 ? results.size():0;
	}

	@Override
	public TrainNumberTransitSearchResult getItem(int position) {
		// TODO Auto-generated method stub
		return results.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return results.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if(convertView == null){
			convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.transit_search_result_list_item, null);
			holder = new ViewHolder();
			holder.priceTv = (TextView) convertView.findViewById(R.id.priceTv);
			holder.allLiShiTv = (TextView) convertView.findViewById(R.id.allLiShiTv);
			holder.trainNumberTv = (TextView) convertView.findViewById(R.id.trainNumberTv);
			holder.startStationTv = (TextView) convertView.findViewById(R.id.startStationTv);
			holder.endStationTv = (TextView) convertView.findViewById(R.id.endStationTv);
			holder.leftStopTip = (TextView) convertView.findViewById(R.id.leftStopTip);
			holder.rightTransitTip = (TextView) convertView.findViewById(R.id.rightTransitTip);
			holder.trainNumberTransitTv = (TextView) convertView.findViewById(R.id.trainNumberTransitTv);
			holder.startStationTransitTv = (TextView) convertView.findViewById(R.id.startStationTransitTv);
			holder.endStationTransitTv = (TextView) convertView.findViewById(R.id.endStationTransitTv);
			convertView.setTag(holder);
		}
		else
			holder = (ViewHolder) convertView.getTag();
		
		TrainNumberTransitSearchResult searchResult = getItem(position);
		float fPrice=0,sPrice=0;
			try{
				fPrice = Float.parseFloat(searchResult.getFirst_price());
			}catch(NumberFormatException e){
				e.printStackTrace();
			}
			try{
				sPrice = Float.parseFloat(searchResult.getSecond_price());
			}catch(NumberFormatException e){
				e.printStackTrace();
			}
		holder.priceTv.setText("共：¥"+(fPrice+sPrice));//总价格
			
		int totalTime = 0, endDay = 0, hour = 0, minute = 0;
		try {
			if (!searchResult.getTotal_time().isEmpty())
				totalTime = Integer.parseInt(searchResult.getTotal_time());
			hour = totalTime / 60;
			minute = totalTime % 60;
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		try {
			if (!searchResult.getEnd_day().isEmpty())
				endDay = Integer.parseInt(searchResult.getEnd_day());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		holder.allLiShiTv.setText("总历时："+(endDay>0?endDay+"天":"")+(hour > 0?hour+"小时":"")+(minute>0?minute+"分钟":""));//总历时

		//转前车次
		try{
			int fisrtTime=Integer.parseInt(searchResult.getFirst_total_time());
			int hour_first= fisrtTime/60;
			int minute_first = fisrtTime%60;
			holder.trainNumberTv.setText(searchResult.getTransfer_start_train_code()+"("+(hour_first>0?hour_first+"时":"")+(minute_first>0?minute_first+"分":"")+")");
		}catch(NumberFormatException e){
			e.printStackTrace();
		}
		

		//转前车次-起始站名－终到站名
		holder.startStationTv.setText(searchResult.getStart_time()+" "+fromStation);
		holder.endStationTv.setText(searchResult.getTransfer_start_time()+" "+searchResult.getTransfer_start_station());

		int totalTrainTime = 0,hour_train=0,minute_train =0;
		try{
			totalTrainTime = Integer.parseInt(searchResult.getTransfer_time());
			hour_train=totalTrainTime/60;
			minute_train = totalTrainTime%60;
		}catch(NumberFormatException e){
			e.printStackTrace();
		}
		holder.leftStopTip.setText("停留"+(hour_train>0?hour_train+"小时":"")+(minute_train>0?minute_train+"分钟":"")+","+searchResult.getZhongzhuan_city()+"中转");
		

		//是否在同一车站换乘
		if(searchResult.getTransfer_start_station().equals(searchResult.getTransfer_end_station()))
			holder.rightTransitTip.setText("同一车站换乘");
		else
			holder.rightTransitTip.setText("不同车站换乘");
		
		//转后车次
		try{
			int fisrtTime=Integer.parseInt(searchResult.getSecond_total_time());
			int hour_first= fisrtTime/60;
			int minute_first = fisrtTime%60;
			holder.trainNumberTransitTv.setText(searchResult.getTransfer_end_train_code()+"("+(hour_first>0?hour_first+"时":"")+(minute_first>0?minute_first+"分":"")+")");
		}catch(NumberFormatException e){
			e.printStackTrace();
		}

		//转后车次 始发站－终到站
		holder.startStationTransitTv.setText(searchResult.getTransfer_arrive_time()+" "+searchResult.getTransfer_end_station());
		holder.endStationTransitTv.setText(searchResult.getArrive_time()+" "+toStation);
		
		
		return convertView;
	}
	
	class ViewHolder{
		TextView priceTv;//总票价
		TextView allLiShiTv;//总历时
		TextView trainNumberTv;//转前车次
		TextView startStationTv;//转前始发站
		TextView endStationTv;//转前终到站
		TextView leftStopTip;//换乘提示
		TextView rightTransitTip;//换乘中间时间间隔提示
		TextView trainNumberTransitTv;//转后车次
		TextView startStationTransitTv;//转后始发站
		TextView endStationTransitTv;//转后终到站
		
	}

}
