package com.tripmaster.traintickets.transit;

import java.io.Serializable;
import java.util.List;

import de.greenrobot.dao.Property;

/**
 * This Class Is Used To Record The Transit Filter Properties
 * @author stemonzhang
 *
 */
public class TransitSearchFilterProperties implements Serializable {

	private List<SearchProperty> mProperty;            //单一属性：标准属性 
	private String[] mStartTimeDuration;         //复合属性：始发时间阈值
	private String[] mEndTimeDuration;           //复合属性：到站时间阈值
	private String mBeforeTransitTrainType;      //单一属性：始发车类型
	private String mTransitArrivedStation;       //单一属性：中转到站城市名称
	private String mTransitArrivedCityName;      //单一属性：中转到站车站名称
	private String[] mTransitArrivedTimeDuration;//复合属性：中转到站时间阈值
	private String mAfterTransitTrainType;       //单一属性：中转车类型
	private String mTransitGoStation;            //单一属性：中转出发站名称
	private String mTransitGoCityName;           //单一属性：中转出发城市名称
	private String[] mTransitGoTimeDuration;     //复合属性：中转发车时间
	
	public TransitSearchFilterProperties() {
		super();
	}
	public List<SearchProperty> getmProperty() {
		return mProperty;
	}
	public void setmProperty(List<SearchProperty> mProperty) {
		this.mProperty = mProperty;
	}
	public String[] getmStartTimeDuration() {
		return mStartTimeDuration;
	}
	public void setmStartTimeDuration(String[] mStartTimeDuration) {
		this.mStartTimeDuration = mStartTimeDuration;
	}
	public String[] getmEndTimeDuration() {
		return mEndTimeDuration;
	}
	public void setmEndTimeDuration(String[] mEndTimeDuration) {
		this.mEndTimeDuration = mEndTimeDuration;
	}
	public String getmBeforeTransitTrainType() {
		return mBeforeTransitTrainType;
	}
	public void setmBeforeTransitTrainType(String mBeforeTransitTrainType) {
		this.mBeforeTransitTrainType = mBeforeTransitTrainType;
	}
	public String getmTransitArrivedStation() {
		return mTransitArrivedStation;
	}
	public void setmTransitArrivedStation(String mTransitArrivedStation) {
		this.mTransitArrivedStation = mTransitArrivedStation;
	}
	public String getmTransitArrivedCityName() {
		return mTransitArrivedCityName;
	}
	public void setmTransitArrivedCityName(String mTransitArrivedCityName) {
		this.mTransitArrivedCityName = mTransitArrivedCityName;
	}
	public String[] getmTransitArrivedTimeDuration() {
		return mTransitArrivedTimeDuration;
	}
	public void setmTransitArrivedTimeDuration(String[] mTransitArrivedTimeDuration) {
		this.mTransitArrivedTimeDuration = mTransitArrivedTimeDuration;
	}
	public String getmAfterTransitTrainType() {
		return mAfterTransitTrainType;
	}
	public void setmAfterTransitTrainType(String mAfterTransitTrainType) {
		this.mAfterTransitTrainType = mAfterTransitTrainType;
	}
	public String getmTransitGoStation() {
		return mTransitGoStation;
	}
	public void setmTransitGoStation(String mTransitGoStation) {
		this.mTransitGoStation = mTransitGoStation;
	}
	public String getmTransitGoCityName() {
		return mTransitGoCityName;
	}
	public void setmTransitGoCityName(String mTransitGoCityName) {
		this.mTransitGoCityName = mTransitGoCityName;
	}
	public String[] getmTransitGoTimeDuration() {
		return mTransitGoTimeDuration;
	}
	public void setmTransitGoTimeDuration(String[] mTransitGoTimeDuration) {
		this.mTransitGoTimeDuration = mTransitGoTimeDuration;
	}
	
	
	
}
