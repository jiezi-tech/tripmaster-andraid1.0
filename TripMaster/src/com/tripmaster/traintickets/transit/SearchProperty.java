package com.tripmaster.traintickets.transit;

import java.io.Serializable;

import de.greenrobot.dao.Property;

/**
 * 排序形式以及该排序是生序还是降序
 * @author stemonzhang
 *
 */
public class SearchProperty implements Serializable {
	private Property mProperty;//排序形式
	private String mOrderStyle;//排序方式
	
	public Property getmProperty() {
		return mProperty;
	}

	public void setmProperty(Property mProperty) {
		this.mProperty = mProperty;
	}

	public String getmOrderStyle() {
		return mOrderStyle;
	}

	public void setmOrderStyle(String mOrderStyle) {
		this.mOrderStyle = mOrderStyle;
	}

	public SearchProperty() {
		super();
	}
	
	
}
