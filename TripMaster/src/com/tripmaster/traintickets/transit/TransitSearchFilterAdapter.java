package com.tripmaster.traintickets.transit;

import java.util.List;

import com.trip.trainnumber.entity.dao.TrainNumberTrainsitSearchOtherResult;
import com.tripmaster.R;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class TransitSearchFilterAdapter extends RecyclerView.Adapter<TransitSearchFilterAdapter.ViewHolder>{
	private int selectIndex = -1;
	private List<TrainNumberTrainsitSearchOtherResult> results ;
	
	public void setResults(List<TrainNumberTrainsitSearchOtherResult> results) {
		this.results = results;
		notifyDataSetChanged();
	}
	
	/**
	 * 设置当前过滤条件
	 * **/
	public void setSelectRadioButton(int position) {
		selectIndex = position;
		notifyDataSetChanged();//刷选页面
	}

	class ViewHolder extends RecyclerView.ViewHolder{
		TextView stationNameTv;//中转城市
		RadioGroup filterRadioGroup;//中转情况
		
		RadioButton beforeCountRB;//转前车数
		RadioButton afterCountRB;//转后车数
		RadioButton biggestMileageRB;//最大里程
		RadioButton leastMileageRB;//最小里程
		RadioButton biggestLiShiRB;//最大历时
		RadioButton leastLiShiRB;//最小历时

		public ViewHolder(View v) {
			super(v);
			initView(v);
		}
		
		private void initView(View v) {
			stationNameTv = (TextView) v.findViewById(R.id.stationNameTv);
			filterRadioGroup = (RadioGroup) v.findViewById(R.id.filterRadioGroup);
			beforeCountRB = (RadioButton) v.findViewById(R.id.beforeCountRB);
			afterCountRB = (RadioButton) v.findViewById(R.id.afterCountRB);
			biggestMileageRB = (RadioButton) v.findViewById(R.id.biggestMileageRB);
			leastMileageRB = (RadioButton) v.findViewById(R.id.leastMileageRB);
			biggestLiShiRB = (RadioButton) v.findViewById(R.id.biggestLiShiRB);
			leastLiShiRB = (RadioButton) v.findViewById(R.id.leastLiShiRB);
			
		}
		
	}

	@Override
	public int getItemCount() {
		// TODO Auto-generated method stub
		return results != null && results.size() > 0?results.size():0;
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		updateRadioGroup(holder);
		TrainNumberTrainsitSearchOtherResult result = results.get(position);
		holder.stationNameTv.setText(result.getZhongzhuan_city());
		holder.beforeCountRB.setText("转前车数："+result.getTransferStartCount());
		holder.afterCountRB.setText("转后车数："+result.getTransferEndCount());
		holder.biggestMileageRB.setText("最大总里程："+result.getMaxMileage()+"公里");
		holder.leastMileageRB.setText("最小总里程："+result.getMinMileage()+"公里");
		holder.biggestLiShiRB.setText("最大总历时："+result.getMaxTime()+"小时");
		holder.leastLiShiRB.setText("最小总历时："+result.getMinTime()+"小时");
		
	}
	
	private void updateRadioGroup(ViewHolder holder) {
		switch (selectIndex) {
		case 0:
			holder.filterRadioGroup.check(R.id.afterCountRB);
			break;
		case 1:
			holder.filterRadioGroup.check(R.id.beforeCountRB);
			break;
		case 2:
			holder.filterRadioGroup.check(R.id.biggestMileageRB);
			break;
		case 3:
			holder.filterRadioGroup.check(R.id.leastMileageRB);
			break;
		case 4:
			holder.filterRadioGroup.check(R.id.biggestLiShiRB);
			break;
		case 5:
			holder.filterRadioGroup.check(R.id.leastLiShiRB);
			break;
		}

	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup group, int position) {
		View v = LayoutInflater.from(group.getContext()).inflate(R.layout.transit_search_filter_listview_item, null);
		return new ViewHolder(v);
	}

}
