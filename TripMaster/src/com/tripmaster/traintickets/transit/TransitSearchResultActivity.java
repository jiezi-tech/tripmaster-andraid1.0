package com.tripmaster.traintickets.transit;

import java.util.List;

import com.trip.trainnumber.entity.dao.TrainNumberJingGuoZhanPrice;
import com.trip.trainnumber.entity.dao.TrainNumberTransitSearchResult;
import com.trip.trainnumber.entity.dao.TrainNumberTransitSearchResultDao.Properties;
import com.trip.trainnumber.entity.dao.TrainStation;
import com.tripmaster.R;
import com.tripmaster.base.BaseFragmentActivity;
import com.tripmaster.base.dbhelper.TrainNumberJingGuoZhanPriceDBHelper;
import com.tripmaster.base.dbhelper.TrainNumberTransitSearchResultDBHelper;
import com.tripmaster.util.Constant;
import com.tripmaster.widget.xlistview.XListView;
import com.tripmaster.widget.xlistview.XListView.IXListViewListener;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;


/**
 * 中转搜索结果列表
 * **/
public class TransitSearchResultActivity extends BaseFragmentActivity
		implements OnClickListener, OnCheckedChangeListener, IXListViewListener, OnItemClickListener {

	private TrainStation fromStation;// 出发站
	private TrainStation toStation;// 到达站
	private String startDate;// 出发日期

	private LinearLayout yesterday;
	private LinearLayout tomorrow;
	
	private XListView listView;
	private ViewStub emptyView;
	private TransitSearchResultAdapter adapter;
	
	private TextView todayTextView;// 当前时间
	private RadioGroup filterRadioGroup;
	
	private TrainNumberTransitSearchResultDBHelper helper;
	private List<TrainNumberTransitSearchResult> results;//存放查询结果的集合
	private int maxPager = 0;//共可以显示多少页
	private int currentPager = 0;//当前显示的是第几页
	private int limit =100;//每页显示的最大数
	private Handler handler;
	private static final int QUERY_FINISH = 100;//中转查询结束
	private ProgressDialog dialog;
	private TransitSearchFilterProperties transitSearchFilterProperties;//过滤条件

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_transit_search_result);
		initView();
		initData();
	}

	private void initView() {
		yesterday = (LinearLayout) findViewById(R.id.yesterday);
		yesterday.setOnClickListener(this);
		tomorrow = (LinearLayout) findViewById(R.id.tomorrow);
		tomorrow.setOnClickListener(this);

		todayTextView = (TextView) findViewById(R.id.todayTextView);

		listView = (XListView) findViewById(R.id.resultlist);
		emptyView = (ViewStub) findViewById(R.id.emptyView);
		listView.setEmptyView(emptyView);
		listView.setXListViewListener(this);
		listView.setPullRefreshEnable(true);
		listView.setPullLoadEnable(true);
		adapter = new TransitSearchResultAdapter();
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);

		filterRadioGroup = (RadioGroup) findViewById(R.id.filterRadioGroup);
		filterRadioGroup.setOnCheckedChangeListener(this);

		filterButton =  (TextView) findViewById(R.id.filter);
		filterButton.setVisibility(View.VISIBLE);
		filterButton.setOnClickListener(this);
		
		helper = TrainNumberTransitSearchResultDBHelper.getInstance();
	}

	private void initData() {
		handler = new Handler(new Handler.Callback() {
			
			@Override
			public boolean handleMessage(Message msg) {
				if (dialog != null && dialog.isShowing()) {
					dialog.dismiss();
				}
				if (msg.what == QUERY_FINISH) {
					adapter.setResults(results);
					initTitle(
							fromStation.getStation_name() + "--"
									+ toStation.getStation_name()+"(中转)",
									"共" + helper.getCount() + "条", true, true);
					maxPager = (int) (helper.getCount()/limit);//计算当前最大页数
					currentPager = 0;
				}
				return false;
			}
		});
		

		Intent intent = getIntent();
		if (intent != null) {
			toStation = (TrainStation) intent.getSerializableExtra("toStation");
			fromStation = (TrainStation) intent
					.getSerializableExtra("fromStation");
			adapter.setFromStation(fromStation.getStation_name());
			adapter.setToStation(toStation.getStation_name());
			
			startDate = intent.getStringExtra("date");

			if (startDate.equals(Constant.formatCurrentString()))
				todayTextView.setText(startDate + " 今天");
			else
				todayTextView.setText(startDate + " "
						+ Constant.getWeekOfDate(startDate));
			
			dialog = new ProgressDialog(this);
			dialog.setMessage("中转计算中...");
			dialog.setCanceledOnTouchOutside(false);
			dialog.show();
			
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					//先将结果存入到数据库表中
					helper.deleteAll();
					helper.trainNumberTransitSave(fromStation.getStation_name(), toStation.getStation_name());
					results = helper.query(0, limit);
					
					handler.sendEmptyMessage(QUERY_FINISH);
				}
			}).start();
			
			initTitle(
					fromStation.getStation_name() + "--"
							+ toStation.getStation_name()+"(中转)",
							"共" + helper.getCount() + "条", true, true);
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.yesterday:// 前一天
			if (!startDate.equals(Constant.formatCurrentString())) {
				startDate = Constant.beforeDay(startDate);
				if (startDate.equals(Constant.formatCurrentString()))
					todayTextView.setText(startDate + " 今天");
				else
					todayTextView.setText(startDate + " "
							+ Constant.getWeekOfDate(startDate));
			} else {
				todayTextView.setText(startDate + " 今天");
				Toast.makeText(this, "不能再往后选了  已经是当前日期了", Toast.LENGTH_LONG)
						.show();
			}
			break;
		case R.id.tomorrow:// 后一天
			startDate = Constant.afterDay(startDate);
			todayTextView.setText(startDate + " "
					+ Constant.getWeekOfDate(startDate));
			break;
		case R.id.filter://筛选
			Intent intent = new Intent(this, TransitSearchFilterActivity.class);
			startActivityForResult(intent, Constant.TRANSITFILTER);
			break;
		}

	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode == RESULT_OK && data != null){
			transitSearchFilterProperties = (TransitSearchFilterProperties) data.getSerializableExtra(TransitSearchFilterActivity.TRANST_SEARCH_FILTER_RESULT);
			filterResult();
		}
		
	}
	
	/**
	 * 根据条件过滤结果
	 */
	private void filterResult(){
		
		
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		List<TrainNumberTransitSearchResult> results = null;
		currentPager = 0;
		switch (checkedId) {
		case R.id.fastestTransit:
			results = helper.orderAsValue(Properties.Transfer_time,0,limit);
			break;
		case R.id.fisrtArrivals:
			results = helper.orderAsValue(Properties.Arrive_time,0,limit);
			break;
		case R.id.shortestRade:
			results = helper.orderAsValue(Properties.Total_time,0,limit);
			break;
		case R.id.lowestPrice:
			//这个地儿少个字段，需要讨论，看是使用数据库排序，还是在结果中排序。
//			results = helper.orderAsValue(Properties.Arrive_time);
			results = helper.orderPrice(0, limit);
			break;
		case R.id.shortestMileage:
			results = helper.orderAsValue(Properties.Total_mileage,0,limit);
			break;
		}
		if(results != null){
			adapter.setResults(results);
		}

	}

	/**
	 * 下拉刷选
	 * **/
	@Override
	public void onRefresh() {
		
		currentPager --;
		if(currentPager < 0){
			currentPager = 0;
			Toast.makeText(this, "已经到最前面", Toast.LENGTH_LONG).show();
			listView.stopLoadMore();
			listView.stopRefresh();
			return;
		}
		results = helper.query(limit * currentPager, limit);
		adapter.setResults(results);
		
		listView.stopLoadMore();
		listView.stopRefresh();
	}

	/**
	 * 上拉加载更多
	 * **/
	@Override
	public void onLoadMore() {
		currentPager ++;
		if(currentPager > maxPager){
			currentPager = maxPager;
			Toast.makeText(this, "已经到最后面", Toast.LENGTH_LONG).show();
			listView.stopLoadMore();
			listView.stopRefresh();
			return;
		}
		results = helper.query(limit * currentPager, limit);
		adapter.setResults(results);
		
		listView.stopLoadMore();
		listView.stopRefresh();
		
	}

	/**
	 * 列表点击之后的回调事件  需要跳转到详情页
	 * **/
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		TrainNumberTransitSearchResult result = (TrainNumberTransitSearchResult) parent.getItemAtPosition(position);
//		String start_train_code = result.getTransfer_start_train_code();//转前车次
//		String end_train_code = result.getTransfer_end_train_code();//转后车次
//		String zhongzhuanCity = result.getZhongzhuan_city();//中转城市
//		TrainNumberJingGuoZhanPriceDBHelper priceDBHelper = TrainNumberJingGuoZhanPriceDBHelper.getInstance();
//		
//		//获得转前、转后车次对象
//		TrainNumberJingGuoZhanPrice from_Price = priceDBHelper.queryTrainNumber(start_train_code, fromStation.getCity_name(), zhongzhuanCity);
//		TrainNumberJingGuoZhanPrice to_Price = priceDBHelper.queryTrainNumber(end_train_code, zhongzhuanCity, toStation.getCity_name());
//		
//		Log.i("lanou", from_Price.getStation_train_code()+"  "+from_Price.getStart_station_name()+"   "+from_Price.getEnd_station_name());
//		Log.i("lanou", to_Price.getStation_train_code()+"  "+to_Price.getStart_station_name()+"   "+to_Price.getEnd_station_name());
//		
		
		Intent intent = new Intent(this, TransitResultDetailActivity.class);
		intent.putExtra("result", result);
		intent.putExtra("fromStation", fromStation);
		intent.putExtra("toStation", toStation);
//		intent.putExtra("from_Price", from_Price);
//		intent.putExtra("to_Price", to_Price);
		startActivity(intent);
	}

}
