package com.tripmaster.traintickets.transit;

import java.util.ArrayList;
import java.util.List;

import com.trip.trainnumber.entity.dao.TrainNumberTrainsitSearchOtherResult;
import com.trip.trainnumber.entity.dao.TrainNumberTrainsitSearchOtherResultDao;
import com.trip.trainnumber.entity.dao.TrainNumberTransitSearchResultDao.Properties;
import com.tripmaster.R;
import com.tripmaster.base.BaseFragmentActivity;
import com.tripmaster.base.dbhelper.TrainNumberTrainsitSearchOtherResultDBHelper;
import com.tripmaster.util.Constant;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import de.greenrobot.dao.Property;

public class TransitSearchFilterActivity extends BaseFragmentActivity implements OnItemClickListener, OnCheckedChangeListener, OnClickListener{
	public static String TRANST_SEARCH_FILTER_RESULT = "transt_search_filter_result";
	private RadioGroup filterRadioGroup;
	
	private RecyclerView recyclerView;
	private TransitSearchFilterAdapter adapter;
	
	private TextView transitFilterBtn;
	private TextView otherTransitFilterBtn;
	private TransitSearchFilterProperties transitSearchFilterProperties;//过滤条件
	private List<SearchProperty> filterProperties;//标准过滤条件
	private TrainNumberTrainsitSearchOtherResultDBHelper helper;
	
	private Handler handler;
	private static final int QUERY_FINISH = 100;//中转查询结束
	private List<TrainNumberTrainsitSearchOtherResult> results;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_transit_search_result_filter);
		
		initView();
		initData();
	}
	
	private void initView() {
		initTitle("筛选", true);
		recyclerView = (RecyclerView) findViewById(R.id.resultlist);
		GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
		recyclerView.setLayoutManager(gridLayoutManager);
		adapter = new TransitSearchFilterAdapter();
		recyclerView.setAdapter(adapter);
		
		filterRadioGroup = (RadioGroup) findViewById(R.id.filterRadioGroup);
		filterRadioGroup.setOnCheckedChangeListener(this);
		
		transitFilterBtn = (TextView) findViewById(R.id.transitFilterBtn);
		otherTransitFilterBtn = (TextView) findViewById(R.id.otherTransitFilterBtn);
		transitFilterBtn.setOnClickListener(this);
		otherTransitFilterBtn.setOnClickListener(this);

	}
	
	private void initData() {
		// TODO Auto-generated method stub
		transitSearchFilterProperties = new TransitSearchFilterProperties();
		filterProperties = new ArrayList<SearchProperty>();
		helper = TrainNumberTrainsitSearchOtherResultDBHelper.getInstance();
		
		handler = new Handler(new Handler.Callback() {
			
			@Override
			public boolean handleMessage(Message msg) {
				if (msg.what == QUERY_FINISH) {
					adapter.setResults(results);
				}
				return false;
			}
		});
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				helper.deleteAll();
				helper.trainNumberTrainsitSearchOtherResultSave();
				results = helper.loadAll();
				handler.sendEmptyMessage(QUERY_FINISH);
			}
		}).start();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (checkedId) {
		
		//在此处添加对应的过滤条件,第一个和第二个的过滤条件不是很理解
		case R.id.mostBefore:
			adapter.setSelectRadioButton(0);
//			filterProperties.add()
			break;
		case R.id.mostAfter:
			adapter.setSelectRadioButton(1);
			break;
		case R.id.shortestTime:
			adapter.setSelectRadioButton(5);

			SearchProperty property = new SearchProperty();
			property.setmProperty(Properties.Total_time);
			filterProperties.add(property);
			break;
		case R.id.lowestMileag:
			adapter.setSelectRadioButton(3);

			SearchProperty propertyLowestMile = new SearchProperty();
			propertyLowestMile.setmProperty(Properties.Total_mileage);
			filterProperties.add(propertyLowestMile);
			break;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.transitFilterBtn:
			transitSearchFilterProperties.setmProperty(filterProperties);
			Intent data = new Intent();
			data.putExtra(TransitSearchFilterActivity.TRANST_SEARCH_FILTER_RESULT, transitSearchFilterProperties);
			setResult(RESULT_OK, data);
			finish();
			break;

		case R.id.otherTransitFilterBtn:
			Intent intent = new Intent(this, TransitSearchFilterOtherActivity.class);
			intent.putExtra(TransitSearchFilterActivity.TRANST_SEARCH_FILTER_RESULT, transitSearchFilterProperties);
			startActivity(intent);
			finish();
			break;
		}
	}

}
