package com.tripmaster.traintickets.transit;

import com.trip.trainnumber.entity.dao.TrainNumberTransitSearchResultDao.Properties;
import com.tripmaster.R;
import com.tripmaster.base.BaseFragmentActivity;
import com.tripmaster.base.dbhelper.TrainNumberTransitSearchResultDBHelper;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import de.greenrobot.dao.Property;

/**
 * 其他筛选列表
 * 
 * **/
public class TransitSearchFilterOtherActivity extends BaseFragmentActivity implements OnClickListener{

	public static String TRANSIT_FILTER_PROPERTY_LIST= "transit_filter_property_list";
	public static String[] FILTER_TIME = new String[]{"00:00", "05:59", "06:00", "11:59", "12:00", "17:59", "18:00", "23:59"}; 
	
	/**始发时间选择**/
	private TextView startItem11,startItem12,startItem13,startItem21,startItem22;
	private int startIndex = 4;
	
	/**终到时间选择**/
	private TextView endItem31,endItem32,endItem33,endItem41,endItem42;
	private int endIndex = 4;
	
	/**逗留时间**/
	private TextView stayItem52,stayItem53;
	private int stayIndex = -1;
	
	/**中转到站时间**/
	private TextView endStationItem82,endStationItem83,endStationItem91,endStationItem92,endStationItem93;
	private int endStationIndex = 4;
	
	/**中转出发时间**/
	private TextView startStationItem122,startStationItem123,startStationItem131,startStationItem132,startStationItem133;
	private int startStationIndex = 4;
	
	/**到站车型选择**/
	private TextView endModelItem62;
	
	/**始发站车型选择**/
	private TextView startModelItem102;
	
	private TextView transitFilterBtn;
	
	private TransitSearchFilterProperties transitSearchFilterProperties;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_transit_search_result_filter_other);
		
		initView();
		initData();
	}
	
	private void initView() {
		initTitle("筛选", true);
		initViewStartTime();
		initViewEndTime();
		initViewStay();
		initViewEndStationTime();
		initViewStartStationTime();
		initModel();
		
		transitFilterBtn = (TextView) findViewById(R.id.transitFilterBtn);
		transitFilterBtn.setOnClickListener(this);
	}
	
	private void initModel() {
		endModelItem62 = (TextView) findViewById(R.id.item62);
		startModelItem102 = (TextView) findViewById(R.id.item102);
		
		ModelOnClickListener listener = new ModelOnClickListener();
		endModelItem62.setOnClickListener(listener);
		startModelItem102.setOnClickListener(listener);
		
		Intent intent = getIntent();
		if(intent != null){
			transitSearchFilterProperties = (TransitSearchFilterProperties) intent.getSerializableExtra(TransitSearchFilterActivity.TRANST_SEARCH_FILTER_RESULT);
		}
	}
	
	class ModelOnClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.item62://到站车型选择
//				transitSearchFilterProperties.setmBeforeTransitTrainType(mBeforeTransitTrainType);
				break;
			case R.id.item102://始发站车型选择
//				transitSearchFilterProperties.setmAfterTransitTrainType(mBeforeTransitTrainType);
				break;
			}
		}
	}
	
	
	private void initViewStartTime() {
		startItem11 = (TextView) findViewById(R.id.item11);
		startItem12 = (TextView) findViewById(R.id.item12);
		startItem13 = (TextView) findViewById(R.id.item13);
		startItem21 = (TextView) findViewById(R.id.item21);
		startItem22 = (TextView) findViewById(R.id.item22);
		
		StartTimeOnClickListener listener = new StartTimeOnClickListener();
		startItem11.setOnClickListener(listener);
		startItem12.setOnClickListener(listener);
		startItem13.setOnClickListener(listener);
		startItem21.setOnClickListener(listener);
		startItem22.setOnClickListener(listener);

	}
	
	class StartTimeOnClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.item11:
				startIndex = 0;
				startItem11.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				startItem12.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startItem13.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startItem21.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startItem22.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				
				startItem11.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				startItem12.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startItem13.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startItem21.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startItem22.setBackgroundResource(R.drawable.choise_check_bg_selector);
				transitSearchFilterProperties.setmStartTimeDuration(new String[]{FILTER_TIME[0], FILTER_TIME[1]});//始发时间：00:00-05:59
				break;
			case R.id.item12:
				startIndex = 1;
				startItem11.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startItem12.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				startItem13.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startItem21.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startItem22.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				
				startItem11.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startItem12.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				startItem13.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startItem21.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startItem22.setBackgroundResource(R.drawable.choise_check_bg_selector);
				transitSearchFilterProperties.setmStartTimeDuration(new String[]{FILTER_TIME[2], FILTER_TIME[3]});//始发时间：06:00-11:59
				
				break;
			case R.id.item13:
				startIndex = 2;
				startItem11.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startItem12.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startItem13.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				startItem21.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startItem22.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				
				startItem11.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startItem12.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startItem13.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				startItem21.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startItem22.setBackgroundResource(R.drawable.choise_check_bg_selector);
				transitSearchFilterProperties.setmStartTimeDuration(new String[]{FILTER_TIME[4], FILTER_TIME[5]});//始发时间：12:00-17:59
				
				break;
			case R.id.item21:
				startIndex = 3;
				startItem11.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startItem12.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startItem13.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startItem21.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				startItem22.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				
				startItem11.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startItem12.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startItem13.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startItem21.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				startItem22.setBackgroundResource(R.drawable.choise_check_bg_selector);
				transitSearchFilterProperties.setmStartTimeDuration(new String[]{FILTER_TIME[6], FILTER_TIME[7]});//始发时间：18:00-23:59
				
				break;
			case R.id.item22:
				startIndex = 4;
				startItem11.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startItem12.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startItem13.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startItem21.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startItem22.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				
				startItem11.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startItem12.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startItem13.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startItem21.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startItem22.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				transitSearchFilterProperties.setmStartTimeDuration(new String[]{FILTER_TIME[0], FILTER_TIME[7]});//始发时间：不限
				
				break;
			}
		}
	}
	
	private void initViewEndTime() {
		endItem31 = (TextView) findViewById(R.id.item31);
		endItem32 = (TextView) findViewById(R.id.item32);
		endItem33 = (TextView) findViewById(R.id.item33);
		endItem41 = (TextView) findViewById(R.id.item41);
		endItem42 = (TextView) findViewById(R.id.item42);
		
		EndOnClickListener listener = new EndOnClickListener();
		endItem31.setOnClickListener(listener);
		endItem32.setOnClickListener(listener);
		endItem33.setOnClickListener(listener);
		endItem41.setOnClickListener(listener);
		endItem42.setOnClickListener(listener);
		
	}
	
	class EndOnClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.item31:
				endIndex = 0;
				endItem31.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				endItem32.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endItem33.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endItem41.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endItem42.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				
				endItem31.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				endItem32.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endItem33.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endItem41.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endItem42.setBackgroundResource(R.drawable.choise_check_bg_selector);
				transitSearchFilterProperties.setmEndTimeDuration(new String[]{FILTER_TIME[0], FILTER_TIME[1]});//终到时间：00:00-05:59
				break;
			case R.id.item32:
				endIndex = 1;
				endItem31.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endItem32.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				endItem33.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endItem41.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endItem42.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				
				endItem31.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endItem32.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				endItem33.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endItem41.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endItem42.setBackgroundResource(R.drawable.choise_check_bg_selector);
				transitSearchFilterProperties.setmEndTimeDuration(new String[]{FILTER_TIME[2], FILTER_TIME[3]});//终到时间：06:00-11:59
				break;
			case R.id.item33:
				endIndex = 2;
				endItem31.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endItem32.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endItem33.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				endItem41.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endItem42.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				
				endItem31.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endItem32.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endItem33.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				endItem41.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endItem42.setBackgroundResource(R.drawable.choise_check_bg_selector);
				transitSearchFilterProperties.setmEndTimeDuration(new String[]{FILTER_TIME[4], FILTER_TIME[5]});//终到时间：12:00-17:59
				break;
			case R.id.item41:
				endIndex = 3;
				endItem31.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endItem32.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endItem33.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endItem41.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				endItem42.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				
				endItem31.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endItem32.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endItem33.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endItem41.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				endItem42.setBackgroundResource(R.drawable.choise_check_bg_selector);
				transitSearchFilterProperties.setmEndTimeDuration(new String[]{FILTER_TIME[6], FILTER_TIME[7]});//终到时间：18:00-23:59
				
				break;
			case R.id.item42:
				endIndex = 4;
				endItem31.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endItem32.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endItem33.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endItem41.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endItem42.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				
				endItem31.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endItem32.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endItem33.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endItem41.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endItem42.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				transitSearchFilterProperties.setmEndTimeDuration(new String[]{FILTER_TIME[0], FILTER_TIME[7]});//终到时间：00:00-23:59
				
				break;
			}
		}
	
		
	}
	
	private void initViewStay() {
		stayItem52 = (TextView) findViewById(R.id.item52);
		stayItem53 = (TextView) findViewById(R.id.item53);
		
		StayOnClickListener listener = new StayOnClickListener();
		stayItem52.setOnClickListener(listener);
		stayItem53.setOnClickListener(listener);
		
	}
	
	class StayOnClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.item52:
				stayIndex = 0;
				stayItem52.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				stayItem53.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				
				stayItem52.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				stayItem53.setBackgroundResource(R.drawable.choise_check_bg_selector);
				
				SearchProperty property = new SearchProperty();
				property.setmProperty(Properties.Transfer_time);
				transitSearchFilterProperties.getmProperty().add(property);//中转时间最小值
				break;
			case R.id.item53:
				stayIndex = 1;
				stayItem52.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				stayItem53.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				
				stayItem52.setBackgroundResource(R.drawable.choise_check_bg_selector);
				stayItem53.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				SearchProperty propertyDesc = new SearchProperty();
				propertyDesc.setmProperty(Properties.Transfer_time);
				propertyDesc.setmOrderStyle(TrainNumberTransitSearchResultDBHelper.ORDER_DESC);
				transitSearchFilterProperties.getmProperty().add(propertyDesc);//中转时间最小值
				break;
			}
		}
	}
	
	private void initViewEndStationTime() {
		endStationItem82 = (TextView) findViewById(R.id.item82);
		endStationItem83 = (TextView) findViewById(R.id.item83);
		endStationItem91 = (TextView) findViewById(R.id.item91);
		endStationItem92 = (TextView) findViewById(R.id.item92);
		endStationItem93 = (TextView) findViewById(R.id.item93);
		
		EndStationOnClickListener listener = new EndStationOnClickListener();
		endStationItem82.setOnClickListener(listener);
		endStationItem83.setOnClickListener(listener);
		endStationItem91.setOnClickListener(listener);
		endStationItem92.setOnClickListener(listener);
		endStationItem93.setOnClickListener(listener);
		
	}
	
	class EndStationOnClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.item82:
				endStationIndex = 0;
				endStationItem82.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				endStationItem83.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endStationItem91.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endStationItem92.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endStationItem93.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				
				endStationItem82.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				endStationItem83.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endStationItem91.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endStationItem92.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endStationItem93.setBackgroundResource(R.drawable.choise_check_bg_selector);
				
				transitSearchFilterProperties.setmTransitArrivedTimeDuration(new String[]{FILTER_TIME[0], FILTER_TIME[1]});//中到站时间：00:00-05:59
				break;
			case R.id.item83:
				endStationIndex = 1;
				endStationItem82.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endStationItem83.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				endStationItem91.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endStationItem92.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endStationItem93.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				
				endStationItem82.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endStationItem83.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				endStationItem91.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endStationItem92.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endStationItem93.setBackgroundResource(R.drawable.choise_check_bg_selector);
				transitSearchFilterProperties.setmTransitArrivedTimeDuration(new String[]{FILTER_TIME[2], FILTER_TIME[3]});//中到站时间：06:00-11:59
				break;
			case R.id.item91:
				endStationIndex = 2;
				endStationItem82.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endStationItem83.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endStationItem91.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				endStationItem92.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endStationItem93.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				
				endStationItem82.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endStationItem83.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endStationItem91.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				endStationItem92.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endStationItem93.setBackgroundResource(R.drawable.choise_check_bg_selector);
				transitSearchFilterProperties.setmTransitArrivedTimeDuration(new String[]{FILTER_TIME[4], FILTER_TIME[5]});//中到站时间："12:00-17:59"
				break;
			case R.id.item92:
				endStationIndex = 3;
				endStationItem82.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endStationItem83.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endStationItem91.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endStationItem92.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				endStationItem93.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				
				endStationItem82.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endStationItem83.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endStationItem91.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endStationItem92.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				endStationItem93.setBackgroundResource(R.drawable.choise_check_bg_selector);
				transitSearchFilterProperties.setmTransitArrivedTimeDuration(new String[]{FILTER_TIME[6], FILTER_TIME[7]});//中到站时间："18:00-23:59"
				break;
			case R.id.item93:
				endStationIndex = 4;
				endStationItem82.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endStationItem83.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endStationItem91.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endStationItem92.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				endStationItem93.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				
				endStationItem82.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endStationItem83.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endStationItem91.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endStationItem92.setBackgroundResource(R.drawable.choise_check_bg_selector);
				endStationItem93.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				transitSearchFilterProperties.setmTransitArrivedTimeDuration(new String[]{FILTER_TIME[0], FILTER_TIME[7]});//中到站时间："00:00-23:59"
				break;
			}
		}
	}
	
	
	
	private void initViewStartStationTime() {
		startStationItem122 = (TextView) findViewById(R.id.item122);
		startStationItem123 = (TextView) findViewById(R.id.item123);
		startStationItem131 = (TextView) findViewById(R.id.item131);
		startStationItem132 = (TextView) findViewById(R.id.item132);
		startStationItem133 = (TextView) findViewById(R.id.item133);
		
		StartStationOnClickListener listener = new StartStationOnClickListener();
		startStationItem122.setOnClickListener(listener);
		startStationItem123.setOnClickListener(listener);
		startStationItem131.setOnClickListener(listener);
		startStationItem132.setOnClickListener(listener);
		startStationItem133.setOnClickListener(listener);
		
	}
	
	class StartStationOnClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.item122:
				startStationIndex = 0;
				startStationItem122.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				startStationItem123.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startStationItem131.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startStationItem132.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startStationItem133.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				
				startStationItem122.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				startStationItem123.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startStationItem131.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startStationItem132.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startStationItem133.setBackgroundResource(R.drawable.choise_check_bg_selector);
				
				transitSearchFilterProperties.setmTransitGoTimeDuration(new String[]{FILTER_TIME[0], FILTER_TIME[1]});//中出发时间：00:00-05:59
				break;
			case R.id.item123:
				startStationIndex = 1;
				startStationItem122.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startStationItem123.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				startStationItem131.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startStationItem132.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startStationItem133.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				
				startStationItem122.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startStationItem123.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				startStationItem131.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startStationItem132.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startStationItem133.setBackgroundResource(R.drawable.choise_check_bg_selector);
				transitSearchFilterProperties.setmTransitGoTimeDuration(new String[]{FILTER_TIME[2], FILTER_TIME[3]});//中出发时间：06:00-11:59
				break;
			case R.id.item131:
				startStationIndex = 2;
				startStationItem122.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startStationItem123.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startStationItem131.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				startStationItem132.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startStationItem133.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				
				startStationItem122.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startStationItem123.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startStationItem131.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				startStationItem132.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startStationItem133.setBackgroundResource(R.drawable.choise_check_bg_selector);
				transitSearchFilterProperties.setmTransitGoTimeDuration(new String[]{FILTER_TIME[4], FILTER_TIME[5]});//中出发时间：12:00-17:59
				break;
			case R.id.item132:
				startStationIndex = 3;
				startStationItem122.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startStationItem123.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startStationItem131.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startStationItem132.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				startStationItem133.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				
				startStationItem122.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startStationItem123.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startStationItem131.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startStationItem132.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				startStationItem133.setBackgroundResource(R.drawable.choise_check_bg_selector);
				transitSearchFilterProperties.setmTransitGoTimeDuration(new String[]{FILTER_TIME[6], FILTER_TIME[7]});//中出发时间：18:00-23:59
				break;
			case R.id.item133:
				startStationIndex = 4;
				startStationItem122.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startStationItem123.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startStationItem131.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startStationItem132.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				startStationItem133.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				
				startStationItem122.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startStationItem123.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startStationItem131.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startStationItem132.setBackgroundResource(R.drawable.choise_check_bg_selector);
				startStationItem133.setBackgroundResource(R.drawable.choise_checked_bg_selector);
				transitSearchFilterProperties.setmTransitGoTimeDuration(new String[]{FILTER_TIME[0], FILTER_TIME[7]});//中出发时间：00:00-23:59
				break;
			}
		}
	}
	
	private void initData() {
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.transitFilterBtn:
			Intent data = new Intent();
			data.putExtra(TransitSearchFilterActivity.TRANST_SEARCH_FILTER_RESULT, transitSearchFilterProperties);
			setResult(RESULT_OK, data);
			finish();
			break;

		default:
			break;
		}
	}

}
