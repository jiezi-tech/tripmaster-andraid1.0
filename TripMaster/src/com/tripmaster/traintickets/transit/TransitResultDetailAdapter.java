package com.tripmaster.traintickets.transit;

import java.util.List;

import com.trip.trainnumber.entity.dao.TrainNumberJingGuoZhanPrice;
import com.tripmaster.R;
import com.tripmaster.http.TrainNumberJingGuoZhan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TransitResultDetailAdapter extends BaseAdapter{
	private List<TrainNumberJingGuoZhanPrice> guoZhans;// 经过站列表

	public void setGuoZhans(List<TrainNumberJingGuoZhanPrice> guoZhans) {
		this.guoZhans = guoZhans;
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return guoZhans != null && guoZhans.size() > 0 ? guoZhans.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holer = null;
		Context context = parent.getContext();
		if(convertView == null){
			convertView = LayoutInflater.from(context).inflate(
					R.layout.result_detail_recycler_view_item, null);
			holer = new ViewHolder();

			holer.station_no = (TextView) convertView.findViewById(R.id.station_no);
			holer.station_name = (TextView) convertView.findViewById(R.id.station_name);
			holer.station_arrivals_time = (TextView) convertView
					.findViewById(R.id.station_arrivals_time);
			holer.station_depart_time = (TextView) convertView
					.findViewById(R.id.station_depart_time);
			holer.station_stop_time = (TextView) convertView
					.findViewById(R.id.station_stop_time);
			holer.station_no_icon = (ImageView) convertView.findViewById(R.id.station_no_icon);
			
			convertView.setTag(holer);
		}
		else
			holer = (ViewHolder) convertView.getTag();
		
		
		TrainNumberJingGuoZhanPrice guoZhan = guoZhans.get(position);
		holer.station_no.setText(guoZhan.getTo_station_no());
		holer.station_name.setText(guoZhan.getEnd_station_name());
		holer.station_arrivals_time.setText(guoZhan.getArrive_time());
		holer.station_depart_time.setText(guoZhan.getStart_time());
		holer.station_stop_time.setText(guoZhan.getStopover_time());
		
		//始发站、终点站 颜色、图标设置
		if(position == 0 || position == guoZhans.size()-1){
			holer.station_no.setTextColor(context.getResources().getColor(R.color.resultListbottomBarTextPressedColor));
			holer.station_name.setTextColor(context.getResources().getColor(R.color.resultListbottomBarTextPressedColor));
			holer.station_arrivals_time.setTextColor(context.getResources().getColor(R.color.resultListbottomBarTextPressedColor));
			holer.station_depart_time.setTextColor(context.getResources().getColor(R.color.resultListbottomBarTextPressedColor));
			holer.station_stop_time.setTextColor(context.getResources().getColor(R.color.resultListbottomBarTextPressedColor));
			
			if(position == 0){
				holer.station_no_icon.setImageResource(R.drawable.start);
			}
			else{
				holer.station_no_icon.setImageResource(R.drawable.end);
			}
			holer.station_no_icon.setVisibility(View.VISIBLE);
		}
		else{
			holer.station_no.setTextColor(context.getResources().getColor(R.color.resultListdetailTextColor));
			holer.station_name.setTextColor(context.getResources().getColor(R.color.resultListdetailTextColor));
			holer.station_arrivals_time.setTextColor(context.getResources().getColor(R.color.resultListdetailTextColor));
			holer.station_depart_time.setTextColor(context.getResources().getColor(R.color.resultListdetailTextColor));
			holer.station_stop_time.setTextColor(context.getResources().getColor(R.color.resultListdetailTextColor));
			holer.station_no_icon.setVisibility(View.GONE);
		}
	
		return convertView;
	}
	


	class ViewHolder {
		private TextView station_no;// 站序
		private TextView station_name;// 站名
		private TextView station_arrivals_time;// 到达时间
		private TextView station_depart_time;// 出发时间
		private TextView station_stop_time;// 停留时间
		private ImageView station_no_icon;//图标
	}

}
