package com.tripmaster.traintickets;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.tripmaster.R;
import com.tripmaster.base.BaseFragmentActivity;

/***
 * 
 * 站站查询之后的结果筛选界面
 **/
public class ResultListFilterActivity extends BaseFragmentActivity implements OnClickListener {
	
	/** Train Type **/
	public static final String[] TRAIN_TYPE = new String[]{ "G%", "D%", "Z%", "T%", "K%", "[1-9]%", "*"};//匹配正整数的正则：^[1-9]\d*$,因为只有数字，所以只需要匹配数字开头的就行
	
	/** Selected **/
	public static final int TRAIN_SELECTED = 1;
	
	/** No Selected **/
	public static final int TRAIN_NO_SELECTED = 0;
	
	/** 筛选按钮 **/
	private TextView confirm;

	/** 清除按钮 **/
	private TextView clearData;
	private boolean clicked;
	
	private TextView gt, dc, zk, tk, ks, pk;
	private int[] checkResult = new int[] { TRAIN_NO_SELECTED, TRAIN_NO_SELECTED, TRAIN_NO_SELECTED, TRAIN_NO_SELECTED, TRAIN_NO_SELECTED, TRAIN_NO_SELECTED };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_searchresult_filter);
		initView();
		initTitle("筛选", true);
	}

	private void initView() {
		confirm = (TextView) findViewById(R.id.confirm);
		confirm.setOnClickListener(this);

		clearData = (TextView) findViewById(R.id.filter);
		clearData.setOnClickListener(this);
		clearData.setVisibility(View.VISIBLE);
		clearData.setText(getResources().getString(R.string.selected_all));
		
		gt = (TextView) findViewById(R.id.gt);
		dc = (TextView) findViewById(R.id.dc);
		zk = (TextView) findViewById(R.id.zk);
		tk = (TextView) findViewById(R.id.tk);
		ks = (TextView) findViewById(R.id.ks);
		pk = (TextView) findViewById(R.id.pk);

		gt.setOnClickListener(this);
		dc.setOnClickListener(this);
		zk.setOnClickListener(this);
		tk.setOnClickListener(this);
		ks.setOnClickListener(this);
		pk.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.confirm:// 筛选
			Intent data = new Intent();
			data.putExtra("trainClassName", checkResult);
			setResult(RESULT_OK, data);
			finish();
			break;
		
		case R.id.filter://清除所选
			String strDeleteAll = getResources().getString(R.string.clear_all);//设置清除
			String strSelectedAll = getResources().getString(R.string.selected_all);//设置全选
			int color = 0;
			int drawable = 0;
			if(clearData.getText().toString().equals(strDeleteAll)){
				checkResult = new int[]{ TRAIN_NO_SELECTED, TRAIN_NO_SELECTED, TRAIN_NO_SELECTED, TRAIN_NO_SELECTED, TRAIN_NO_SELECTED, TRAIN_NO_SELECTED };
				color = getResources().getColor(R.color.resultListbottomBarTextColor);
				drawable = R.drawable.choise_check_bg_selector;
				clearData.setText(strSelectedAll);
			}else{
				checkResult = new int[]{ TRAIN_SELECTED, TRAIN_SELECTED, TRAIN_SELECTED, TRAIN_SELECTED, TRAIN_SELECTED, TRAIN_SELECTED };
				color = getResources().getColor(R.color.resultListbottomBarTextPressedColor);
				drawable = R.drawable.choise_checked_bg_selector;
				clearData.setText(strDeleteAll);
			}
			gt.setTextColor(color);
			gt.setBackgroundResource(drawable);
			dc.setTextColor(color);
			dc.setBackgroundResource(drawable);
			zk.setTextColor(color);
			zk.setBackgroundResource(drawable);
			tk.setTextColor(color);
			tk.setBackgroundResource(drawable);
			ks.setTextColor(color);
			ks.setBackgroundResource(drawable);
			pk.setTextColor(color);
			pk.setBackgroundResource(drawable);
			break;
		case R.id.gt:// 高铁
			updateView(0);
			break;

		case R.id.dc:// 动车
			updateView(1);
			break;

		case R.id.zk:// 直快
			updateView(2);
			break;

		case R.id.tk:// 特快
			updateView(3);
			break;

		case R.id.ks:// 快速
			updateView(4);
			break;

		case R.id.pk:// 普快
			updateView(5);
			break;
		}
		
		int totalNum = checkResult[0]+checkResult[1]+checkResult[2]+checkResult[3]+checkResult[4]+checkResult[5];
		if(totalNum == 0){
			clearData.setText(getResources().getString(R.string.selected_all));
		}else{
			clearData.setText(getResources().getString(R.string.clear_all));
		}
	}

	/**
	 * 更新选中项
	 **/
	private void updateView(int position) {
		this.checkResult[position] = this.checkResult[position] == TRAIN_SELECTED ? TRAIN_NO_SELECTED : TRAIN_SELECTED;
		switch (position) {
		case 0:
			if (this.checkResult[position] == TRAIN_SELECTED) {
				gt.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				gt.setBackgroundResource(R.drawable.choise_checked_bg_selector);
			} else {
				gt.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				gt.setBackgroundResource(R.drawable.choise_check_bg_selector);
			}
			break;

		case 1:
			if (this.checkResult[position] == TRAIN_SELECTED) {
				dc.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				dc.setBackgroundResource(R.drawable.choise_checked_bg_selector);
			} else {
				dc.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				dc.setBackgroundResource(R.drawable.choise_check_bg_selector);
			}
			break;
		case 2:
			if (this.checkResult[position] == TRAIN_SELECTED) {
				zk.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				zk.setBackgroundResource(R.drawable.choise_checked_bg_selector);
			} else {
				zk.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				zk.setBackgroundResource(R.drawable.choise_check_bg_selector);
			}
			break;
		case 3:
			if (this.checkResult[position] == TRAIN_SELECTED) {
				tk.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				tk.setBackgroundResource(R.drawable.choise_checked_bg_selector);
			} else {
				tk.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				tk.setBackgroundResource(R.drawable.choise_check_bg_selector);
			}
			break;
		case 4:
			if (this.checkResult[position] == TRAIN_SELECTED) {
				ks.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				ks.setBackgroundResource(R.drawable.choise_checked_bg_selector);
			} else {
				ks.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				ks.setBackgroundResource(R.drawable.choise_check_bg_selector);
			}
			break;
		case 5:
			if (this.checkResult[position] == TRAIN_SELECTED) {
				pk.setTextColor(getResources().getColor(R.color.resultListbottomBarTextPressedColor));
				pk.setBackgroundResource(R.drawable.choise_checked_bg_selector);
			} else {
				pk.setTextColor(getResources().getColor(R.color.resultListbottomBarTextColor));
				pk.setBackgroundResource(R.drawable.choise_check_bg_selector);
			}
			break;
		}

	}
}
