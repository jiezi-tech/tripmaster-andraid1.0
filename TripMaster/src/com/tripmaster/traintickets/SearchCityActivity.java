package com.tripmaster.traintickets;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.EditText;
import android.widget.TabHost;
import com.trip.trainnumber.entity.dao.TrainStation;
import com.tripmaster.R;
import com.tripmaster.base.BaseFragmentActivity;
import com.tripmaster.sortlistview.CitySelectStationListFragment;
import com.tripmaster.traintickets.CitySelectRecentlyFragment.MyOnItem;

public class SearchCityActivity extends BaseFragmentActivity implements MyOnItem{
	

	private TabHost tabHost;
	private TabManager tabManager;
	private int requestCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_city_select);
		initTitle("车站选择",true);
		initTabHost(savedInstanceState);
		requestCode = getIntent().getIntExtra("requestCode", 0);
	}
	
	public void initTabHost(Bundle savedInstanceState){
		
		tabManager = new TabManager(this, getSupportFragmentManager(),
				R.id.realtabcontent, listener);
//		tabHost = (TabHost) findViewById(R.id.tabhost);
		tabHost = tabManager.handleCreateView(getWindow().getDecorView());
		tabManager.addTab(
				tabHost.newTabSpec("tab1").setIndicator("最近常用"),
				CitySelectRecentlyFragment.class, null);
		tabManager.addTab(
				tabHost.newTabSpec("tab2").setIndicator("热点车站"),
				CitySelectRecentlyFragment.class, null);
		tabManager.addTab(
				tabHost.newTabSpec("tab3").setIndicator("车站列表"),
				CitySelectStationListFragment.class, null);
		Bundle bundle = new Bundle();
		bundle.putString("tab", "OneWayTicket");
		tabManager.handleViewStateRestored(savedInstanceState);
		
		
//		tabHost.setup();
//		tabHost.addTab(tabHost.newTabSpec("tab1").setIndicator("最近常用").setContent(R.id.view1));
//		tabHost.addTab(tabHost.newTabSpec("tab2").setIndicator("热点车站").setContent(R.id.view2));
//		tabHost.addTab(tabHost.newTabSpec("tab3").setIndicator("车站列表").setContent(R.id.view3));
//		tabHost.setOnTabChangedListener(new OnTabChangeListener() {
//			
//			@Override
//			public void onTabChanged(String tabId) {
//				// TODO Auto-generated method stub
//				if (tabId.equals("tab1")) {
////					roundTabIv.setVisibility(View.INVISIBLE);
////					oneWayTabIv.setVisibility(View.VISIBLE);
////					onwWayTabTv.setTextColor(getResources().getColor(R.color.titleSelectColor));
////					roundTabTv.setTextColor(getResources().getColor(R.color.titleUnselectColor));
//				} else if (tabId.equals("tab2")) {
////					roundTabIv.setVisibility(View.VISIBLE);
////					oneWayTabIv.setVisibility(View.INVISIBLE);
////					onwWayTabTv.setTextColor(getResources().getColor(R.color.titleUnselectColor));
////					roundTabTv.setTextColor(getResources().getColor(R.color.titleSelectColor));
//				} else if (tabId.equals("tab3")) {
//					
//				}
//			}
//		});
	}
	
	@Override
	protected void onDestroy() {
		if(tabManager != null)
			tabManager.handleDestroyView();
		super.onDestroy();
	}
	
	MyOnTabChangeListener listener = new MyOnTabChangeListener() {

		@Override
		public void OnTabChange(String tag) {
			if (tag.equals("tab1")) {
//				roundTabIv.setVisibility(View.INVISIBLE);
//				oneWayTabIv.setVisibility(View.VISIBLE);
//				onwWayTabTv.setTextColor(getResources().getColor(R.color.titleSelectColor));
//				roundTabTv.setTextColor(getResources().getColor(R.color.titleUnselectColor));
			} else if (tag.equals("tab2")) {
//				roundTabIv.setVisibility(View.VISIBLE);
//				oneWayTabIv.setVisibility(View.INVISIBLE);
//				onwWayTabTv.setTextColor(getResources().getColor(R.color.titleUnselectColor));
//				roundTabTv.setTextColor(getResources().getColor(R.color.titleSelectColor));
			} else if (tag.equals("tab3")) {

			}

		}
	};
	
	public int getRequestCode() {
		return requestCode;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onItemClick(TrainStation station) {
		Intent intent = new Intent();
		intent.putExtra("station", station);//将车站对象返回
		setResult(requestCode, intent);
		finish();
		
	}
}
