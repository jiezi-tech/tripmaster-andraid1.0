package com.tripmaster.traintickets;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.trip.trainnumber.entity.dao.TrainNumberSearchResult;
import com.trip.trainnumber.entity.dao.TrainNumberSearchResultDao.Properties;
import com.trip.trainnumber.entity.dao.TrainStation;
import com.tripmaster.R;
import com.tripmaster.base.BaseFragmentActivity;
import com.tripmaster.base.dbhelper.TrainNumberSearchResultDBHelper;
import com.tripmaster.http.StationToStationRequest;
import com.tripmaster.http.StationToStationRequest.QueryZhanToZhanCallBack;
import com.tripmaster.traintickets.transit.TransitSearchResultActivity;
import com.tripmaster.util.Constant;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 站站查询结果页面
 **/
public class ResultListMainActivity extends BaseFragmentActivity implements OnClickListener, OnCheckedChangeListener {

	private LinearLayout yesterday;
	private LinearLayout tomorrow;
	private ListView listView;
	private ViewStub emptyView;
	private TicketResultAdapter ticketResultAdapter;
	private Handler handler;

	private TrainStation fromStation;// 出发站
	private TrainStation toStation;// 到达站

	private String startDate;// 出发日期
	private boolean isRoundTrip;// 是否带往返
	private ProgressDialog dialog;
	private TextView transitTv;// 中转
	private TextView todayTextView;// 当前时间

	private RadioGroup filterRadioGroup;
	private TrainNumberSearchResultDBHelper helper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_searchresult_main);
		yesterday = (LinearLayout) findViewById(R.id.yesterday);
		yesterday.setOnClickListener(this);
		tomorrow = (LinearLayout) findViewById(R.id.tomorrow);
		tomorrow.setOnClickListener(this);

		todayTextView = (TextView) findViewById(R.id.todayTextView);

		listView = (ListView) findViewById(R.id.resultlist);
		emptyView = (ViewStub) findViewById(R.id.emptyView);
		listView.setEmptyView(emptyView);

		filterRadioGroup = (RadioGroup) findViewById(R.id.filterRadioGroup);
		filterRadioGroup.setOnCheckedChangeListener(this);

		ticketResultAdapter = new TicketResultAdapter(getApplicationContext());
		listView.setAdapter(ticketResultAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				TrainNumberSearchResult tn = ticketResultAdapter.getItem(arg2);
				String t = JSON.toJSONString(tn);
				Intent toDetailIntent = new Intent(ResultListMainActivity.this, ResultDetailActivity.class);
				toDetailIntent.putExtra("trainName", tn.getTrain_class_name() + " " + tn.getStation_train_code());
				toDetailIntent.putExtra("ticketPrice", getPrice(tn));
				toDetailIntent.putExtra("startTime", tn.getStart_time() + "");
				toDetailIntent.putExtra("endTime", tn.getArrive_time() + "");
				toDetailIntent.putExtra("startStation", tn.getFrom_station_name() + "");
				toDetailIntent.putExtra("endStation", tn.getTo_station_name() + "");
				toDetailIntent.putExtra("trainNumber", t);
				toDetailIntent.putExtra("startImage", tn.getStart_station_name().equals(tn.getFrom_station_name()));
				toDetailIntent.putExtra("endImage", tn.getEnd_station_name().equals(tn.getTo_station_name()));

				if (tn.getAllMileage() != null)
					toDetailIntent.putExtra("useTime", tn.getAllTime());
				String allMileage = new String();
				try {
					int from_no = Integer.parseInt(tn.getFrom_station_no());
					int to_no = Integer.parseInt(tn.getTo_station_no());
					allMileage = (to_no - from_no) + "站";
				} catch (NumberFormatException e) {

				}
				if (tn.getAllMileage() != null)
					allMileage += " " + tn.getAllMileage();
				toDetailIntent.putExtra("useStation", allMileage);
				String[] nums = getMaxNum(tn).split("/");
				toDetailIntent.putExtra("type", nums[0]);
				toDetailIntent.putExtra("number", nums[1]);

				startActivity(toDetailIntent);
			}
		});

		Intent intent = getIntent();
		if (intent != null) {
			toStation = (TrainStation) intent.getSerializableExtra("toStation");
			fromStation = (TrainStation) intent.getSerializableExtra("fromStation");
			startDate = intent.getStringExtra("date");

			if (startDate.equals(Constant.formatCurrentString()))
				todayTextView.setText(startDate + " 今天");
			else
				todayTextView.setText(startDate + " " + Constant.getWeekOfDate(startDate));

			isRoundTrip = intent.getBooleanExtra("isRoundTrip", false);

			System.out.println(fromStation.getTele_code() + "  " + toStation.getTele_code() + "   " + startDate + "  "
					+ isRoundTrip);

			getTrainNumberList(fromStation.getTele_code(), toStation.getTele_code(), startDate, handler);
		}

		transitTv = (TextView) findViewById(R.id.transit);
		transitTv.setOnClickListener(this);
		initTitle(fromStation.getStation_name() + "--" + toStation.getStation_name(), "共" + listView.getCount() + "条",
				true, true);

		filterButton = (TextView) findViewById(R.id.filter);
		filterButton.setVisibility(View.VISIBLE);
		filterButton.setOnClickListener(this);

		helper = TrainNumberSearchResultDBHelper.getInstance();
	}

	public void getTrainNumberList(String from_station_telecode, String to_station_telecode, final String queryDate,
			final Handler h) {
		dialog = new ProgressDialog(this);
		dialog.setTitle("网络请求");
		dialog.setMessage("数据拉取中......");
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
		StationToStationRequest stationRequest = new StationToStationRequest(this);
		stationRequest.getQuery(queryDate, from_station_telecode, to_station_telecode, new QueryZhanToZhanCallBack() {

			@Override
			public void onResult(List<TrainNumberSearchResult> list) {
				if (dialog != null && dialog.isShowing())
					dialog.dismiss();
				if (list != null) {
					helper.deleteAll();
					helper.saveTrainNumberSearchResult(list);

					ticketResultAdapter.append(list);
					// 重新设置拉取到的条数
					initTitle(fromStation.getStation_name() + "--" + toStation.getStation_name(),
							"共" + ticketResultAdapter.getCount() + "条", true, true);
				} else
					System.out.println("查询失败  无结果");

			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.yesterday:// 前一天
			if (!startDate.equals(Constant.formatCurrentString())) {
				startDate = Constant.beforeDay(startDate);
				if (startDate.equals(Constant.formatCurrentString()))
					todayTextView.setText(startDate + " 今天");
				else
					todayTextView.setText(startDate + " " + Constant.getWeekOfDate(startDate));

				getTrainNumberList(fromStation.getTele_code(), toStation.getTele_code(), startDate, handler);
			} else {
				todayTextView.setText(startDate + " 今天");
				Toast.makeText(this, "不能再往后选了  已经是当前日期了", Toast.LENGTH_LONG).show();
			}
			break;
		case R.id.tomorrow:// 后一天
			startDate = Constant.afterDay(startDate);
			todayTextView.setText(startDate + " " + Constant.getWeekOfDate(startDate));

			getTrainNumberList(fromStation.getTele_code(), toStation.getTele_code(), startDate, handler);
			break;
		case R.id.transit:// 中转
			Intent intent = new Intent(this, TransitSearchResultActivity.class);
			intent.putExtra("fromStation", fromStation);
			intent.putExtra("toStation", toStation);
			intent.putExtra("date", startDate);
			startActivity(intent);
			break;
		case R.id.filter:// 筛选
			Intent intent2 = new Intent(this, ResultListFilterActivity.class);
			startActivityForResult(intent2, Constant.RESULTLISTFILTER);
			break;
		}
	}

	/** 得到票数最多到显示 **/
	private String getMaxNum(TrainNumberSearchResult n) {
		int max = 0;
		String result = new String();
		try {
			max = Integer.parseInt(n.getGr_num());
			result = "高级软卧/" + max + "张";
		} catch (NumberFormatException e) {
			result = "高级软卧/" + n.getGr_num();
		}

		try {
			int temp = Integer.parseInt(n.getRz_num());
			if (temp > max) {
				result = "软座/" + max + "张";
				max = temp;
			}
		} catch (NumberFormatException e) {
			result = "软座/" + n.getGr_num();
		}
		try {
			int temp = Integer.parseInt(n.getTz_num());
			if (temp > max) {
				result = "特等座/" + max + "张";
				max = temp;
			}
		} catch (NumberFormatException e) {
			result = "特等座/" + n.getGr_num();
		}
		try {
			int temp = Integer.parseInt(n.getSwz_num());
			if (temp > max) {
				result = "商务座/" + max + "张";
				max = temp;
			}
		} catch (NumberFormatException e) {
			result = "商务座/" + n.getGr_num();
		}
		try {
			int temp = Integer.parseInt(n.getZy_num());
			if (temp > max) {
				result = "一等座/" + max + "张";
				max = temp;
			}
		} catch (NumberFormatException e) {
			result = "一等座/" + n.getGr_num();
		}
		try {
			int temp = Integer.parseInt(n.getZe_num());
			if (temp > max) {
				result = "二等座/" + max + "张";
				max = temp;
			}
		} catch (NumberFormatException e) {
			result = "二等座/" + n.getGr_num();
		}
		try {
			int temp = Integer.parseInt(n.getRw_num());
			if (temp > max) {
				result = "软卧/" + max + "张";
				max = temp;
			}
		} catch (NumberFormatException e) {
			result = "软卧/" + n.getGr_num();
		}
		try {
			int temp = Integer.parseInt(n.getYw_num());
			if (temp > max) {
				result = "硬卧/" + max + "张";
				max = temp;
			}
		} catch (NumberFormatException e) {
			result = "硬卧/" + n.getGr_num();
		}
		try {
			int temp = Integer.parseInt(n.getYz_num());
			if (temp > max) {
				result = "硬座/" + max + "张";
				max = temp;
			}
		} catch (NumberFormatException e) {
			result = "硬座/" + n.getGr_num();
		}
		try {
			int temp = Integer.parseInt(n.getWz_num());
			if (temp > max) {
				result = "无座/" + max + "张";
				max = temp;
			}
		} catch (NumberFormatException e) {
			result = "无座/" + n.getGr_num();
		}

		return result;
	}

	/** 拼接票价 ***/
	private String getPrice(TrainNumberSearchResult n) {
		StringBuffer buffer = new StringBuffer();
		if (n.getGr_price() != null && !n.getGr_price().isEmpty())
			buffer.append("高级软卧:").append(n.getGr_price());
		if (n.getRz_price() != null && !n.getRz_price().isEmpty())
			buffer.append(" 软座:").append(n.getRz_price());
		if (n.getTz_price() != null && !n.getTz_price().isEmpty())
			buffer.append(" 特等座:").append(n.getTz_price());
		if (n.getSwz_price() != null && !n.getSwz_price().isEmpty())
			buffer.append(" 商务座:").append(n.getSwz_price());
		if (n.getZy_price() != null && !n.getZy_price().isEmpty())
			buffer.append(" 一等座:").append(n.getZy_price());
		if (n.getZe_price() != null && !n.getZe_price().isEmpty())
			buffer.append(" 二等座:").append(n.getZe_price());
		if (n.getRw_price() != null && !n.getRw_price().isEmpty())
			buffer.append(" 软卧:").append(n.getRw_price());
		if (n.getYw_price() != null && !n.getYw_price().isEmpty())
			buffer.append(" 硬卧:").append(n.getYw_price());
		if (n.getYz_price() != null && !n.getYz_price().isEmpty())
			buffer.append(" 硬座:").append(n.getYz_price());
		if (n.getWz_price() != null && !n.getWz_price().isEmpty())
			buffer.append(" 无座:").append(n.getWz_price());

		return buffer.toString();
	}

	public class TicketResultAdapter extends BaseAdapter {
		public LayoutInflater inflater;
		public List<TrainNumberSearchResult> list;

		public TicketResultAdapter(Context context) {
			// TODO Auto-generated constructor stub
			inflater = LayoutInflater.from(context);
		}

		public void append(List<TrainNumberSearchResult> list) {
			this.list = list;
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list != null ? list.size() : 0;
		}

		@Override
		public TrainNumberSearchResult getItem(int position) {
			// TODO Auto-generated method stub
			return list != null ? list.get(position) : null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Train holder = null;
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.arrayadapter_resultitem, null);
				holder = new Train();
				holder.trainName = (TextView) convertView.findViewById(R.id.trainName);
				holder.ticketPrice = (TextView) convertView.findViewById(R.id.ticketPrice);
				holder.startTime = (TextView) convertView.findViewById(R.id.startTime);
				holder.endTime = (TextView) convertView.findViewById(R.id.endTime);
				holder.imageStart = (ImageView) convertView.findViewById(R.id.imageStart);
				holder.imageEnd = (ImageView) convertView.findViewById(R.id.imageEnd);
				holder.startStation = (TextView) convertView.findViewById(R.id.startStation);
				holder.endStation = (TextView) convertView.findViewById(R.id.endStation);
				holder.useTime = (TextView) convertView.findViewById(R.id.useTime);
				holder.useStation = (TextView) convertView.findViewById(R.id.useStation);
				holder.type = (TextView) convertView.findViewById(R.id.type);
				holder.number = (TextView) convertView.findViewById(R.id.number);
				convertView.setTag(holder);

			} else {
				holder = (Train) convertView.getTag();
			}

			TrainNumberSearchResult tn = getItem(position);
			if (tn != null) {
				holder.trainName.setText(tn.getTrain_class_name() + " " + tn.getStation_train_code());
				holder.ticketPrice.setText(getPrice(tn));
				holder.startTime.setText(tn.getStart_time() + "");
				holder.endTime.setText(tn.getArrive_time() + "");
				holder.startStation.setText(tn.getFrom_station_name() + "");
				holder.endStation.setText(tn.getTo_station_name() + "");
				//区分始发站，过站，终到站
				holder.imageStart.setImageResource(tn.getFrom_station_name().equals(tn.getStart_station_name()) ? R.drawable.start : R.drawable.transit_end);
				holder.imageEnd.setImageResource(tn.getTo_station_name().equals(tn.getEnd_station_name()) ? R.drawable.end : R.drawable.transit_end);
				
				// 设置时间
				try {
					long lishi = Integer.parseInt(tn.getLishiValue());
					long hour = lishi / 60;
					long second = lishi % 60;

					if (hour > 0)
						holder.useTime.setText(hour + "时" + second + "分");
					else
						holder.useTime.setText(second + "分");
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}

				// if (tn.getAllMileage() != null)
				// holder.useTime.setText(tn.getAllTime());
				String allMileage = new String();
				try {
					int from_no = Integer.parseInt(tn.getFrom_station_no());
					int to_no = Integer.parseInt(tn.getTo_station_no());
					allMileage = (to_no - from_no) + "站";
				} catch (NumberFormatException e) {

				}
				if (tn.getAllMileage() != null)
					allMileage += " " + tn.getAllMileage();
				holder.useStation.setText(allMileage);

				String[] nums = getMaxNum(tn).split("/");
				holder.type.setText(nums[0]);
				holder.number.setText(nums[1]);
			}

			return convertView;
		}

		public class Train {
			TextView trainName;
			TextView ticketPrice;
			TextView startTime;
			TextView endTime;
			TextView startStation;
			TextView endStation;
			TextView useTime;
			TextView useStation;
			TextView type;
			TextView number;
			ImageView imageStart;
			ImageView imageEnd;
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == Constant.RESULTLISTFILTER && resultCode == RESULT_OK && data != null) {
			int[] trainClassName = data.getIntArrayExtra("trainClassName");
			List<TrainNumberSearchResult> results = new ArrayList<TrainNumberSearchResult>();
			for (int counter = 0, size = trainClassName.length; counter < size; counter++) {
				if (trainClassName[counter] == ResultListFilterActivity.TRAIN_SELECTED) {
					results.addAll(helper.getTrainNumberSearchResult(ResultListFilterActivity.TRAIN_TYPE[counter]));
				}
			}
			if (results != null)
				ticketResultAdapter.append(results);
		}
	}

	/***
	 * 底部单选响应回调
	 **/
	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		List<TrainNumberSearchResult> results = null;
		switch (checkedId) {
		case R.id.startTime:// 发时
			results = helper.orderAsValue(Properties.Start_time);
			break;

		case R.id.useTime:// 用时
			results = helper.orderAsValue(Properties.LishiValue);
			break;
		case R.id.arrivalTime:// 到时
			results = helper.orderAsValue(Properties.Arrive_time);
			break;
		}
		if (results != null)
			ticketResultAdapter.append(results);

	}
}
