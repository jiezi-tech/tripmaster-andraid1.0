package com.tripmaster.traintickets;

import java.util.List;

import com.tripmaster.R;
import com.tripmaster.http.TrainNumberJingGuoZhan;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class ResultDetailRecyclerViewAdapter extends
		RecyclerView.Adapter<ResultDetailRecyclerViewAdapter.ViewHolder> {
	private List<TrainNumberJingGuoZhan> guoZhans;// 经过站列表
	private Context context;

	public void setGuoZhans(List<TrainNumberJingGuoZhan> guoZhans) {
		this.guoZhans = guoZhans;
		notifyDataSetChanged();
	}

	class ViewHolder extends RecyclerView.ViewHolder {
		private TextView station_no;// 站序
		private TextView station_name;// 站名
		private TextView station_arrivals_time;// 到达时间
		private TextView station_depart_time;// 出发时间
		private TextView station_stop_time;// 停留时间
		private ImageView station_no_icon;//图标

		public ViewHolder(View v) {
			super(v);
			initView(v);
		}

		private void initView(View v) {
			station_no = (TextView) v.findViewById(R.id.station_no);
			station_name = (TextView) v.findViewById(R.id.station_name);
			station_arrivals_time = (TextView) v
					.findViewById(R.id.station_arrivals_time);
			station_depart_time = (TextView) v
					.findViewById(R.id.station_depart_time);
			station_stop_time = (TextView) v
					.findViewById(R.id.station_stop_time);
			station_no_icon = (ImageView) v.findViewById(R.id.station_no_icon);

		}

	}

	@Override
	public int getItemCount() {
		return guoZhans != null && guoZhans.size() > 0 ? guoZhans.size() : 0;
	}

	@Override
	public void onBindViewHolder(ViewHolder holer, int position) {
		TrainNumberJingGuoZhan guoZhan = guoZhans.get(position);
		holer.station_no.setText(guoZhan.getStation_no());
		holer.station_name.setText(guoZhan.getStation_name());
		holer.station_arrivals_time.setText(guoZhan.getArrive_time());
		holer.station_depart_time.setText(guoZhan.getStart_time());
		holer.station_stop_time.setText(guoZhan.getStopover_time());
		
		//始发站、终点站 颜色、图标设置
		if(position == 0 || position == guoZhans.size()-1){
			holer.station_no.setTextColor(context.getResources().getColor(R.color.resultListbottomBarTextPressedColor));
			holer.station_name.setTextColor(context.getResources().getColor(R.color.resultListbottomBarTextPressedColor));
			holer.station_arrivals_time.setTextColor(context.getResources().getColor(R.color.resultListbottomBarTextPressedColor));
			holer.station_depart_time.setTextColor(context.getResources().getColor(R.color.resultListbottomBarTextPressedColor));
			holer.station_stop_time.setTextColor(context.getResources().getColor(R.color.resultListbottomBarTextPressedColor));
			
			if(position == 0){
				holer.station_no_icon.setImageResource(R.drawable.start);
			}
			else{
				holer.station_no_icon.setImageResource(R.drawable.end);
			}
			holer.station_no_icon.setVisibility(View.VISIBLE);
		}
		else{
			holer.station_no.setTextColor(context.getResources().getColor(R.color.resultListdetailTextColor));
			holer.station_name.setTextColor(context.getResources().getColor(R.color.resultListdetailTextColor));
			holer.station_arrivals_time.setTextColor(context.getResources().getColor(R.color.resultListdetailTextColor));
			holer.station_depart_time.setTextColor(context.getResources().getColor(R.color.resultListdetailTextColor));
			holer.station_stop_time.setTextColor(context.getResources().getColor(R.color.resultListdetailTextColor));
			holer.station_no_icon.setVisibility(View.GONE);
		}
	}

	@SuppressLint("InflateParams") 
	@Override
	public ViewHolder onCreateViewHolder(ViewGroup group, int position) {
		View view = LayoutInflater.from(group.getContext()).inflate(
				R.layout.result_detail_recycler_view_item, null);
		context = group.getContext();
		return new ViewHolder(view);
	}

}
