package com.feiyucloud.http;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class AsyncHttpClient {
    private final ExecutorService threadPool;
    private int connectionTimeout = 20000;
    private int dataRetrievalTimeout = 20000;
    private boolean followRedirects = true;

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public int getDataRetrievalTimeout() {
        return dataRetrievalTimeout;
    }

    public void setDataRetrievalTimeout(int dataRetrievalTimeout) {
        this.dataRetrievalTimeout = dataRetrievalTimeout;
    }

    public boolean getFollowRedirects() {
        return followRedirects;
    }

    public void setFollowRedirects(boolean followRedirects) {
        this.followRedirects = followRedirects;
    }

    public AsyncHttpClient() {
        threadPool = Executors.newCachedThreadPool();
    }

    public void doRequest(final Request request, final ResponseHandler handler) {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                exeRequest(request, handler);
            }
        });
    }


	public void setDefaultSSLSocketFactory() {
		SSLContext sc;
		try {
			sc = SSLContext.getInstance("TLS");
			TrustManager[] tmArr = { new X509TrustManager() {
				public void checkClientTrusted(
						X509Certificate[] paramArrayOfX509Certificate,
						String paramString) throws CertificateException {
				}

				public void checkServerTrusted(
						X509Certificate[] paramArrayOfX509Certificate,
						String paramString) throws CertificateException {
				}

				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
			} };
			sc.init(null, tmArr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc
					.getSocketFactory());
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
    private void exeRequest(final Request request, final ResponseHandler handler) {
        HttpURLConnection connection = null;
        Request.Method method = request.getMethod();
        String url = request.getUrl();

        try {
        	setDefaultSSLSocketFactory();
        	
            URL resourceUrl = new URL(url);
            connection = (HttpURLConnection) resourceUrl.openConnection();

            // Settings
            connection.setConnectTimeout(connectionTimeout);
            connection.setReadTimeout(dataRetrievalTimeout);
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(followRedirects);
            connection.setRequestMethod(method.toString());
            connection.setDoInput(true);

            // Headers
            Map<String, String> headers = request.getHeaders();
            for (Map.Entry<String, String> header : headers.entrySet()) {
                connection.setRequestProperty(header.getKey(), header.getValue());
            }
            // Request start
            handler.sendStartMessage();

            // Request Body
            // POST and PUT expect an output body.
            if (method == Request.Method.POST) {
                connection.setDoOutput(true);
                if (request.hasFiles()) {
                    // Use multipart/form-data to send fields and files
                    // 32kb at a time
                    connection.setChunkedStreamingMode(32 * 1024);
                    MultipartWriter.write(connection, request);
                } else {
                    // Send content as form-urlencoded
                    byte[] content = request.encodeParameters();
                    connection.setRequestProperty("Content-Type",
                            "application/x-www-form-urlencoded;charset=" + Request.UTF8);
                    connection.setRequestProperty("Content-Length", Long.toString(content.length));
                    // Stream the data so we don't run out of memory
                    connection.setFixedLengthStreamingMode(content.length);
                    OutputStream os = connection.getOutputStream();
                    os.write(content);
                    os.flush();
                    os.close();
                }
            }
            // Process the response in the handler because it can be done in
            // different ways
            handler.processResponse(connection);
            // Request finished
            handler.sendFinishMessage();

        } catch (IOException e) {
            handler.sendFailureMessage(e);
        } finally {
            if (connection != null) {
                connection.disconnect();
                connection = null;
            }
        }

    }
}
